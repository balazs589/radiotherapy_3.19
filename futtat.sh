# datum=`date +%Y%m%d_%H%M%S` && nohup ./futtat.sh "${datum}" >"${datum}_stdout.txt"  2>&1 </dev/null &
# watch -d tail ./output/teleterapia.txt
# sha256sum -c checksum3_sha256.txt

#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-5.5/lib64

###########################################################
egyszerre_elinditott_foton_db=100000            # [db] kernelhivasok parametere
ciklus_db=10   #000                            # [db] enyiszer inditunk el fotonokat egy kiserletben
kiserletek_szama=10                             # [db] ennyi kiserletet atlagolunk a teljes meresben

kocka_oldalhossz_cm=50.0            # [cm] mintat tartalmazo kocka elhosszusaga
voxel_meret_cm=`echo "scale=5;${kocka_oldalhossz_cm}/250.0" | bc`       # [cm] mindig 250 db voxel jusson egy elhosszra


# korabbi beallitas:
# x = oldalhossz / 2
# y = oldalhossz / 2
# z = oldalhossz + inditasi_tavolsag_cm
kezdopont_x=25.0        # [cm] forras korlap kozeppontjanak x koordinataja
kezdopont_y=25.0        # [cm] forras korlap kozeppontjanak y koordinataja
kezdopont_z=51.0        # [cm] forras korlap kozeppontjanak z koordinataja


# korabbi beallitas: y-z sikban megdontve nyalab_doles_fok mertekben
# vagy fuggolegesen lefele: 0,0,-1
kezdoirany_x=0.0         # [1] korlap normalvektoranak x komponense
kezdoirany_y=0.0         # [1] korlap normalvektoranak y komponense
kezdoirany_z=-1.0        # [1] korlap normalvektoranak z komponense

nyalab_sugar_cm=0.0                 # [cm] nyalab atmero fele
                                    # 0.1 0.2 0.3 0.4 0.5  ... 

# parhuzamos vagy divergalo: (eredetileg a validalaskor hasznalva)
# --------------------------------------------
# nyalab_divergencia_fok=0.0; nyalab_tipus="parhuzam"     # [°] nyalab szettartas (izotrop kup felnyillas szoge fokban)

##### vagy:
 nyalab_divergencia_fok=45.0; nyalab_tipus="divergal"     # [°] nyalab szettartas
#--------------------------------------------

#inditasi_tavolsag_cm=1.0            # [cm] inditasi korlap tavolsaga a kocka felso lapja folott

#nyalab_doles_fok=0.0                # [°] izotrop kup tengelyenek dontese y-z sikban


###########################################################
datum=$1
#datum=`date +%Y%m%d_%H%M%S`
mappa="${datum}_output_${nyalab_tipus}_${nyalab_sugar_cm}/"


# "a program 15db bemeneti parametert var:"
#
# " 1.: egyszerre elinditott fotonok szama"
# " 2.: egy kiserletben hany ciklusban inditunk fotonokat"
# " 3.: kiserletek szama"
#
# " 4.: kocka merete cm-ben"
# " 5.: voxel meret cm-ben"
#
# " 6.: forras kezdopont x koordinataja cm-ben"
# " 7.: forras kezdopont y koordinataja cm-ben"
# " 8.: forras kezdopont z koordinataja cm-ben"
#
# " 9.: kezdo iranyvektor x komponense"
# "10.: kezdo iranyvektor y komponense"
# "11.: kezdo iranyvektor z komponense"
#
# "12.: indulo nyalab korlap sugara cm-ben"
# "13.: nyalab szettartasa (kup fel nyilasszoge fokban)"
#
# "14.: indulo nyalab korlap kozeppontjanak tavolsaga a kocka felso lapjatol cm-ben"
# "15.: nyalab iranyanak elterese a fuggolegestol fokban (y-z sikban)"



echo "--------------------------------------"
date
echo "
teleterapia.run \
 \
 ${egyszerre_elinditott_foton_db} \
 ${ciklus_db} \
 ${kiserletek_szama} \
 ${kocka_oldalhossz_cm} \
 ${voxel_meret_cm} \
 ${kezdopont_x} \
 ${kezdopont_y} \
 ${kezdopont_z} \
 ${kezdoirany_x} \
 ${kezdoirany_y} \
 ${kezdoirany_z} \
 ${nyalab_sugar_cm} \
 ${nyalab_divergencia_fok} \
 \
"
echo "kezdes" 
echo "--------------------------------------"


(rm -rf output/; mkdir output/) && \
(cd output/; mkdir eredmeny/; mkdir kepek/; mkdir generalt_adatok; cd generalt_adatok; mkdir adatok) && \
(cp ./input/spektrum.txt ./output/; cp ./input/00_hataskeresztmetszet_NaI.txt ./output/; cp ./input/01_hataskeresztmetszet_H2O.txt ./output/; cp ./input/02_hataskeresztmetszet_Al.txt ./output/; cp ./input/03_hataskeresztmetszet_N2.txt ./output/) && \
 \
(  `#nvprof --unified-memory-profiling off` ./teleterapia.run \
 \
 ${egyszerre_elinditott_foton_db} \
 ${ciklus_db} \
 ${kiserletek_szama} \
 ${kocka_oldalhossz_cm} \
 ${voxel_meret_cm} \
 ${kezdopont_x} \
 ${kezdopont_y} \
 ${kezdopont_z} \
 ${kezdoirany_x} \
 ${kezdoirany_y} \
 ${kezdoirany_z} \
 ${nyalab_sugar_cm} \
 ${nyalab_divergencia_fok} \
 \
 > ./output/teleterapia.txt) && \
(./feldolgoz.run) && \
(cd ./rajzol; ./metszet_rajzol.sh) && \
(zip -r ./output/src.zip ./src/) && \
(mv output/ "${mappa}/")



echo ""
echo "        --------------------------------------"
echo -n "        "; date
echo "
        teleterapia.run \
 \
 ${egyszerre_elinditott_foton_db} \
 ${ciklus_db} \
 ${kiserletek_szama} \
 ${kocka_oldalhossz_cm} \
 ${voxel_meret_cm} \
 ${kezdopont_x} \
 ${kezdopont_y} \
 ${kezdopont_z} \
 ${kezdoirany_x} \
 ${kezdoirany_y} \
 ${kezdoirany_z} \
 ${nyalab_sugar_cm} \
 ${nyalab_divergencia_fok} \
 \
"
echo "        vege" 
echo "        --------------------------------------"


echo "";

#####################################################################################
