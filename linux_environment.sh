
rm -rf linux_environment.txt

echo "==================================================================" >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "lsb_release -a" >> linux_environment.txt
lsb_release -a >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "uname -a" >> linux_environment.txt
uname -a >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "nvcc --version" >> linux_environment.txt
nvcc --version >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "gcc --version" >> linux_environment.txt
gcc --version >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "doxygen --version" >> linux_environment.txt
doxygen --version >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "octave --version" >> linux_environment.txt
octave --version >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "gnuplot --version" >> linux_environment.txt
gnuplot --version >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "make --version" >> linux_environment.txt
make --version >> linux_environment.txt
echo "" >> linux_environment.txt

echo "------------------------------------------------------------------" >> linux_environment.txt
echo "bash --version" >> linux_environment.txt
bash --version >> linux_environment.txt
echo "" >> linux_environment.txt

echo "==================================================================" >> linux_environment.txt
/usr/local/cuda-8.0/samples/1_Utilities/deviceQuery/deviceQuery >> linux_environment.txt
echo "" >> linux_environment.txt
echo "==================================================================" >> linux_environment.txt


#echo "" >> linux_environment.txt
#echo "------------------------------------------------------------------" >> linux_environment.txt
#echo "sudo lshw " >> linux_environment.txt
#sudo lshw >> linux_environment.txt
#echo "" >> linux_environment.txt
#echo "==================================================================" >> linux_environment.txt

