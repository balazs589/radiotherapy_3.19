
#set palette gray;
#set palette defined (...);


set terminal pngcairo;

set output "../output/kepek/gnuplot_metszet_01_xy.png";
plot '../output/generalt_adatok/adatok/voxel_metszet_01.txt'  matrix with image

set output "../output/kepek/gnuplot_metszet_02_xy.png";
plot '../output/generalt_adatok/adatok/voxel_metszet_02.txt'  matrix with image

set output "../output/kepek/gnuplot_metszet_03_xy.png";
plot '../output/generalt_adatok/adatok/voxel_metszet_03.txt'  matrix with image

set output "../output/kepek/gnuplot_metszet_04_yz.png";
plot '../output/generalt_adatok/adatok/voxel_metszet_04.txt'  matrix with image

set output "../output/kepek/gnuplot_metszet_04_yz_szoras.png";
plot '../output/generalt_adatok/adatok/voxel_metszet_04_szoras.txt'  matrix with image
