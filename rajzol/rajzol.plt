set key left;

set terminal pngcairo;
set output "test.png";
test





set title "az egyes fotonok által leadott energiák eloszlása"
set xlabel " E [MeV] "
set ylabel " [db] "

set output '../output/kepek/detektor_logscale.png'
set logscale y
set key off
plot '../output/generalt_adatok/adatok/detektor_output.txt' with lines linewidth 2



set output '../output/kepek/detektor_linscale.png'
unset logscale y
set key off
plot '../output/generalt_adatok/adatok/detektor_output.txt' with lines linewidth 2



#set ytics 0.1
set xlabel " E [MeV] "
set ylabel ''



set title "tapasztalati es elméleti eloszlás függvények"
set output "../output/kepek/energia_01.png";
plot    "../output/generalt_adatok/adatok/generalt_eloszlas.txt" using 2:1 title 'generált' with steps lw 5 linetype 7 , \
        "../output/generalt_adatok/adatok/elmeleti_eloszlas.txt" using 1:2 title 'elméleti' with lines lw 2 dashtype 2 linetype -1;

set title "tapasztalati eloszlás függvény"
set output "../output/kepek/energia_generalt.png";
plot    "../output/generalt_adatok/adatok/generalt_eloszlas.txt" using 2:1 title 'generált' with steps lw 5 linetype 7;

set title "elméleti eloszlás függvény"
set output "../output/kepek/energia_elmeleti.png";
plot    "../output/generalt_adatok/adatok/elmeleti_eloszlas.txt" using 1:2 title 'elméleti' with lines lw 2 dashtype 2 linetype -1;



set output


# # set size square;
# set size ratio -1
# set output "koordinatak.png";
# splot "generalt_koordinatak.txt" using 6:7:8 title 'korlap' with points pointtype 0;

# set output "vegpont_metszet.png";
# set xrange [0.0:50.0]
# set yrange [0.0:50.0]
# plot "generalt_koordinatak.txt" using 12:13 title 'vegpontok' with points pointtype 0;
# # splot "generalt_koordinatak.txt" using 11:12:13 title 'vegpontok' with points pointtype 0;

# set autoscale
# # set output "iranyok.png";
# # set view equal xyz;
# # splot 'generalt_iranyok.txt';
