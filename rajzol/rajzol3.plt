
#set palette gray;
#set palette defined (...);


set terminal pngcairo;

set output "../output/kepek/gnuplot_log_metszet_01_xy.png";
plot '../output/generalt_adatok/adatok/log_voxel_metszet_01.txt'  matrix with image

set output "../output/kepek/gnuplot_log_metszet_02_xy.png";
plot '../output/generalt_adatok/adatok/log_voxel_metszet_02.txt'  matrix with image

set output "../output/kepek/gnuplot_log_metszet_03_xy.png";
plot '../output/generalt_adatok/adatok/log_voxel_metszet_03.txt'  matrix with image

set output "../output/kepek/gnuplot_log_metszet_04_yz.png";
plot '../output/generalt_adatok/adatok/log_voxel_metszet_04.txt'  matrix with image
