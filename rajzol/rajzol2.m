%function [] = rajzol2()

A01 = dlmread('../output/generalt_adatok/adatok/voxel_metszet_01.txt');
A02 = dlmread('../output/generalt_adatok/adatok/voxel_metszet_02.txt');
A03 = dlmread('../output/generalt_adatok/adatok/voxel_metszet_03.txt');
A04 = dlmread('../output/generalt_adatok/adatok/voxel_metszet_04.txt');
A05 = dlmread('../output/generalt_adatok/adatok/voxel_metszet_04_szoras.txt');



figure(1)
imagesc(A01)
set(gca, 'YDir', 'normal');
axis equal;
colorbar;
print ../output/kepek/octave_metszet_01_xy.jpg -djpg

figure(2)
imagesc(A02)
set(gca, 'YDir', 'normal');
axis equal;
colorbar;
print ../output/kepek/octave_metszet_02_xy.jpg -djpg

figure(3)
imagesc(A03)
set(gca, 'YDir', 'normal');
axis equal;
colorbar;
print ../output/kepek/octave_metszet_03_xy.jpg -djpg

figure(4)
imagesc(A04)
set(gca, 'YDir', 'normal');
axis equal;
colorbar;
print ../output/kepek/octave_metszet_04_yz.jpg -djpg



figure(5)
imagesc(A05)
set(gca, 'YDir', 'normal');
axis equal;
colorbar;
print ../output/kepek/octave_metszet_04_yz_szoras.jpg -djpg


close all