
### Sugárterápiás Monte Carlo szimulációs kód fejlesztése GPU architektúrára teleterápiás alkalmazások esetén

BME TTK NTI BSc szakdolgozat feladat megvalósítása NVIDIA CUDA C nyelven.

Szakdolgozat kiírás:
http://www.ttk.bme.hu/node/2857

Vázlatos dokumentáció:
http://fizikusbsc.hu/sugarterapia-szakdolgozat/
