#include "kirepulo_fotonok_kernel.cuh"

#include "Allapot.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Kirepülő fotonok kezelése.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void kirepulo_fotonok_kernel(int N, Allapot foton)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < N)
    {
        int kolcsonhatas_tipus = foton.kolcsonhatas_tipus[sorszam];
        
        if(kolcsonhatas_tipus == 0)
        {
            
            double regi_energia = foton.energia[sorszam];
            
            // esetleg a terfogatot elhagyo fotonok energiajanak kigyujtesehez:
            foton.leadott_energia[sorszam] = regi_energia;
            foton.energia[sorszam] = 0.0;
            
// foton energiak tallyzesehez, a megfelelo helyen kell kapcsolgatni:
//    if(foton.allapot_kapcsolo[sorszam] == 1)
//    {
//        foton.detektor_energia[sorszam] += regi_energia;
//    }
            
            
            // kovetkezo lepesben mar nem kell szamolni ezekkel a fotonokkal
            foton.aktiv_foton[sorszam] = 0;
            
        }
    }
}
