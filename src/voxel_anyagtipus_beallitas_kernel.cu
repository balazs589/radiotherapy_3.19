#include "voxel_anyagtipus_beallitas_kernel.cuh"


#include "Hataskeresztmetszet.cuh"
#include "constants.h"
//#include <cmath>



/*!
\ingroup Kernel_fuggvenyek_group

\brief
A mérés elején minden egyes voxel anyagtípusának beállítása.

\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void voxel_anyagtipus_beallitas_kernel(Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < szigma.voxelek_szama)
    {
        
        
        // tesztekhez: ket kulonbozo reteg egymasutan
        /*
        int voxel_anyagtipus = 0;   // elso feleben viz
        
        // melyebben aluminium:
        if( sorszam < ( szigma.voxelek_szama / 2 ) )
        {
            voxel_anyagtipus = 1;
        }
        /* */
        
        
        
        
        int voxel_anyagtipus = 0;   // mindenhol viz / NaI (ami inputFeldolgozasa.cu -ban van)
        /* */
        
        
        /*
        // NaI henger:
        // 0 - NaI
        // 1 - vakuum
        
        // voxel kozeppontjanak x,y,z koordinatai:
        double x = szigma.dx / 2 + szigma.dx * double( sorszam % szigma.nx);
        double y = szigma.dy / 2 + szigma.dy * double((sorszam / szigma.nx) % szigma.ny);
        double z = szigma.dz / 2 + szigma.dz * double((sorszam / szigma.nx) / szigma.ny);
        
        // kocka kozeppontja:
        double x0 = (szigma.x_min + szigma.x_max) / 2.0;
        double y0 = (szigma.y_min + szigma.y_max) / 2.0;
        double z0 = (szigma.z_min + szigma.z_max) / 2.0;
        
        
        // oldalhosszak fele:
        double a0 = (szigma.x_max - szigma.x_min) / 2.0;
        double b0 = (szigma.y_max - szigma.y_min) / 2.0;
        double c0 = (szigma.z_max - szigma.z_min) / 2.0;
        
        int voxel_anyagtipus = 1;   //  default vakuum
        
        // hengeren belul NaI
        // a henger atmeroje  a kocka oldalhosszaval
        // a henger magassaga a kocka oldalhosszanak 2/3-aval egyenlo 
        // (igy lesz kb. 4 cm magas es 3 cm sugaru)
        if ( ( 3.0*z < 4.0*z0 ) && ( (x-x0)*(x-x0)/(a0*a0) + (y-y0)*(y-y0)/(b0*b0) < 1.0 ) )
        {
            voxel_anyagtipus = 0;
        }
        
        /* */
        
        
        
        
        
        
        
        /*
        
        // 3D Shepp-Logan        
        // 50 x 50 x 50 cm
        
    
        int voxel_anyagtipus = 2;   //  default levego
        
        // voxel_anyagtipus:
        // 0 - viz
        // 1 - aluminimum
        // 2 - nitrogengaz
        
        
        double x = szigma.dx / 2 + szigma.dx * double( sorszam % szigma.nx);
        double y = szigma.dy / 2 + szigma.dy * double((sorszam / szigma.nx) % szigma.ny);
        double z = szigma.dz / 2 + szigma.dz * double((sorszam / szigma.nx) / szigma.ny);
        

        // Shepp-Logan 3D geometria 50cm x 50 cm x 50cm kockaban:
        
        double x1 = 25.000;
        double y1 = 25.000;
        double z1 = 25.000;
        double a1 = 17.250;
        double b1 = 23.000;
        double c1 = 17.250;

        double x2 = 25.000;
        double y2 = 24.540;
        double z2 = 25.000;
        double a2 = 16.560;
        double b2 = 21.850;
        double c2 = 16.560;

        double x3 = 30.500;
        double y3 = 25.000;
        double z3 = 25.000;
        double a3 =  2.750;
        double b3 =  7.750;
        double c3 =  2.750;
        double alfa3_rad = -18.0 / 180.0 * _PI_;

        double x4 = 19.500;
        double y4 = 25.000;
        double z4 = 25.000;
        double a4 =  4.000;
        double b4 = 10.250;
        double c4 =  4.000;
        double alfa4_rad = +18.0 / 180.0 * _PI_;
        
        double x5 = 25.000;
        double y5 = 33.750;
        double z5 = 25.000;
        double a5 =  5.250;
        double b5 =  6.250;
        double c5 =  5.250;
        
        double x6 = 25.000;
        double y6 = 27.500;
        double z6 = 25.000;
        double a6 =  1.150;
        double b6 =  1.150;
        double c6 =  1.150;
        
        double x7 = 25.000;
        double y7 = 22.500;
        double z7 = 25.000;
        double a7 =  1.150;
        double b7 =  1.150;
        double c7 =  1.150;

        double x8 = 23.000;
        double y8 =  9.875;
        double z8 = 25.000;
        double a8 =  1.150;
        double b8 =  0.575;
        double c8 =  1.150;

        double x9 = 25.000;
        double y9 =  9.875;
        double z9 = 25.000;
        double a9 =  0.575;
        double b9 =  0.575;
        double c9 =  0.575;
        
        double x10 = 26.500;
        double y10 =  9.875;
        double z10 = 25.000;
        double a10 =  0.575;
        double b10 =  1.150;
        double c10 =  0.575;
        
                
                
        // tobb ellipszoid egymasban elhelyezve
        if( (x-x1)*(x-x1)/(a1*a1) + (y-y1)*(y-y1)/(b1*b1) + (z-z1)*(z-z1)/(c1*c1) < 1.0 )
        {
            if( (x-x2)*(x-x2)/(a2*a2) + (y-y2)*(y-y2)/(b2*b2) + (z-z2)*(z-z2)/(c2*c2) > 1.0 )
            {
                voxel_anyagtipus = 1;   // aluminium burkolat
            }
            else
            {
                voxel_anyagtipus = 0;
                
                //elso ket ellipszis forgatva:
                if( ( sin(alfa3_rad)*sin(alfa3_rad) / (b3*b3) + cos(alfa3_rad)*cos(alfa3_rad) / (a3*a3) )*(x-x3)*(x-x3) + ( sin(alfa3_rad)*sin(alfa3_rad) / (a3*a3) + cos(alfa3_rad)*cos(alfa3_rad) / (b3*b3) )*(y-y3)*(y-y3) +  2.0*sin(alfa3_rad)*cos(alfa3_rad) * (1.0/(a3*a3) - 1.0/(b3*b3)) *(x-x3)*(y-y3) + (z-z3)*(z-z3)/(c3*c3)  < 1.0 )
                {
                    voxel_anyagtipus = 1;   //  
                }
                
                if( ( sin(alfa4_rad)*sin(alfa4_rad) / (b4*b4) + cos(alfa4_rad)*cos(alfa4_rad) / (a4*a4) )*(x-x4)*(x-x4) + ( sin(alfa4_rad)*sin(alfa4_rad) / (a4*a4) + cos(alfa4_rad)*cos(alfa4_rad) / (b4*b4) )*(y-y4)*(y-y4) +  2.0*sin(alfa4_rad)*cos(alfa4_rad) * (1.0/(a4*a4) - 1.0/(b4*b4)) *(x-x4)*(y-y4) + (z-z4)*(z-z4)/(c4*c4)  < 1.0 )
                {
                    voxel_anyagtipus = 1;   //  
                }


                
                

                
                if((x-x5)*(x-x5)/(a5*a5) + (y-y5)*(y-y5)/(b5*b5) + (z-z5)*(z-z5)/(c5*c5) < 1.0)
                {
                    voxel_anyagtipus = 1;   //  
                }
                
                if((x-x6)*(x-x6)/(a6*a6) + (y-y6)*(y-y6)/(b6*b6) + (z-z6)*(z-z6)/(c6*c6) < 1.0)
                {
                    voxel_anyagtipus = 1;   //  
                }

                if((x-x7)*(x-x7)/(a7*a7) + (y-y7)*(y-y7)/(b7*b7) + (z-z7)*(z-z7)/(c7*c7) < 1.0)
                {
                    voxel_anyagtipus = 1;   //  
                }
                
                if((x-x8)*(x-x8)/(a8*a8) + (y-y8)*(y-y8)/(b8*b8) + (z-z8)*(z-z8)/(c8*c8) < 1.0)
                {
                    voxel_anyagtipus = 1;   //  
                }
                
                if((x-x9)*(x-x9)/(a9*a9) + (y-y9)*(y-y9)/(b9*b9) + (z-z9)*(z-z9)/(c9*c9) < 1.0)
                {
                    voxel_anyagtipus = 1;   //  
                }
                
                if((x-x10)*(x-x10)/(a10*a10) + (y-y10)*(y-y10)/(b10*b10) + (z-z10)*(z-z10)/(c10*c10) < 1.0)
                {
                    voxel_anyagtipus = 1;   //  
                }
            }
            
            
        }
        
        /* */
        
        
        
        
        szigma.voxel_anyagtipus[sorszam] = voxel_anyagtipus;
        
        
    }
}
