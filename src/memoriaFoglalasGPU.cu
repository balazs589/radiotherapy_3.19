#include "memoriaFoglalasGPU.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"



/*!
\ingroup wrapper_fuggvenyek_group

\brief
Mérés elején grafikus kártya globális memóriájának lefoglalása, valamint Thrust pointerek beállítása.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void memoriaFoglalasGPU(Adatok *parameter)
{
    
    
    
    // memoriafoglalas anyagot tartalmazo terfogat adatai GPU-ra:
    
    // voxeleknek memoria foglalas GPU-n
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.voxel_anyagtipus,    parameter->_d_szigma.voxelek_szama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.voxel_rho,           parameter->_d_szigma.voxelek_szama*sizeof(double)));

    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.dozis,               parameter->_d_szigma.voxelek_szama*sizeof(double)));
    //CUDA_CALL(cudaMalloc(&parameter->_d_szigma.beutesek_szama,    parameter->_d_szigma.voxelek_szama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.dozis_atlag,         parameter->_d_szigma.voxelek_szama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.dozis_szoras,        parameter->_d_szigma.voxelek_szama*sizeof(double)));
    
    // egy lepes utan leadott energiak kigyujtesehez:
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.sorszam_gyujto,      parameter->_gyujto_meret*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.dozis_gyujto,        parameter->_gyujto_meret*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.beutesszam_gyujto,   parameter->_gyujto_meret*sizeof(int)));
    
    
    
    
    // XCOM hkrm adatoknak memoriafoglalas GPU-n
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.rho,     parameter->_d_szigma.anyagfajtak_szama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.energia, parameter->_d_szigma.sorok_szama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.fotoeff, parameter->_d_szigma.anyagfajtak_szama*parameter->_d_szigma.sorok_szama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.compton, parameter->_d_szigma.anyagfajtak_szama*parameter->_d_szigma.sorok_szama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.parkelt, parameter->_d_szigma.anyagfajtak_szama*parameter->_d_szigma.sorok_szama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.totalis, parameter->_d_szigma.anyagfajtak_szama*parameter->_d_szigma.sorok_szama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.szupremum, parameter->_d_szigma.sorok_szama*sizeof(double)));
    
    // detektor csatornaknak:
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.csatornak,                   parameter->_d_szigma.csatornak_szama*sizeof(long long int)));
    // csatornak kigyujtesehez:
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.csatorna_sorszam_gyujto,     parameter->_csatorna_gyujto_meret*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.csatorna_beutesszam_gyujto,  parameter->_csatorna_gyujto_meret*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.detektor_0_energias_fotonok_szama, sizeof(long long int)));
    
    
    CUDA_CALL(cudaMalloc(&parameter->_d_szigma.kirepult_energia,    sizeof(double)));
    
    
    
    
    
    // memoriafoglalas a fotonokat leiro adatoknak:
    CUDA_CALL(cudaMalloc(&parameter->_foton.energia,        parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.elozo_hely.x,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.elozo_hely.y,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.elozo_hely.z,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.elozo_irany.x,  parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.elozo_irany.y,  parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.elozo_irany.z,  parameter->fotonSzalakSzama*sizeof(double)));
    
    CUDA_CALL(cudaMalloc(&parameter->_foton.aktiv_foton,        parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.kolcsonhatas_tipus, parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_foton.leadott_energia,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.voxel,              parameter->fotonSzalakSzama*sizeof(int)));
    //CUDA_CALL(cudaMalloc(&parameter->_foton.leadott_energia_2,  parameter->fotonSzalakSzama*sizeof(double)));
    
    CUDA_CALL(cudaMalloc(&parameter->_foton.fotoeff_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.compton_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.parkelt_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_foton.volt_kolcsonhatas,  parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.allapot_kapcsolo,   parameter->fotonSzalakSzama*sizeof(int)));
    
    
    CUDA_CALL(cudaMalloc(&parameter->_foton.detektor_energia,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.detektor_csatorna,  parameter->fotonSzalakSzama*sizeof(int)));
    
    
    CUDA_CALL(cudaMalloc(&parameter->_foton.foton_aram_1,       6*parameter->fotonSzalakSzama*sizeof(int)));
    
    
    //teszteleshez
    CUDA_CALL(cudaMalloc(&parameter->_foton.hkrm_csoport,       parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.hkrm_totalis,       parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.lambda,             parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.kiindulo_pont.x,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.kiindulo_pont.y,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_foton.kiindulo_pont.z,    parameter->fotonSzalakSzama*sizeof(double)));
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    // memoriafoglalas a parkeltesbol szarmazo fotonokat leiro adatoknak:
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.energia,        parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.elozo_hely.x,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.elozo_hely.y,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.elozo_hely.z,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.elozo_irany.x,  parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.elozo_irany.y,  parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.elozo_irany.z,  parameter->fotonSzalakSzama*sizeof(double)));
    
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.aktiv_foton,        parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.kolcsonhatas_tipus, parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.leadott_energia,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.voxel,              parameter->fotonSzalakSzama*sizeof(int)));
    //CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.leadott_energia_2,  parameter->fotonSzalakSzama*sizeof(double)));
    
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.fotoeff_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.compton_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    //CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.parkelt_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.volt_kolcsonhatas,  parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.allapot_kapcsolo,   parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.detektor_energia,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.detektor_csatorna,  parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.foton_aram_1,       6*parameter->fotonSzalakSzama*sizeof(int)));
    
    
    //teszteleshez
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.hkrm_csoport,       parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.hkrm_totalis,       parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.lambda,             parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.kiindulo_pont.x,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.kiindulo_pont.y,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_parkelt_foton.kiindulo_pont.z,    parameter->fotonSzalakSzama*sizeof(double)));
    
    
    
    /*----------------------------------------------------------------------------------------------------*/
    
    // memoriafoglalas fotonok pillanatnyi allapotat leiro adatoknak:
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.energia,        parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.elozo_hely.x,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.elozo_hely.y,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.elozo_hely.z,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.elozo_irany.x,  parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.elozo_irany.y,  parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.elozo_irany.z,  parameter->fotonSzalakSzama*sizeof(double)));
    
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.aktiv_foton,        parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.kolcsonhatas_tipus, parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.leadott_energia,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.voxel,              parameter->fotonSzalakSzama*sizeof(int)));
    //CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.leadott_energia_2,  parameter->fotonSzalakSzama*sizeof(double)));
    
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.fotoeff_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.compton_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.parkelt_szam,       parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.volt_kolcsonhatas,  parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.allapot_kapcsolo,   parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.detektor_energia,   parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.detektor_csatorna,  parameter->fotonSzalakSzama*sizeof(int)));
    
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.foton_aram_1,       6*parameter->fotonSzalakSzama*sizeof(int)));
    
    
    //teszteleshez
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.hkrm_csoport,       parameter->fotonSzalakSzama*sizeof(int)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.hkrm_totalis,       parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.lambda,             parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.kiindulo_pont.x,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.kiindulo_pont.y,    parameter->fotonSzalakSzama*sizeof(double)));
    CUDA_CALL(cudaMalloc(&parameter->_pillanatnyi_foton.kiindulo_pont.z,    parameter->fotonSzalakSzama*sizeof(double)));
        
    /*----------------------------------------------------------------------------------------------------*/
    
    
    // kernelek altal hasznalt veletlenszam generatornak
    CUDA_CALL(cudaMalloc(&parameter->_devStates, parameter->fotonSzalakSzama*sizeof(curandState)));
    //CUDA_CALL(cudaMalloc(&parkelt_devStates, parameter->fotonSzalakSzama*sizeof(curandState)));
    
    
    /*----------------------------------------------------------------------------------------------------*/
    
    
    
    // thrust algoritmusok hasznalatahoz
    //thrust::device_ptr<int>       beutesek_szama  = thrust::device_pointer_cast(parameter->_d_szigma.beutesek_szama);
    //parameter->_thrust._dozis               = thrust::device_pointer_cast(parameter->_d_szigma.dozis);
    parameter->_thrust._dozis_atlag         = thrust::device_pointer_cast(parameter->_d_szigma.dozis_atlag);
    //thrust::device_ptr<double>  dozis_szoras        = thrust::device_pointer_cast(parameter->_d_szigma.dozis_szoras);
    
    parameter->_thrust._sorszam_gyujto      = thrust::device_pointer_cast(parameter->_d_szigma.sorszam_gyujto);
    parameter->_thrust._dozis_gyujto        = thrust::device_pointer_cast(parameter->_d_szigma.dozis_gyujto);
    parameter->_thrust._beutesszam_gyujto   = thrust::device_pointer_cast(parameter->_d_szigma.beutesszam_gyujto);
    
    
    
    parameter->_thrust._csatornak                   = thrust::device_pointer_cast(parameter->_d_szigma.csatornak);
    parameter->_thrust._csatorna_sorszam_gyujto     = thrust::device_pointer_cast(parameter->_d_szigma.csatorna_sorszam_gyujto);
    parameter->_thrust._csatorna_beutesszam_gyujto  = thrust::device_pointer_cast(parameter->_d_szigma.csatorna_beutesszam_gyujto);
    
    parameter->_thrust._detektor_0_energias_fotonok_szama    = thrust::device_pointer_cast(parameter->_d_szigma.detektor_0_energias_fotonok_szama);
    
    
    parameter->_thrust._kirepult_energia    = thrust::device_pointer_cast(parameter->_d_szigma.kirepult_energia);
    
    
    // elso fotonhoz
    parameter->_thrust._energia         = thrust::device_pointer_cast(parameter->_foton.energia);
    parameter->_thrust._elozo_hely_x    = thrust::device_pointer_cast(parameter->_foton.elozo_hely.x);
    parameter->_thrust._elozo_hely_y    = thrust::device_pointer_cast(parameter->_foton.elozo_hely.y);
    parameter->_thrust._elozo_hely_z    = thrust::device_pointer_cast(parameter->_foton.elozo_hely.z);
    
    parameter->_thrust._elozo_irany_x   = thrust::device_pointer_cast(parameter->_foton.elozo_irany.x);
    parameter->_thrust._elozo_irany_y   = thrust::device_pointer_cast(parameter->_foton.elozo_irany.y);
    parameter->_thrust._elozo_irany_z   = thrust::device_pointer_cast(parameter->_foton.elozo_irany.z);
    
    parameter->_thrust._aktiv_foton         = thrust::device_pointer_cast(parameter->_foton.aktiv_foton);
    parameter->_thrust._kolcsonhatas_tipus  = thrust::device_pointer_cast(parameter->_foton.kolcsonhatas_tipus);
    parameter->_thrust._leadott_energia     = thrust::device_pointer_cast(parameter->_foton.leadott_energia);
    
    parameter->_thrust._voxel           = thrust::device_pointer_cast(parameter->_foton.voxel);
    //thrust::device_ptr<double>  leadott_energia_2   = thrust::device_pointer_cast(parameter->_foton.leadott_energia_2);
    
    
    parameter->_thrust._fotoeff_szam   = thrust::device_pointer_cast(parameter->_foton.fotoeff_szam);
    parameter->_thrust._compton_szam   = thrust::device_pointer_cast(parameter->_foton.compton_szam);
    parameter->_thrust._parkelt_szam   = thrust::device_pointer_cast(parameter->_foton.parkelt_szam);
    
    parameter->_thrust._volt_kolcsonhatas   = thrust::device_pointer_cast(parameter->_foton.volt_kolcsonhatas);
    parameter->_thrust._allapot_kapcsolo   = thrust::device_pointer_cast(parameter->_foton.allapot_kapcsolo);
    
    parameter->_thrust._detektor_energia   = thrust::device_pointer_cast(parameter->_foton.detektor_energia);
    parameter->_thrust._detektor_csatorna  = thrust::device_pointer_cast(parameter->_foton.detektor_csatorna);
    
    parameter->_thrust._foton_aram_1   = thrust::device_pointer_cast(parameter->_foton.foton_aram_1);
    
    
    
    //teszteleshez
    parameter->_thrust._hkrm_csoport    = thrust::device_pointer_cast(parameter->_foton.hkrm_csoport);
    parameter->_thrust._hkrm_totalis    = thrust::device_pointer_cast(parameter->_foton.hkrm_totalis);
    parameter->_thrust._lambda          = thrust::device_pointer_cast(parameter->_foton.lambda);
    
    parameter->_thrust._kiindulo_pont_x = thrust::device_pointer_cast(parameter->_foton.kiindulo_pont.x);
    parameter->_thrust._kiindulo_pont_y = thrust::device_pointer_cast(parameter->_foton.kiindulo_pont.y);
    parameter->_thrust._kiindulo_pont_z = thrust::device_pointer_cast(parameter->_foton.kiindulo_pont.z);
    
    
    
    
    // parkeltesbol szarmazo fotonhoz
    parameter->_thrust._parkelt_energia          = thrust::device_pointer_cast(parameter->_parkelt_foton.energia);
    parameter->_thrust._parkelt_elozo_hely_x     = thrust::device_pointer_cast(parameter->_parkelt_foton.elozo_hely.x);
    parameter->_thrust._parkelt_elozo_hely_y     = thrust::device_pointer_cast(parameter->_parkelt_foton.elozo_hely.y);
    parameter->_thrust._parkelt_elozo_hely_z     = thrust::device_pointer_cast(parameter->_parkelt_foton.elozo_hely.z);
    parameter->_thrust._parkelt_elozo_irany_x    = thrust::device_pointer_cast(parameter->_parkelt_foton.elozo_irany.x);
    parameter->_thrust._parkelt_elozo_irany_y    = thrust::device_pointer_cast(parameter->_parkelt_foton.elozo_irany.y);
    parameter->_thrust._parkelt_elozo_irany_z    = thrust::device_pointer_cast(parameter->_parkelt_foton.elozo_irany.z);
    
    parameter->_thrust._parkelt_aktiv_foton         = thrust::device_pointer_cast(parameter->_parkelt_foton.aktiv_foton);
    parameter->_thrust._parkelt_kolcsonhatas_tipus  = thrust::device_pointer_cast(parameter->_parkelt_foton.kolcsonhatas_tipus);
    
    parameter->_thrust._parkelt_leadott_energia = thrust::device_pointer_cast(parameter->_parkelt_foton.leadott_energia);
    parameter->_thrust._parkelt_voxel           = thrust::device_pointer_cast(parameter->_parkelt_foton.voxel);
    //thrust::device_ptr<double>  parkelt_leadott_energia_2   = thrust::device_pointer_cast(parameter->_parkelt_foton.leadott_energia_2);
    
    parameter->_thrust._parkelt_fotoeff_szam    = thrust::device_pointer_cast(parameter->_parkelt_foton.fotoeff_szam);
    parameter->_thrust._parkelt_compton_szam    = thrust::device_pointer_cast(parameter->_parkelt_foton.compton_szam);
    //thrust::device_ptr<int>     parkelt_parkelt_szam    = thrust::device_pointer_cast(parameter->_parkelt_foton.parkelt_szam);
    
    
    parameter->_thrust._parkelt_volt_kolcsonhatas   = thrust::device_pointer_cast(parameter->_parkelt_foton.volt_kolcsonhatas);
    parameter->_thrust._parkelt_allapot_kapcsolo    = thrust::device_pointer_cast(parameter->_parkelt_foton.allapot_kapcsolo);
    
    parameter->_thrust._parkelt_detektor_energia   = thrust::device_pointer_cast(parameter->_parkelt_foton.detektor_energia);
    parameter->_thrust._parkelt_detektor_csatorna  = thrust::device_pointer_cast(parameter->_parkelt_foton.detektor_csatorna);
    
    parameter->_thrust._parkelt_foton_aram_1    = thrust::device_pointer_cast(parameter->_parkelt_foton.foton_aram_1);
    
    
    //teszteleshez
    parameter->_thrust._parkelt_hkrm_csoport    = thrust::device_pointer_cast(parameter->_parkelt_foton.hkrm_csoport);
    parameter->_thrust._parkelt_hkrm_totalis    = thrust::device_pointer_cast(parameter->_parkelt_foton.hkrm_totalis);
    parameter->_thrust._parkelt_lambda          = thrust::device_pointer_cast(parameter->_parkelt_foton.lambda);
    parameter->_thrust._parkelt_kiindulo_pont_x = thrust::device_pointer_cast(parameter->_parkelt_foton.kiindulo_pont.x);
    parameter->_thrust._parkelt_kiindulo_pont_y = thrust::device_pointer_cast(parameter->_parkelt_foton.kiindulo_pont.y);
    parameter->_thrust._parkelt_kiindulo_pont_z = thrust::device_pointer_cast(parameter->_parkelt_foton.kiindulo_pont.z);
    
    
    
    
    
    
    // pillanatnyi allapothoz:
    
    parameter->_thrust._pillanatnyi_energia         = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.energia);
    parameter->_thrust._pillanatnyi_elozo_hely_x    = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.elozo_hely.x);
    parameter->_thrust._pillanatnyi_elozo_hely_y    = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.elozo_hely.y);
    parameter->_thrust._pillanatnyi_elozo_hely_z    = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.elozo_hely.z);
    
    parameter->_thrust._pillanatnyi_elozo_irany_x   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.elozo_irany.x);
    parameter->_thrust._pillanatnyi_elozo_irany_y   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.elozo_irany.y);
    parameter->_thrust._pillanatnyi_elozo_irany_z   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.elozo_irany.z);
    
    parameter->_thrust._pillanatnyi_aktiv_foton         = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.aktiv_foton);
    parameter->_thrust._pillanatnyi_kolcsonhatas_tipus  = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.kolcsonhatas_tipus);
    parameter->_thrust._pillanatnyi_leadott_energia     = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.leadott_energia);
    
    parameter->_thrust._pillanatnyi_voxel           = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.voxel);
    //thrust::device_ptr<double>  leadott_energia_2   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.leadott_energia_2);
    
    
    parameter->_thrust._pillanatnyi_fotoeff_szam   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.fotoeff_szam);
    parameter->_thrust._pillanatnyi_compton_szam   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.compton_szam);
    parameter->_thrust._pillanatnyi_parkelt_szam   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.parkelt_szam);
    
    parameter->_thrust._pillanatnyi_volt_kolcsonhatas   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.volt_kolcsonhatas);
    parameter->_thrust._pillanatnyi_allapot_kapcsolo    = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.allapot_kapcsolo);
    
    parameter->_thrust._pillanatnyi_detektor_energia   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.detektor_energia);
    parameter->_thrust._pillanatnyi_detektor_csatorna  = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.detektor_csatorna);
    
    parameter->_thrust._pillanatnyi_foton_aram_1   = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.foton_aram_1);
    
    
    
    //teszteleshez
    parameter->_thrust._pillanatnyi_hkrm_csoport    = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.hkrm_csoport);
    parameter->_thrust._pillanatnyi_hkrm_totalis    = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.hkrm_totalis);
    parameter->_thrust._pillanatnyi_lambda          = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.lambda);
    
    parameter->_thrust._pillanatnyi_kiindulo_pont_x = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.kiindulo_pont.x);
    parameter->_thrust._pillanatnyi_kiindulo_pont_y = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.kiindulo_pont.y);
    parameter->_thrust._pillanatnyi_kiindulo_pont_z = thrust::device_pointer_cast(parameter->_pillanatnyi_foton.kiindulo_pont.z);
    
    
    
    
    
    
}


 