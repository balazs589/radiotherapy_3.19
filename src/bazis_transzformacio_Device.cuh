#ifndef BAZIS_TRANSZFORMACIO_DEVICE_CUH
#define BAZIS_TRANSZFORMACIO_DEVICE_CUH

#include "Vektor3D.cuh"


__device__ Vektor3D bazis_transzformacio_Device(Vektor3D tengely, Vektor3D elterules);


#endif
