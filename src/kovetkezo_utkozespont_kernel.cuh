#ifndef KOVETKEZO_UTKOZESPONT_KERNEL_CUH
#define KOVETKEZO_UTKOZESPONT_KERNEL_CUH

#include <curand_kernel.h>
#include "Allapot.cuh"
#include "Hataskeresztmetszet.cuh"

__global__ void kovetkezo_utkozespont_kernel(int N, curandState *state, Allapot foton, Hataskeresztmetszet szigma);


#endif
