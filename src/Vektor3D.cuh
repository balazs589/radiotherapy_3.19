#ifndef VEKTOR3D_CUH
#define VEKTOR3D_CUH




/*!
    \ingroup Osztalyok_goup
    
    \brief Egyszerű 3D vektor adatstruktúra.
    
    \details
    
*/

class Vektor3D
{
public:
    double x;      //!< 
    double y;      //!< 
    double z;      //!< 
};


#endif