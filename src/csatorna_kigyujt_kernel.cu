#include "csatorna_kigyujt_kernel.cuh"

#include "Hataskeresztmetszet.cuh"


/*!
\ingroup Kernel_fuggvenyek_group

\brief
Az utolsó lépések után mindig összegyűjtjük, hogy az adott csatornában hány darab foton adott le energiát.

\param [in] csatorna_listahossz Kigyűjtendő csatornák száma.
\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void csatorna_kigyujt_kernel(int csatorna_listahossz, Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < csatorna_listahossz)
    {
        int csatorna_sorszam    = szigma.csatorna_sorszam_gyujto[sorszam];
        int csatorna_beutesszam = szigma.csatorna_beutesszam_gyujto[sorszam];
        
        if(csatorna_sorszam != -1)
        {
            szigma.csatornak[csatorna_sorszam] += (long long int)(csatorna_beutesszam);
        }
        else
        {
            // (-1) sorszam -> olyan fotonok szama amik 0 energiat adtak le
            szigma.detektor_0_energias_fotonok_szama[0] += (long long int)(csatorna_beutesszam);
        }
        
    }
}
