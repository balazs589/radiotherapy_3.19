#include "dofespont_kernel.cuh"

#include "Allapot.cuh"
#include "Hataskeresztmetszet.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
A kocka felső lapját hol éri el a foton.

\param [in] N Egyszerre elindított fotonok száma
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\note
Csak akkor számol helyesen, ha a <b> felülről </b> érkező foton <b> eltalálja </b> a kocka <b> felső lapját </b>.

\details 

*/


// 
// egyeb esetek nincsenek megvizsgalva

__global__ void dofespont_kernel(int N, Allapot foton, Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam<N)
    {
        double x = foton.elozo_hely.x[sorszam];
        double y = foton.elozo_hely.y[sorszam];
        double z = foton.elozo_hely.z[sorszam];
        
        double vx = foton.elozo_irany.x[sorszam];
        double vy = foton.elozo_irany.y[sorszam];
        double vz = foton.elozo_irany.z[sorszam];
        
        double z_max = szigma.z_max;
        
        double t = (z_max - z) / vz;
        
        x += vx*t;
        y += vy*t;
        z = z_max;
        
        //-----------------------------------------
        // teszt
        //x = 25.0;
        //y = 25.0;
        //z = 25.0;
        //-----------------------------------------
        
        
        foton.elozo_hely.x[sorszam] = x;
        foton.elozo_hely.y[sorszam] = y;
        foton.elozo_hely.z[sorszam] = z;
        
        
        // megfelelo(!) kezdeti nyalab eseten minden foton a kocka felso lapjan lep be
        foton.aktiv_foton[sorszam] = 1;
        foton.kolcsonhatas_tipus[sorszam] = -1;
        foton.voxel[sorszam] = -1;
        foton.leadott_energia[sorszam] = 0.0;
        
        foton.fotoeff_szam[sorszam] = 0;
        foton.compton_szam[sorszam] = 0;
        foton.parkelt_szam[sorszam] = 0;
        
        foton.volt_kolcsonhatas[sorszam] = 0;
        foton.allapot_kapcsolo[sorszam]  = 0;       // kapcsolo 0-ra allitva
        
        
        foton.detektor_energia[sorszam] = 0.0;
        foton.detektor_csatorna[sorszam] = -1;
        
        
        //foton.foton_aram_1[sorszam + 0*N] = 0;
        for(int iii = 0; iii<6; iii++)
        {
            foton.foton_aram_1[sorszam + iii*N] = 0;
        }
        
    }
}
