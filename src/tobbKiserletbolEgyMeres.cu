#include "tobbKiserletbolEgyMeres.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"


# include <curand_kernel.h>
// http://stackoverflow.com/questions/23352122/including-thrust-sort-h-before-curand-kernel-h-gives-compilation-error
#include <thrust/reduce.h>


#include "cuRAND_beallitas_kernel.cuh"
#include "dozis_atlag_nullaz_meres_elejen_kernel.cuh"
#include "voxel_anyagtipus_beallitas_kernel.cuh"
#include "dozis_atlag_meghataroz_meres_vegen_kernel.cuh"

#include "tobbLejatszasbolEgyKiserlet.cuh"

/*!
\ingroup wrapper_fuggvenyek_group

\brief
Szimuláció futtatása GPU-n. Adott számú kísérlet során leadott dózisok
összesítése és ezek átlagának valamint szórásának meghatározása voxelenként.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void tobbKiserletbolEgyMeres(Adatok *parameter)
{
    check_cuda_errors(__FILE__, __LINE__);
    
    
    // meres kezdete:
    
    
    
    cuRAND_beallitas_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_devStates, parameter->_seed);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
/*----------------------------------------------------------------------------------------------------*/
    
// a meres elejen a dozisok atlagahoz es szorasahoz szukseges mennyisegeket nullazzuk   
    
    dozis_atlag_nullaz_meres_elejen_kernel<<<parameter->_voxel_gridSize, parameter->_voxel_blockSize>>>(parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
/*----------------------------------------------------------------------------------------------------*/
// a meres elejen minden egyes voxel anyagtipusanak beallitasa
    
    voxel_anyagtipus_beallitas_kernel<<<parameter->_voxel_gridSize, parameter->_voxel_blockSize>>>(parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
/*----------------------------------------------------------------------------------------------------*/
    // kiserletek 
    
    
    parameter->_van_aktiv_foton = 1;
    parameter->_lepes_szamlalo = 0;
    
    parameter->_parkelt_van_aktiv_foton = 1;
    parameter->_parkelt_lepes_szamlalo = 0;
    
    for(parameter->_kiserlet_index = 0; parameter->_kiserlet_index<parameter->_kiserletek_szama; parameter->_kiserlet_index++)
    {
        
        // kiserletek idejenek merese:
        cudaEventCreate(&parameter->_start_KISERLET);
        cudaEventCreate(&parameter->_stop_KISERLET);
        cudaEventRecord(parameter->_start_KISERLET,0);
        
        
        check_cuda_errors(__FILE__, __LINE__);
            tobbLejatszasbolEgyKiserlet(parameter);
        check_cuda_errors(__FILE__, __LINE__);
        
        
        cudaEventRecord(parameter->_stop_KISERLET,0);
        cudaEventSynchronize(parameter->_stop_KISERLET);
        cudaEventElapsedTime(&parameter->_time_KISERLET, parameter->_start_KISERLET, parameter->_stop_KISERLET);
        
        std::cout << parameter->_kiserlet_index+1 << ". KISERLET ELTELT IDO:\t" << parameter->_time_KISERLET << " ms" << std::endl; 
        
        
        std::cout << "maximum_lepes_szam = " << parameter->_maximum_lepes_szam << '\n' << std::endl;
        std::cout << "# ---------------------------------------------"  << '\n' << std::endl;
        
        
    } // end for(int parameter->_kiserlet_index = 0; parameter->_kiserlet_index<parameter->_kiserletek_szama; parameter->_kiserlet_index++)
    // az osszes kiserlet azaz a meres vege
    
    
    
    parameter->_osszes_leadott_energia = thrust::reduce(parameter->_thrust._dozis_atlag, parameter->_thrust._dozis_atlag+parameter->_d_szigma.voxelek_szama);
    
    check_cuda_errors(__FILE__, __LINE__);
    
//  double max_dozis = thrust::reduce(parameter->_thrust._dozis_atlag, parameter->_thrust._dozis_atlag+parameter->_d_szigma.voxelek_szama, -1.0, thrust::maximum<double>());
//  double min_dozis = thrust::reduce(parameter->_thrust._dozis_atlag, parameter->_thrust._dozis_atlag+parameter->_d_szigma.voxelek_szama, 1.0e100, thrust::minimum<double>());
    
    
    
// az utolso kiserlet utan meghatarozzuk a dozisok atlagat es szorasat
    
    dozis_atlag_meghataroz_meres_vegen_kernel<<<parameter->_voxel_gridSize, parameter->_voxel_blockSize>>>(parameter->_kiserletek_szama, parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    // meres vege
    
    
    
    
    check_cuda_errors(__FILE__, __LINE__);
    
}


 