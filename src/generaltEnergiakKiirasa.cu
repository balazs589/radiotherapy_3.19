#include "generaltEnergiakKiirasa.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"


#include "allapotMasolasGPUn.cuh"
#include "allapotKiirasHostrol.cuh"


#include <curand_kernel.h>
// http://stackoverflow.com/questions/23352122/including-thrust-sort-h-before-curand-kernel-h-gives-compilation-error
#include <thrust/sort.h>



/*!
\ingroup wrapper_fuggvenyek_group

\brief
A szimuláció pillanatnyi adatait energia szerint sorbarendező és szöveges fájlba kiíró függvény.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\param [in] fajlnev Irásra megnyitandó szöveges fájl neve.
\return Nincs.

\details 

*/

void generaltEnergiakKiirasa(Adatok *parameter, const char* fajlnev)
{
    std::cout << "\n\n" << std::endl;

    
    allapotMasolasGPUn(parameter); // pillanatnyi allapot masolasa ideiglenes helyre sorbarendezes elott

    
    // tapasztalati eloszlas kiirasahoz energia szerint (csokkeno) sorrendbe rakva az ideiglenes fotonokat
    thrust::sort_by_key(    parameter->_thrust._pillanatnyi_energia, parameter->_thrust._pillanatnyi_energia+parameter->fotonSzalakSzama, 
                            thrust::make_zip_iterator(make_tuple(   parameter->_thrust._pillanatnyi_aktiv_foton,
                                                                    parameter->_thrust._pillanatnyi_kolcsonhatas_tipus,
                                                                    parameter->_thrust._pillanatnyi_leadott_energia,
                                                                    parameter->_thrust._pillanatnyi_voxel,
                                                                    parameter->_thrust._pillanatnyi_elozo_hely_x,
                                                                    parameter->_thrust._pillanatnyi_elozo_hely_y,
                                                                    parameter->_thrust._pillanatnyi_elozo_hely_z,
                                                                    parameter->_thrust._pillanatnyi_elozo_irany_x,
                                                                    parameter->_thrust._pillanatnyi_elozo_irany_y,
                                                                    parameter->_thrust._pillanatnyi_elozo_irany_z))
//                            , thrust::greater<double>()
                            );
                            

    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    
    
    allapotKiirasHostrol(parameter, fajlnev);

    std::cout << "\n\n" << std::endl;

    
    
}


 