#include "cuRAND_beallitas_kernel.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
CuRAND legegyszerűbb véletlenszám generátorának kezdeti beállítása.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] state CUDA random generátor állapota.
\param [in] seed CUDA random generátor inicializáláshoz.
\return Nincs.

\details 

*/


__global__ void cuRAND_beallitas_kernel(int N, curandState *state, int seed)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < N)
    {
        curand_init(seed, sorszam, 0, &state[sorszam]);
    }
}

