#ifndef HATASKERESZTMETSZET_CUH
#define HATASKERESZTMETSZET_CUH



/*!
    \ingroup Osztalyok_goup
    
    \brief Vizsgált térfogatot kitöltő anyag leírásához.
    
    \details
    
*/

class Hataskeresztmetszet
{
public:
    
    /*! \name Az anyagot tartalmazó téglatest határoló lapjainak elhelyezkedése
    \{*/
    double  x_min;              //!< az 1. oldallap helyzete [cm]
    double  x_max;              //!< az 2. oldallap helyzete [cm]
    double  y_min;              //!< az 3. oldallap helyzete [cm]
    double  y_max;              //!< az 4. oldallap helyzete [cm]
    double  z_min;              //!< az 5. oldallap helyzete [cm]
    double  z_max;              //!< az 6. oldallap helyzete [cm]
    /*!\}*/
    
    
    /*! \name Voxelek mérete és száma
    \{*/
    double dx;                  //!< voxelek mérete x tengely mentén [cm]
    double dy;                  //!< voxelek mérete y tengely mentén [cm]
    double dz;                  //!< voxelek mérete z tengely mentén [cm]
    
    int nx;                     //!< voxelek száma x irányban
    int ny;                     //!< voxelek száma y irányban
    int nz;                     //!< voxelek száma z irányban
    
    int voxelek_szama;          //!< összes voxel száma  = nx*ny*nz
    /*!\}*/
    
    /*! \name Adott voxelt jellemző adatok
    \{*/
    int*    voxel_anyagtipus;   //!< milyen típusú anyag van a voxelben
    double* voxel_rho;          //!< voxel sűrűsége [g/cm3]
    
    double* dozis;              //!< egy kísérlet során a voxelben leadott összes energia [MeV]
    
    double* dozis_atlag;        //!< az összes kíserlet során a voxelben leadott átlagos energia, a mérés végeredménye [MeV]
    double* dozis_szoras;       //!< a voxelben leadott átlagenergia becsült standard hibája [MeV]
    /*!\}*/
    
    /*! \name Minden kölcsönhatás után a voxelek kigyűjtéséhez
    \{*/
    int*    sorszam_gyujto;     //!< a voxel sorszáma
    int*    beutesszam_gyujto;  //!< ebben a voxelben kölcsönható fotonok száma
    double* dozis_gyujto;       //!< ebben a voxelben leadott energia [MeV]
    
    double* kirepult_energia;   //!< vizsgált térfogatot elhagyó fotonok összenergiája [MeV]
    /*!\}*/
    
    
    /*! \name Az anyagi tulajdonságokat leíró adatok
    \{*/
    int     anyagfajtak_szama;  //!< hányféle különböző anyaggal vannak kitöltve a voxelek
    double* rho;                //!< anyagonként különböző default sűrűségeket tartalmazó tömb [g/cm3]
    
    int     sorok_szama;        //!< XCOM tablazat sorainak szama
    double* energia;            //!< XCOM energiacsoportok osztópontja  [MeV]
    
    double* fotoeff;            //!< XCOM táblázat fotoeffektusra vonatkozó oszlopa 
    double* compton;            //!< XCOM táblázat Compton-szórásra vonatkozó oszlopa
    double* parkelt;            //!< XCOM táblázat párkeltésre vonatkozó oszlopa
    double* totalis;            //!< XCOM táblázat totális hatáskeresztmetszetet tartalmazó oszlopa
    
    double* szupremum;          //!< Woodcock módszerhez makroszkópikus hatáskeresztmetszetek szuprémuma, minden energiához 1db
    /*!\}*/
    
    
    /*! \name Sokcsatornás energiadetektorhoz
    \{*/
    double csatorna_max_energia;        //!< legnagyobb detektor energia [MeV]
    double csatorna_energia_osztas;     //!< energiaintervallumok szélessége [MeV]
    int  csatornak_szama;               //!< energiaintervallumok száma
    
    long long int* csatornak;           //!< detektor csatornáinak beütés számai
    int* csatorna_sorszam_gyujto;       //!< az energialeadás során érintett csatornák sorszámai
    int* csatorna_beutesszam_gyujto;    //!< az adott sorszámú csatornában összeszámolt beütések száma
    
    long long int* detektor_0_energias_fotonok_szama;   //!< fotonok amik nem adtak le energiát életútjuk során
    /*!\}*/
    
    
    
    /*! \name Fotonáramok meghatározásához
    X-Y síkkal párhuzamos téglalap alakú felület melynek 2 oldala az X, másik 2 oldala az Y tengellyel párhuzamos.
    \{*/
    double felulet_x_min;           //!< téglalap egyik Y-tengellyel párhuzamos oldalvonalának helyzetét megadó koordináta
    double felulet_x_max;           //!< téglalap másik Y-tengellyel párhuzamos oldalvonalának helyzetét megadó koordináta
    double felulet_y_min;           //!< téglalap egyik X-tengellyel párhuzamos oldalvonalának helyzetét megadó koordináta
    double felulet_y_max;           //!< téglalap másik X-tengellyel párhuzamos oldalvonalának helyzetét megadó koordináta
    
    double felulet1_z;              //!< az 1. téglalap síkjának Z-tengellyel való metszéspontja
    double felulet2_z;              //!< az 2. téglalap síkjának Z-tengellyel való metszéspontja
    double felulet3_z;              //!< az 3. téglalap síkjának Z-tengellyel való metszéspontja
    double felulet4_z;              //!< az 4. téglalap síkjának Z-tengellyel való metszéspontja
    double felulet5_z;              //!< az 5. téglalap síkjának Z-tengellyel való metszéspontja
    double felulet6_z;              //!< az 6. téglalap síkjának Z-tengellyel való metszéspontja
    /*!\}*/
    
    
    
};


#endif
