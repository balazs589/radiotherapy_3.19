#include "csatorna_meghataroz_kernel.cuh"


#include "Allapot.cuh"
#include "Hataskeresztmetszet.cuh"


/*!
\ingroup Kernel_fuggvenyek_group

\brief
Az utolsó lépések után mindig meghatározzuk, hogy a foton által leadott energia melyik csatornába esik.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\param [in,out] parkelt_foton Párkeltésből származó második fotonok aktuális adatait elérő struktúra.
\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void csatorna_meghataroz_kernel(int N, Allapot foton, Allapot parkelt_foton, Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < N)
    {
        double detektor_energia = 0.0;
        detektor_energia += foton.detektor_energia[sorszam];
        detektor_energia += parkelt_foton.detektor_energia[sorszam];
        
        if(detektor_energia > 0.0)
        {
            int csatorna_sorszam = int((detektor_energia - 0.0)/szigma.csatorna_energia_osztas); 
            foton.detektor_csatorna[sorszam] = csatorna_sorszam;
        }
        
        
    }
}
