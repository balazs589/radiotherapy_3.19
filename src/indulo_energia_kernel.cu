#include "indulo_energia_kernel.cuh"


#include <curand_kernel.h>
#include "Allapot.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Foton induló energiájának sorsolása.

\param [in] N Egyszerre elindított fotonok száma.
\param [in] also_hatar Foton sorszám intervallum alsó határ.
\param [in] felso_hatar Foton sorszám intervallum felső határ.
\param [in] E1 Energia intervallum alsó osztópontja.
\param [in] E2 Energia intervallum felső osztópontja.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\param [in,out] state CUDA random generátor állapota.
\return Nincs.

\details 
( felso_hatar - also_hatar = [E1,E2] intervallumba sorsolt fotonok darabszáma )
*/

// 
// (0,1] intervallumon  egyenletes eloszlassal generalt veletlenszam felhasznalasaval
// lepcsos hisztogram szerint

__global__ void indulo_energia_kernel(int N, int also_hatar, int felso_hatar, double E1, double E2, Allapot foton, curandState *state)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if( (also_hatar <= sorszam) && (sorszam < felso_hatar) )
    {
        curandState localState = state[sorszam];

        double u = curand_uniform_double(&localState);
        
        foton.energia[(N-1)-sorszam] = E1 + (E2-E1)*u;      // hisztogram adott rekeszeben energiaban egyenletes eloszlas
                                                            // sorszam novekedesevel a nagyobb energiaktol a kisebbek fele haladva
        
        state[sorszam] = localState;
        
    }
}

