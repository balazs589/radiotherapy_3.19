#ifndef FOTON_ARAM_MEGHATAROZAS_KERNEL_CUH
#define FOTON_ARAM_MEGHATAROZAS_KERNEL_CUH

#include "Allapot.cuh"
#include "Hataskeresztmetszet.cuh"


__global__ void foton_aram_meghatarozas_kernel(int N, Allapot foton, Hataskeresztmetszet szigma);


#endif
