#ifndef CUDA_DEBUG_CUH
#define CUDA_DEBUG_CUH


#define CUDA_CALL(x) do { if((x) != cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__, __LINE__);\
    exit(EXIT_FAILURE);}} while(0) 




// check_cuda_errors() mellozese doxygen call graph-bol:
//#define check_cuda_errors(const char *filename, const int line_number)




#define DEBUG

inline void check_cuda_errors(const char *filename, const int line_number)
{

#ifdef DEBUG
    
    cudaDeviceSynchronize();
    cudaError_t error = cudaGetLastError();
    if(error != cudaSuccess)
    {
        printf("CUDA error at %s:%i: %s\n",filename, line_number, cudaGetErrorString(error));
        exit(EXIT_FAILURE);
    }
    return;
    
#endif



#ifndef DEBUG
    
    cudaDeviceSynchronize();
    return;
    
#endif

}


/**/






//#define ORA_INDIT
//#define ORA_MEGALLIT


// float time;                  //
// cudaEvent_t start, stop;     // az adott fuggvenyben vagy az Adatok strukturaban deklaralva

//#define   ORA_INDIT       cudaEventCreate(&start); cudaEventCreate(&stop); cudaEventRecord(start,0)
//#define   ORA_MEGALLIT    cudaEventRecord(stop,0); cudaEventSynchronize(stop); cudaEventElapsedTime(&time, start, stop); \
//                      std::cout << "# eltelt ido:\t" << time << " ms" << std::endl; \
//                      std::cout << "# ---------------------------------------------"  << '\n' <<std::endl



#endif

