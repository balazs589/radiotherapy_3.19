#include "fotoeffektus_kernel.cuh"


#include "Allapot.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Fotoeffektus során leadott energia meghatározása.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void fotoeffektus_kernel(int N, Allapot foton)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < N)
    {
        int aktiv_foton = foton.aktiv_foton[sorszam];
        int kolcsonhatas_tipus = foton.kolcsonhatas_tipus[sorszam];
        
        if((aktiv_foton == 1)&&(kolcsonhatas_tipus == 1))
        {
            foton.fotoeff_szam[sorszam] = 1;
            
            // kolcsonhatas tortent a kockan belul:
            foton.volt_kolcsonhatas[sorszam] = 1;       // esetleg lehetne +=1, ha az osszes kolcsonhatast ossze akarnank szamolni
            
            double regi_energia = foton.energia[sorszam];
            
            foton.leadott_energia[sorszam] = regi_energia;
            
            // foton energiak tallyzesehez, a megfelelo helyen kell kapcsolgatni:
            if(foton.allapot_kapcsolo[sorszam] == 1)
            {
                foton.detektor_energia[sorszam] += regi_energia;
            }
            
            foton.energia[sorszam] = 0.0;
            
            // kovetkezo lepesben mar nem kell szamolni ezekkel a fotonokkal
            foton.aktiv_foton[sorszam] = 0;
            
        }
    }
}
