#include "izotrop_kup_Device.cuh"



/*!
\ingroup Device_fuggvenyek_group

\brief
Kúpban 3D izotróp irány sorsolása.

\param [in] cos_alfa
Kúp fél nyílásszögének koszinusza.
\param [in,out] localState
CUDA random generátor állapota.

\return
Z-tengelyhez rögzített kúpban izotróp módon sorsolt irányvektor.

\details 
`cos_alfa` (fél nyilásszög koszinusza) paraméterrel jellemzett kúpban 3D izotróp iránysorsolás
(a tengely iránya a z-tengellyel esik egybe)

*/


__device__ Vektor3D izotrop_kup_Device(double cos_alfa, curandState* localState)
{
    Vektor3D izotrop_irany;
    
    izotrop_irany.z = curand_uniform_double(localState) * (1 - cos_alfa) + cos_alfa;
    
    double sin_theta = sqrt(1 - izotrop_irany.z * izotrop_irany.z);
    
    double fi = 2 * _PI_ * curand_uniform_double(localState);
    
    izotrop_irany.x = sin_theta * cos(fi);
    izotrop_irany.y = sin_theta * sin(fi);
    
    
    return izotrop_irany;
        
}
