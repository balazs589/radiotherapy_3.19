#include "kovetkezo_utkozespont_kernel.cuh"

#include <curand_kernel.h>
#include "Allapot.cuh"
#include "Hataskeresztmetszet.cuh"


#include <stdio.h>


/*!
\ingroup Kernel_fuggvenyek_group

\brief
Aktuális energia ismeretében makroszkópikus hatáskeresztmetszet meghatározása
és szabad úthossz sorsolása (Woodcock-módszerrel),
valamint a kockát nem elhagyó fotonok esetén kölcsönhatás sorsolása.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] state CUDA random generátor állapota.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void kovetkezo_utkozespont_kernel(int N, curandState *state, Allapot foton, Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam<N)
    {
        int aktiv_foton = foton.aktiv_foton[sorszam];
        
        if(aktiv_foton == 1)
        {
        
            curandState localState = state[sorszam];
            
            // foton aktualis koordinatainak es iranyanak beolvasasa
            double x = foton.elozo_hely.x[sorszam];
            double y = foton.elozo_hely.y[sorszam];
            double z = foton.elozo_hely.z[sorszam];
            double vx = foton.elozo_irany.x[sorszam];
            double vy = foton.elozo_irany.y[sorszam];
            double vz = foton.elozo_irany.z[sorszam];
            
            
            foton.kiindulo_pont.x[sorszam] = x;
            foton.kiindulo_pont.y[sorszam] = y;
            foton.kiindulo_pont.z[sorszam] = z;     // fotonaram meghatarozasahoz
            
            
            // a foton aktualis energiajabol energiacsoport meghatarozasa:
            double aktualis_energia = foton.energia[sorszam];   // [MeV]
            int energia_csoport = -1;                              // XCOM adatok melyik 2 sora kozott vagyunk
            
            for(int iii = 0; iii < szigma.sorok_szama; iii++)   // vegiglepkedunk a tablazat sorain
            {
                energia_csoport += (aktualis_energia > szigma.energia[iii] ? 1 : 0);
            }
            
            
            // XCOM tablazat osztopontjai kozott egyenes szakasszal kozelitve
            // interpolacios osztopont meghatarozasa:
            double osztopont = (aktualis_energia - szigma.energia[energia_csoport]) / (szigma.energia[energia_csoport+1] - szigma.energia[energia_csoport]);
            
            
            // (Woodcock) hkrm kiszamitasa             [cm2/g] 
            double szigma_szupremum = szigma.szupremum[energia_csoport] + (szigma.szupremum[energia_csoport+1] - szigma.szupremum[energia_csoport]) * osztopont;
            
            
            double egy_per_szigma_szupremum = 1.0 / szigma_szupremum;
            
            int hkrm_csoport = energia_csoport;
            
            
            int kolcsonhatas_tipus = 4;     // nincs meg meg, hogy milyen valodi kolcsonhatas kovetkezett be
                                            // 4 - virtualis
            
            double lambda = 0.0;
            int voxel = -1;
            double szigma_totalis = 0.0;
            
            
            while(kolcsonhatas_tipus == 4)
            {
                
                
                // szabad uthossz sorsolas (Woodcock):
                lambda = (-1.0) * egy_per_szigma_szupremum * log(curand_uniform_double(&localState));  // [cm]
                
                
    //----------------------------------------------
    // teszt: egyenletesen szetszorva [10 cm - 20 cm] kozott
    //
    //lambda = 10.0 + 10.0 * curand_uniform_double(&localState);
    //
    //----------------------------------------------
                
                
                x += lambda * vx;
                y += lambda * vy;
                z += lambda * vz;       // kovetkezo (egyelore virtualis) utkozespont koordinatai
                
                // megvizsgaljuk, hogy a meghatarozott utkozespont a vizsgalt terfogaton (kockan) belul van-e:
                if( !((szigma.x_min<x)&&(x<szigma.x_max)&&(szigma.y_min<y)&&(y<szigma.y_max)&&(szigma.z_min<z)&&(z<szigma.z_max)) )
                {
                    // ha a foton elhagyta a kockat
                    // minden szukseges adatot frissitunk
                    
                    foton.kolcsonhatas_tipus[sorszam] = 0;
                    foton.voxel[sorszam] = -1;
                    
                    foton.elozo_hely.x[sorszam] = x;
                    foton.elozo_hely.y[sorszam] = y;
                    foton.elozo_hely.z[sorszam] = z;
                    
                    state[sorszam] = localState;            // randomot is
                    
                    
                    //foton.allapot_kapcsolo[sorszam] = 1;    // kapcsolo bekapcsolva, ha kirepult a foton
                    
                    // majd visszaterunk a kernelbol
                    return;
                }
                
                
                // ha a virtualis utkozes a kockan belul van:                
                // meghatarozzuk a kovetkezo voxelt:   (x,y,z) => voxel szama
                // es az abban levo anyag tipusat az ahhoz tartozo totalis makroszkopikus hataskeresztmetszettel egyutt
                
                voxel =  int((x-szigma.x_min)/szigma.dx) + szigma.nx*int((y-szigma.y_min)/szigma.dy) + szigma.nx*szigma.ny*int((z-szigma.z_min)/szigma.dz);
                
                int voxel_anyagtipus = szigma.voxel_anyagtipus[voxel];
                
                // kulonbozo HKRM voxeltol fuggoen:
                hkrm_csoport = energia_csoport + voxel_anyagtipus * szigma.sorok_szama;
                
                szigma_totalis = szigma.totalis[hkrm_csoport] + (szigma.totalis[hkrm_csoport+1] - szigma.totalis[hkrm_csoport]) * osztopont;
                
                // voxelek sulyozasa mri kep jel intenzitassal:
                double voxel_rho = szigma.voxel_rho[voxel];
                szigma_totalis *= voxel_rho;
                
                // majd a totalis es virtualis hataskeresztmetszetek felhasznalasaval kisorsoljuk,
                // hogy valodi vagy virtualis kolcsonhatas tortent
                
                
    //----------------------------------------------
    // teszt: szamabrazolasok pontatlansaga miatt elofordulhat hoy homogen anyagnal is virtualis utkozes lesz
    // ezert feltetel kikommentezve:
    //if( curand_uniform_double(&localState) < (szigma_totalis*egy_per_szigma_szupremum) )
    //----------------------------------------------
                
                if( curand_uniform_double(&localState) < (szigma_totalis*egy_per_szigma_szupremum) )
                {
                    kolcsonhatas_tipus = 1;
                }
                
            
            }
            // while ciklusbol csak akkor lepunk ki, ha a kockan belul nem virtualis (==4) a kolcsonhatas tipusa
            
            
            
            // ekkor mar biztosan lesz valamekkora leadott energia ebben a voxelben
            foton.voxel[sorszam] = voxel;
            
            // beolvassuk a a megfelelo parcialis makroszkopikus hataskeresztmetszeteket
            
            double szigma_fotoeff = szigma.fotoeff[hkrm_csoport] + (szigma.fotoeff[hkrm_csoport+1] - szigma.fotoeff[hkrm_csoport]) * osztopont;
            double szigma_compton = szigma.compton[hkrm_csoport] + (szigma.compton[hkrm_csoport+1] - szigma.compton[hkrm_csoport]) * osztopont;
            
            
            // totalist mar egyszer beolvastuk, ezert 3-bol 2 is eleg a sorsolashoz
            //double szigma_parkelt = szigma.parkelt[hkrm_csoport] + (szigma.parkelt[hkrm_csoport+1] - szigma.parkelt[hkrm_csoport]) * osztopont;
            
            
            
            // ha a foton a kockan belul "utkozott", kolcsonhatast sorsolunk neki:
            
            double p1 = szigma_fotoeff / szigma_totalis;
            double p2 = szigma_compton / szigma_totalis;
            //double p3 = szigma_parkelt * egy_per_szigma_szupremum;
            
            //p1;
            p2 = p1 + p2;
            //p3 = p2 + p3;
            
            kolcsonhatas_tipus = 1;                 // 1,2,3 -> fotoeff,compton,parkelt
            double u = curand_uniform_double(&localState);
            if(u>p1) kolcsonhatas_tipus++;
            if(u>p2) kolcsonhatas_tipus++;
            //if(u>p3) kolcsonhatas_tipus++;
            
    //-----------------------------------------
    // teszt: csak egyfele kcsh. (3,2,1 -> parkelt,compton,fotoeff)
    //kolcsonhatas_tipus = 1;
    //-----------------------------------------
            
            foton.kolcsonhatas_tipus[sorszam] = kolcsonhatas_tipus;
            
            
            
            foton.elozo_hely.x[sorszam] = x;
            foton.elozo_hely.y[sorszam] = y;
            foton.elozo_hely.z[sorszam] = z;        // adatok frissitese            
            
            
            foton.allapot_kapcsolo[sorszam] = 1;    // kapcsolo bekapcsolva, ha volt valamilyen kolcsonhatas
            
            //foton.hkrm_csoport[sorszam] = hkrm_csoport;           // teszteleshez adatok kimentese
            //foton.hkrm_totalis[sorszam] = szigma_totalis;         // teszteleshez adatok kimentese
            //szigma_parkelt = szigma_parkelt * szigma.rho[2];      // teszt eleshez
            //foton.lambda[sorszam] = lambda;                       // teszteleshez adatok kimentese
            
            
            
            state[sorszam] = localState;
        }
    }
}
