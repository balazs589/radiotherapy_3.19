#include "voxel_nullaz_kiserletenkent_kernel.cuh"


#include "Hataskeresztmetszet.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Minden kísérlet elején minden voxelben 0 a leadott dózis.

\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void voxel_nullaz_kiserletenkent_kernel(Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < szigma.voxelek_szama)
    {
        //szigma.beutesek_szama[sorszam]    = 0;
        szigma.dozis[sorszam]           = 0.0;
    }
}
