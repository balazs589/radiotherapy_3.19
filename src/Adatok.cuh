#ifndef ADATOK_CUH
#define ADATOK_CUH

#include <fstream>
#include <curand_kernel.h>

#include "Hataskeresztmetszet.cuh"
#include "Korlap.cuh"
#include "Allapot.cuh"

#include "ThrustObjektumok.cuh"


/*!
    \ingroup Osztalyok_goup
    
    \brief A program által használt adatok összegyűjtésére használt osztály
    
    \details
    
*/


class Adatok
{
public:
    
    /*! \name Parancssori paraméterek adatainak
    \{*/
    int _argc;              //!< program bemeneti paramétereinek száma
    char** _argv;           //!< program bemeneti paramétereinek listája
    
    int fotonSzalakSzama;           //!< egyszerre elindított fotonok száma
    int _ciklusok_szama;            //!< kísérletenként ennyiszer indítunk fotonokat
    int _kiserletek_szama;          //!< ennyi darab kísérlet eredményéből számolunk átlagot
    double _kocka_meret;            //!< [cm] 
    double _voxel_meret;            //!< [cm] 
    double _kezdopont_x;            //!< [cm]
    double _kezdopont_y;            //!< [cm]
    double _kezdopont_z;            //!< [cm]
    double _kezdoirany_x;           //!< [cm]
    double _kezdoirany_y;           //!< [cm]
    double _kezdoirany_z;           //!< [cm]
    double _kor_sugara;             //!< [cm]
    double _divergencia_fok;        //!< [°]
    /*!\}*/
    
    
    /*! \name Kiírásokhoz fájlpointerek
    \{*/
    std::ofstream _statisztika;                 //!< eredményeket összesítése (szöveges fájl)
    
    std::ofstream _elmeleti_eloszlas;           //!< spektrum.txt input fájlnak megfelelően (szöveges fájl)
    std::ofstream _generalt_eloszlas;           //!< egyszerre elindított fotonok adatai energia szerint sorbarendezve (szöveges fájl)
    
    std::ofstream _pillanatnyi_allapot;         //!< egyszerre elindított fotonok adatai (szöveges fájl)
    
    std::ofstream _dozis_atlag_file;            //!< végeredmény: voxelenként leadott energia [MeV] (bináris fájl)
    std::ofstream _dozis_szoras_file;           //!< voxelenként leadott energia becsült standard hibája [MeV] (bináris fájl)
    
    std::ofstream _detektor_file;               //!< detektor energiaosztópontok [MeV] és beütésszámok [db] táblázata (szöveges fájl)
    /*!\}*/
    
    
    /*! \name Voxelek adatainak
    \{*/
    Hataskeresztmetszet _h_szigma;  //!< kockát kitöltő anyag tulajdonságait és geomeriáját leíró adatok host-on
    Hataskeresztmetszet _d_szigma;  //!< kockát kitöltő anyag tulajdonságait és geomeriáját leíró adatok device-on
    /*!\}*/
    
    
    /*! \name Fotonok adatainak
    \{*/
    Allapot _foton;                 //!< fotonok aktuális adatainak
    Allapot _parkelt_foton;         //!< párkeltésből származó második fotonok adatainak
    Allapot _pillanatnyi_foton;     //!< teszteléshez: fotonok aktuális állapotának kimentéséhez GPU-ra
    Allapot _host_foton;            //!< teszteléshez: fotonok aktuális állapotának kimentéséhez host-ra
    /*!\}*/
    
    
    /*! \name Foton nyaláb geometriája és energiaspektruma
    \{*/
    Korlap _kezdo_kor;              //!< gamma nyaláb kezdő keresztmetszetét megadó adatoknak
    
    double* _E_intervallumok;       //!< külön megadott fájlban lévő hisztogram energia adatainak (E [MeV])
    double* _fluxus;                //!< fluxus oszlop elemei spektrum.txt-ből
    int _spektrum_sorok_szama;      //!< spektrum.txt sorainak száma
    double _sum_fluxus;             //!< fluxus oszlop összege normáláshoz
    int* _foton_eloszlas;           //!< az adott energiánál kisebb energiájú fotonok száma (az egyszerre elindított  fotonok közül)
    /*!\}*/
    
    
    /*! \name Téglalap alakú fotonáram felületek síkjának z-, illetve oldalainak x- és y-koordinátái
    \{*/
    double _felulet1_z;             //!< [cm]
    double _felulet2_z;             //!< [cm]
    double _felulet3_z;             //!< [cm]
    double _felulet4_z;             //!< [cm]
    double _felulet5_z;             //!< [cm]
    double _felulet6_z;             //!< [cm]
    
    double _felulet_x_min;          //!< [cm]
    double _felulet_x_max;          //!< [cm]
    double _felulet_y_min;          //!< [cm]
    double _felulet_y_max;          //!< [cm]
    /*!\}*/
    
    
    
    /*! \name Sokcsatornás detekor üzemmód paraméterei
    \{*/
    double _csatorna_max_energia;       //!< legnagyobb detektált energia
    int _csatornak_szama;               //!< [0,`_csatorna_max_energia`] felosztása
    /*!\}*/
    
    
    /*! \name Számlálók és kapcsolók
    \{*/
    
    unsigned long long int _elso_lepesben_fotoeff_szam;     //!< parciális hatáskeresztmetszetek tesztjéhez
    unsigned long long int _elso_lepesben_compton_szam;     //!< parciális hatáskeresztmetszetek tesztjéhez
    unsigned long long int _elso_lepesben_parkelt_szam;     //!< parciális hatáskeresztmetszetek tesztjéhez
    unsigned long long int _elso_lepesben_kirepult_szam;    //!< parciális hatáskeresztmetszetek tesztjéhez
    
    
    unsigned long long int _osszes_elinditott_foton;    //!< foton indítási ciklusok végén növelve
    unsigned long long int _osszes_fotoeff_szam;        //!< foton indítási ciklusok végén növelve
    unsigned long long int _osszes_compton_szam;        //!< foton indítási ciklusok végén növelve
    unsigned long long int _osszes_parkelt_szam;        //!< foton indítási ciklusok végén növelve
    
    
    
    long long int _osszes_detektor_0_energias_fotonok_szama;        //!< összes detektorbeütést szummázva ezzel együtt kapjuk vissza `_osszes_elinditott_foton` értékét
    
    
    double _osszes_kirepult_energia;            //!< voxelekben leadott energiát szummázva ezzel együtt kapjuk meg `_osszes_elinditott_energia` értékét [MeV]
    
    long long int _foton_aram_szam_2[6];        //!< 6db foton áram felülethez
    
    double _osszes_elinditott_energia;          //!< [MeV]
    double _osszes_leadott_energia;             //!< voxelekben leadott energia szummája [MeV]
    
    
    int _van_aktiv_foton;               //!< 1 - ha az adott indítási ciklusban van még olyan foton ami a vizsgált térfogaton belül energiát képes még leadni
    int _lepes_szamlalo;                //!< egy indítási ciklusban eddig hány lépésben volt kölcsönhatás sorsolva
    
    int _parkelt_van_aktiv_foton;       //!< 1 - ha az adott indítási ciklusban van még olyan párkeltésből származó második foton ami a vizsgált térfogaton belül energiát képes még leadni
    int _parkelt_lepes_szamlalo;        //!< egy indítási ciklusban eddig hány lépésben volt kölcsönhatás sorsolva a párkeltésből származó második fotonoknak
    
    
    int _kiserlet_index;                //!< a mérés során hányadik kísérlet zajlik
    int _ciklus_index;                  //!< indítási ciklus számláló egy kísérleten belül
    
    int _maximum_lepes_szam;            //!< kisérletenként mennyi volt a legnagyobb lépésszám (csak első fotonokra)
    /*!\}*/
    
    
    /*! \name GPU futtatáshoz
    \{*/
    
    ThrustObjektumok _thrust;           //!< Thrust algoritmusokhoz használt objektumok összegyűjtve
    
    curandState* _devStates;            //!< CUDA random generátor állapota
    
    int _seed;                          //!< GPU random generátorának seed
    
    int _blockSize;                     //!< kernel hivások esetén az egy blokkban lévő szálak száma
    
    int _foton_blockSize;               //!< kernel hívás paramétere amikor az összes fotonon dolgozunk egyszerre
    int _foton_gridSize;                //!< kernel hívás paramétere amikor az összes fotonon dolgozunk egyszerre
    
    int _voxel_blockSize;               //!< kernel hívás paramétere amikor az összes voxelen dolgozunk egyszerre
    int _voxel_gridSize;                //!< kernel hívás paramétere amikor az összes voxelen dolgozunk egyszerre
    
    int _gyujto_blockSize;              //!< kernel hívás paramétere amikor a beütések kigyűjtése történik
    int _gyujto_gridSize;               //!< kernel hívás paramétere amikor a beütések kigyűjtése történik
    
    int _detektor_gyujto_blockSize;     //!< kernel hívás paramétere amikor a detektor csatornák kigyűjtése történik
    int _detektor_gyujto_gridSize;      //!< kernel hívás paramétere amikor a detektor csatornák kigyűjtése történik
    
    int _gyujto_meret;                  //!< egy lépésben legfeljebb ennyi különböző voxelben történhet energialeadás (max{fotonszam,voxelszam})
    
    int _csatorna_gyujto_meret;         //!< detektor csatornákhoz
    
    int _lista2_hossz;                  //!< beütések kigyűjtésekor azoknak a voxeleknek száma amelyekben volt energialeadás az aktuális lépésben
    int _lista3_hossz;                  //!< detektor csatornák kigyűjtésekor azoknak a csatornáknak a száma amelyekben volt energialeadás
    
    
    /*!\}*/
    
    
    /*! \name Futásidő méréséhez
    \{*/
    float _time_MERES;                  //!< mérés összideje [ms]
    cudaEvent_t _start_MERES;           //!< 
    cudaEvent_t _stop_MERES;            //!< 
    
    float _time_KISERLET;               //!< egy kisérlet ideje [ms]
    cudaEvent_t _start_KISERLET;        //!< 
    cudaEvent_t _stop_KISERLET;         //!< 
    /*!\}*/
    
    
    
    
};


#endif
