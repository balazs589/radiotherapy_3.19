#include "comptonszoras_kernel.cuh"

#include <curand_kernel.h>
#include "Allapot.cuh"

#include "constants.h"      // pi es elektron nyugalmi energia
#include "Vektor3D.cuh"
#include "kahn_sorsolas_Device.cuh"
#include "bazis_transzformacio_Device.cuh"


/*!
\ingroup Kernel_fuggvenyek_group

\brief
Compton-szórásban történt változások (új energia és irány) meghatározása.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] state CUDA random generátor állapota.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void comptonszoras_kernel(int N, curandState *state, Allapot foton)
{   
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < N)
    {
        
        int aktiv_foton = foton.aktiv_foton[sorszam];
        int kolcsonhatas_tipus = foton.kolcsonhatas_tipus[sorszam];
        
        if((aktiv_foton == 1)&&(kolcsonhatas_tipus == 2))
        {
            
            foton.compton_szam[sorszam] += 1;
            
            // kolcsonhatas tortent a kockan belul:
            foton.volt_kolcsonhatas[sorszam] = 1;       // esetleg lehetne +=1, ha az osszes kolcsonhatast ossze akarnank szamolni
            
            curandState localState = state[sorszam];
            
            
            
            double regi_energia = foton.energia[sorszam];
            double compton_hullamhossz = NYUGALMI_ENERGIA / regi_energia;
            double X = kahn_sorsolas_Device(compton_hullamhossz, &localState);
            double uj_energia = regi_energia / X;
            
            foton.leadott_energia[sorszam] = (regi_energia - uj_energia);
            
            // foton energiak tallyzesehez, a megfelelo helyen kell kapcsolgatni:
            if(foton.allapot_kapcsolo[sorszam] == 1)
            {
                foton.detektor_energia[sorszam] += (regi_energia - uj_energia);
            }
            
            foton.energia[sorszam] = uj_energia;
            
            double cos_theta = 1.0 + (1.0 - X)*compton_hullamhossz;
            double sin_theta = sqrt(1.0 - cos_theta*cos_theta);
            double fi = 2.0 * _PI_ * curand_uniform_double(&localState);
            
            Vektor3D elterules_iranya;
            
            elterules_iranya.x = sin_theta * cos(fi);
            elterules_iranya.y = sin_theta * sin(fi);
            elterules_iranya.z = cos_theta;
            
            Vektor3D tengely_irany;
            
            tengely_irany.x = foton.elozo_irany.x[sorszam];
            tengely_irany.y = foton.elozo_irany.y[sorszam];
            tengely_irany.z = foton.elozo_irany.z[sorszam];
            
            tengely_irany = bazis_transzformacio_Device(tengely_irany, elterules_iranya);
            
            //-----------------------------------------
            // kikommentezve: teszt: nincs iranyvaltozas
            foton.elozo_irany.x[sorszam] = tengely_irany.x;
            foton.elozo_irany.y[sorszam] = tengely_irany.y;
            foton.elozo_irany.z[sorszam] = tengely_irany.z;
            // teszt: nincs iranyvaltozas
            //-----------------------------------------
            
            
    //-----------------------------------------
    //teszt: kocka aljara teleportal a foton
    //foton.elozo_hely.x[sorszam] = x;
    //foton.elozo_hely.y[sorszam] = y;
    //foton.elozo_hely.z[sorszam] = 0.0000001;
    //-----------------------------------------
            
            
            
            state[sorszam] = localState;
            
        }
    }
    
}
