#ifndef VOXEL_KIGYUJT_LEPESENKENT_KERNEL_CUH
#define VOXEL_KIGYUJT_LEPESENKENT_KERNEL_CUH

#include "Hataskeresztmetszet.cuh"


__global__ void voxel_kigyujt_lepesenkent_kernel(int listahossz, Hataskeresztmetszet szigma);


#endif
