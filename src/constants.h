#ifndef CONSTANTS
#define CONSTANTS


/*! \ingroup Konstansok_group */
#define NYUGALMI_ENERGIA 0.510998928        //!< \brief elektron nyugalmi energiája MeV-ben, forrás: http://physics.nist.gov/cgi-bin/cuu/Value?mec2mev

/*! \ingroup Konstansok_group */
#define _PI_ 3.141592653589793              //!<


#endif


