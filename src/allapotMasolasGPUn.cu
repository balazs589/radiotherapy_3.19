#include "allapotMasolasGPUn.cuh"


#include "Adatok.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()


/*!
\ingroup wrapper_fuggvenyek_group

\brief
Fotonok pillanatnyi állapotát megadó adatok külön memóriaterületre másolása GPU-n sorba rendezéshez.


\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void allapotMasolasGPUn(Adatok *parameter)
{
    
    std::cout << std::endl;
    std::cout << "fotonok aktualis allapotanak masolasa GPUn...\n" << std::endl;
    
    check_cuda_errors(__FILE__, __LINE__);
    
    
     
    // a fotonokat leiro adatok masolasa
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.energia,            parameter->_foton.energia,            parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.elozo_hely.x,       parameter->_foton.elozo_hely.x,       parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.elozo_hely.y,       parameter->_foton.elozo_hely.y,       parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.elozo_hely.z,       parameter->_foton.elozo_hely.z,       parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.elozo_irany.x,      parameter->_foton.elozo_irany.x,      parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.elozo_irany.y,      parameter->_foton.elozo_irany.y,      parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.elozo_irany.z,      parameter->_foton.elozo_irany.z,      parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.aktiv_foton,        parameter->_foton.aktiv_foton,        parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.kolcsonhatas_tipus, parameter->_foton.kolcsonhatas_tipus, parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToDevice));
    
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.leadott_energia,    parameter->_foton.leadott_energia,    parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.voxel,              parameter->_foton.voxel,              parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToDevice));
    
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.fotoeff_szam,       parameter->_foton.fotoeff_szam,       parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.compton_szam,       parameter->_foton.compton_szam,       parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.parkelt_szam,       parameter->_foton.parkelt_szam,       parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToDevice));
    
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.volt_kolcsonhatas,  parameter->_foton.volt_kolcsonhatas,  parameter->fotonSzalakSzama*sizeof(int),    cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.allapot_kapcsolo,   parameter->_foton.allapot_kapcsolo,   parameter->fotonSzalakSzama*sizeof(int),    cudaMemcpyDeviceToDevice));
    
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.foton_aram_1,       parameter->_foton.foton_aram_1,       6*parameter->fotonSzalakSzama*sizeof(int),    cudaMemcpyDeviceToDevice));
    
    
    //teszteleshez
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.hkrm_csoport,       parameter->_foton.hkrm_csoport,       parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.hkrm_totalis,       parameter->_foton.hkrm_totalis,       parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.lambda,             parameter->_foton.lambda,             parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.kiindulo_pont.x,    parameter->_foton.kiindulo_pont.x,    parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.kiindulo_pont.y,    parameter->_foton.kiindulo_pont.y,    parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    CUDA_CALL(cudaMemcpy(parameter->_pillanatnyi_foton.kiindulo_pont.z,    parameter->_foton.kiindulo_pont.z,    parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToDevice));
    
    
    check_cuda_errors(__FILE__, __LINE__);
    
    
    std::cout << "masolas elkeszult\n" << std::endl;
    std::cout << "-------------------------------------" << std::endl;
    std::cout << std::endl;
    
    
    
}


 