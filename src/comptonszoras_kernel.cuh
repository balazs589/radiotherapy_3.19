#ifndef COMPTONSZORAS_KERNEL_CUH
#define COMPTONSZORAS_KERNEL_CUH

#include <curand_kernel.h>
#include "Allapot.cuh"


__global__ void comptonszoras_kernel(int N, curandState *state, Allapot foton);


#endif
