#include "marsaglia_izotrop_tesztelo_kernel.cuh"


#include <curand_kernel.h>
#include "Allapot.cuh"
#include "Vektor3D.cuh"
#include "marsaglia_izotrop_irany_Device.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
`marsaglia_izotrop_irany_Device()` fuggveny tesztelesehez

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] state CUDA random generátor állapota.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 

*/

// 

__global__ void marsaglia_izotrop_tesztelo_kernel(int N, curandState *state, Allapot foton)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam<N)
    {
        curandState localState = state[sorszam];
        
        Vektor3D izotrop_irany = marsaglia_izotrop_irany_Device(&localState);
        
        foton.elozo_irany.x[sorszam] = izotrop_irany.x;
        foton.elozo_irany.y[sorszam] = izotrop_irany.y;
        foton.elozo_irany.z[sorszam] = izotrop_irany.z;
        
        state[sorszam] = localState;
    }
}
