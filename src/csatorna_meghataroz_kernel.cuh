#ifndef CSATORNA_MEGHATAROZ_KERNEL_CUH
#define CSATORNA_MEGHATAROZ_KERNEL_CUH

#include "Allapot.cuh"
#include "Hataskeresztmetszet.cuh"

__global__ void csatorna_meghataroz_kernel(int N, Allapot foton, Allapot parkelt_foton, Hataskeresztmetszet szigma);


#endif
