#include "parkelt_foton_nullaz_kernel.cuh"

#include "Allapot.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Párkeltésből származó második fotonok számára memóriaterületek előkészítése.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] parkelt_foton Párkeltésből származó második fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void parkelt_foton_nullaz_kernel(int N, Allapot parkelt_foton)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam<N)
    {
        
        parkelt_foton.elozo_hely.x[sorszam] = 0.0;
        parkelt_foton.elozo_hely.y[sorszam] = 0.0;
        parkelt_foton.elozo_hely.z[sorszam] = 0.0;
        
        parkelt_foton.elozo_irany.x[sorszam] = 0.0;
        parkelt_foton.elozo_irany.y[sorszam] = 0.0;
        parkelt_foton.elozo_irany.z[sorszam] = 0.0;
        
        parkelt_foton.energia[sorszam] = 0.0;
        parkelt_foton.aktiv_foton[sorszam] = 0;
        parkelt_foton.kolcsonhatas_tipus[sorszam] = -1;
        parkelt_foton.voxel[sorszam] = -1;
        parkelt_foton.leadott_energia[sorszam] = 0.0;
        
        parkelt_foton.fotoeff_szam[sorszam] = 0;
        parkelt_foton.compton_szam[sorszam] = 0;
        
        parkelt_foton.volt_kolcsonhatas[sorszam] = 1;       // fotonaram szamitaskor nem kolcsonhatas nelkulinek tekintve ezeket a fotonokat
        parkelt_foton.allapot_kapcsolo[sorszam]  = 0;       // kapcsolo 0-ra allitva
        
        
        parkelt_foton.detektor_energia[sorszam] = 0.0;
        parkelt_foton.detektor_csatorna[sorszam] = -1;
        
        
        
        for(int iii = 0; iii<6; iii++)
        {
            parkelt_foton.foton_aram_1[sorszam + iii*N] = 0;
        }
        
        
    }
}
