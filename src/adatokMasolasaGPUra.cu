#include "adatokMasolasaGPUra.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"



/*!
\ingroup wrapper_fuggvenyek_group

\brief
A szükséges input adatoknak a grafikus kártya globális memóriájába másolása.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void adatokMasolasaGPUra(Adatok *parameter)
{
    
    
    
    // beolvasott XCOM adatok masolasa GPU-ra
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.rho,          parameter->_h_szigma.rho,       parameter->_d_szigma.anyagfajtak_szama                                  * sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.energia,      parameter->_h_szigma.energia,   parameter->_d_szigma.sorok_szama                                        * sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.fotoeff,      parameter->_h_szigma.fotoeff,   parameter->_d_szigma.anyagfajtak_szama*parameter->_d_szigma.sorok_szama * sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.compton,      parameter->_h_szigma.compton,   parameter->_d_szigma.anyagfajtak_szama*parameter->_d_szigma.sorok_szama * sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.parkelt,      parameter->_h_szigma.parkelt,   parameter->_d_szigma.anyagfajtak_szama*parameter->_d_szigma.sorok_szama * sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.totalis,      parameter->_h_szigma.totalis,   parameter->_d_szigma.anyagfajtak_szama*parameter->_d_szigma.sorok_szama * sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.szupremum,    parameter->_h_szigma.szupremum, parameter->_d_szigma.sorok_szama                                        * sizeof(double), cudaMemcpyHostToDevice));
    
    // minta voxel sulyok (voxelek_suruseg.dat-bol) GPU-ra:
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.voxel_rho,    parameter->_h_szigma.voxel_rho, parameter->_d_szigma.voxelek_szama * sizeof(double), cudaMemcpyHostToDevice));
    
    
    // osszes kirepult energiahoz:
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.kirepult_energia, &parameter->_osszes_kirepult_energia, 1*sizeof(double), cudaMemcpyHostToDevice));
    
    
    
    CUDA_CALL(cudaMemcpy( parameter->_d_szigma.csatornak, parameter->_h_szigma.csatornak, parameter->_h_szigma.csatornak_szama * sizeof(long long int), cudaMemcpyHostToDevice));
    
    // detektorban 0 energiat leado fotonok
    CUDA_CALL( cudaMemcpy( parameter->_d_szigma.detektor_0_energias_fotonok_szama, &parameter->_osszes_detektor_0_energias_fotonok_szama, 1*sizeof(long long int), cudaMemcpyHostToDevice));
    
    
    
    
    
}


 