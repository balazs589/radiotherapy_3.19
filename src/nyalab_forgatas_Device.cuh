#ifndef NYALAB_FORGATAS_DEVICE_CUH
#define NYALAB_FORGATAS_DEVICE_CUH

#include "Vektor3D.cuh"


__device__ Vektor3D nyalab_forgatas_Device(Vektor3D t, Vektor3D v);


#endif
