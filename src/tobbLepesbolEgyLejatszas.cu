#include "tobbLepesbolEgyLejatszas.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"


# include <curand_kernel.h>
// http://stackoverflow.com/questions/23352122/including-thrust-sort-h-before-curand-kernel-h-gives-compilation-error


//#include <thrust/device_vector.h>
//#include <thrust/host_vector.h>
#include <thrust/sort.h>

#include <thrust/reduce.h>
//#include <thrust/pair.h>
#include <thrust/iterator/constant_iterator.h>





#include "indulo_energia_kernel.cuh"
#include "korlap_kernel.cuh"
#include "divergalo_nyalab_kernel.cuh"
#include "dofespont_kernel.cuh"

#include "parkelt_foton_nullaz_kernel.cuh"


#include "generaltEnergiakKiirasa.cuh"

#include "lepesElsoFotonnak.cuh"
#include "lepesParkeltesFotonnak.cuh"

#include "csatorna_meghataroz_kernel.cuh"
#include "csatorna_kigyujt_kernel.cuh"

/*!
\ingroup wrapper_fuggvenyek_group

\brief
Fotonok indítása, életutak végigjátszása (dózisok összegzése első és másidik foton esetén is),
detektor-csatornák kigyűjtése.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void tobbLepesbolEgyLejatszas(Adatok *parameter)
{
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    parameter->_lepes_szamlalo  = -1;   // pillanatnyi allapot kiirasakor while cikluson kivul vagyunk

    // a koordinatak sorsolasahoz is hasznalt veletlenszam generator hasznalataval
    // a hisztogram egyes rekeszeiben energiaban egyenletes eloszlast hozunk letre
    for(int iii = 1; iii < parameter->_spektrum_sorok_szama; iii++ )
    {
        indulo_energia_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_foton_eloszlas[iii-1], parameter->_foton_eloszlas[iii], parameter->_E_intervallumok[iii-1], parameter->_E_intervallumok[iii], parameter->_foton, parameter->_devStates);
    }
    check_cuda_errors(__FILE__, __LINE__);
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    // kiindulo koordinatakat sorsolunk:
    
    
    // fotonok kezdo koordinatainak legeneralasa
    korlap_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_devStates,  parameter->_kezdo_kor, parameter->_foton);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    divergalo_nyalab_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_devStates,  parameter->_kezdo_kor, parameter->_foton);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    // kocka felso lapjan belepesi pont meghatarozasa
    dofespont_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_foton, parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    parkelt_foton_nullaz_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_parkelt_foton);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    parameter->_osszes_elinditott_energia += thrust::reduce(parameter->_thrust._energia, parameter->_thrust._energia+parameter->fotonSzalakSzama);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    // ki lehet iratni barmelyik ciklus kezdeti adatait (vagy ugyanez dofespont_kernel elott ):
    if( (parameter->_kiserlet_index == 3) && (parameter->_ciklus_index == 3) )
    {
      const char generalt_energiak_kiir_fajlnev[] ="./output/generalt_adatok/adatok/generalt_eloszlas.txt";
      
      generaltEnergiakKiirasa(parameter,generalt_energiak_kiir_fajlnev);
    }
    
    
    parameter->_van_aktiv_foton = 1;
    parameter->_lepes_szamlalo  = 0;
    
    
/*----------------------------------------------------------------------------------------------------*/
    // elso fotonok vegigszamolasa (parkeltes masodik fotonjai kulon ciklusban) 
    while(parameter->_van_aktiv_foton == 1)
    {
    
    parameter->_lepes_szamlalo++;
    
    if(parameter->_maximum_lepes_szam < parameter->_lepes_szamlalo)
    {
        parameter->_maximum_lepes_szam = parameter->_lepes_szamlalo;
    }
    
    check_cuda_errors(__FILE__, __LINE__);
        lepesElsoFotonnak(parameter);
    check_cuda_errors(__FILE__, __LINE__);
    
    } // end while(parameter->_van_aktiv_foton == 1)
    
    
    
/*----------------------------------------------------------------------------------------------------*/
    
// PARKELTESBOL SZARMAZO FOTONOK
// az elso fotonokkal azonos modon
    
    parameter->_parkelt_van_aktiv_foton = thrust::reduce(parameter->_thrust._parkelt_aktiv_foton, parameter->_thrust._parkelt_aktiv_foton+parameter->fotonSzalakSzama, -1, thrust::maximum<int>());
    check_cuda_errors(__FILE__, __LINE__);
    
    
    parameter->_parkelt_lepes_szamlalo = 0;
    
/*----------------------------------------------------------------------------------------------------*/
    
    while(parameter->_parkelt_van_aktiv_foton == 1)
    {
        parameter->_parkelt_lepes_szamlalo++;
    
        check_cuda_errors(__FILE__, __LINE__);
            lepesParkeltesFotonnak(parameter);
        check_cuda_errors(__FILE__, __LINE__);
    
    
    } // end while(parameter->_parkelt_van_aktiv_foton == 1)
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    parameter->_osszes_elinditott_foton += parameter->fotonSzalakSzama;   // szamoljuk, hogy mennyi fotont inditottunk el osszesen
    
    // es hogy az osszes elinditott Foton milyen kolcsonhatas tipusokbol mennyit hoz letre:
    parameter->_osszes_fotoeff_szam += thrust::reduce(parameter->_thrust._fotoeff_szam, parameter->_thrust._fotoeff_szam+parameter->fotonSzalakSzama);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    parameter->_osszes_compton_szam += thrust::reduce(parameter->_thrust._compton_szam, parameter->_thrust._compton_szam+parameter->fotonSzalakSzama);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    parameter->_osszes_parkelt_szam += thrust::reduce(parameter->_thrust._parkelt_szam, parameter->_thrust._parkelt_szam+parameter->fotonSzalakSzama);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    //osszes_parkelt_fotoeff_szam += thrust::reduce(parameter->_thrust._parkelt_fotoeff_szam, parameter->_thrust._parkelt_fotoeff_szam+parameter->fotonSzalakSzama);
    parameter->_osszes_fotoeff_szam += thrust::reduce(parameter->_thrust._parkelt_fotoeff_szam, parameter->_thrust._parkelt_fotoeff_szam+parameter->fotonSzalakSzama);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    //osszes_parkelt_compton_szam += thrust::reduce(parameter->_thrust._parkelt_compton_szam, parameter->_thrust._parkelt_compton_szam+parameter->fotonSzalakSzama);
    parameter->_osszes_compton_szam += thrust::reduce(parameter->_thrust._parkelt_compton_szam, parameter->_thrust._parkelt_compton_szam+parameter->fotonSzalakSzama);
    check_cuda_errors(__FILE__, __LINE__);
    

    // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    // DETEKTOR
    
    
    // detektor beutesek osszegyujtese
    csatorna_meghataroz_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_foton, parameter->_parkelt_foton, parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    // detektor_csatorna beutesszamok osszegyujtesehez, detektor_csatorna -kat novekvo sorrendbe rakva
    thrust::sort( parameter->_thrust._detektor_csatorna, parameter->_thrust._detektor_csatorna + parameter->fotonSzalakSzama );
    check_cuda_errors(__FILE__, __LINE__);
    
    
    // osszegyujtjuk, hogy melyik csatornan hany darab foton adott le energiat:
    parameter->_thrust._lista3_vege = thrust::reduce_by_key(parameter->_thrust._detektor_csatorna, parameter->_thrust._detektor_csatorna+parameter->fotonSzalakSzama, thrust::constant_iterator<int>(1), parameter->_thrust._csatorna_sorszam_gyujto, parameter->_thrust._csatorna_beutesszam_gyujto);
    check_cuda_errors(__FILE__, __LINE__);
    
    parameter->_lista3_hossz = parameter->_thrust._lista3_vege.first - parameter->_thrust._csatorna_sorszam_gyujto;
    
    
    
    parameter->_detektor_gyujto_blockSize = parameter->_blockSize;
    parameter->_detektor_gyujto_gridSize = parameter->_lista3_hossz / parameter->_detektor_gyujto_blockSize + ( (parameter->_lista3_hossz % parameter->_detektor_gyujto_blockSize == 0) ? 0:1);
    
    // az adott lepesben tortent beuteseket feljegyezzuk a csatornak teljes listajaban:
    csatorna_kigyujt_kernel<<<parameter->_detektor_gyujto_gridSize, parameter->_detektor_gyujto_blockSize>>>(parameter->_lista3_hossz, parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    check_cuda_errors(__FILE__, __LINE__);
    
}


 