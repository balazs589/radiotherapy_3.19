#include "allapotKiirasHostrol.cuh"

#include "Adatok.cuh"

#include <fstream>
#include <iomanip>


#include "allapotMasolasHostra.cuh"


/*!
\ingroup CPU_fuggvenyek_group

\brief
Fotonok pillanatnyi állapotát megadó adatok kiírása szöveges fájlba.

\param [in,out] parameter Ezen a változón keresztül érhető el a program összes globálisan használt adata.
\param [in] fajlnev Írásra megnyitandó szöveges fájl neve.
\return Nincs.

\details 

*/

void allapotKiirasHostrol(Adatok *parameter, const char* fajlnev)
{
    
    
    allapotMasolasHostra(parameter);


    parameter->_pillanatnyi_allapot.open(fajlnev);
     
     
     
     // fotonok aktualis allapotanak kiirasa
    std::cout << std::endl;
     
    std::cout << "fotonok pillanatnyi allapotanak kiirasa HDD-re...\n" << std::endl;
    std::cout << "kiserlet index: " << parameter->_kiserlet_index << std::endl;
    std::cout << "ciklus   index: " << parameter->_ciklus_index << std::endl;
    std::cout << "lepes szamlalo: " << parameter->_lepes_szamlalo << std::endl;

    
    
    parameter->_pillanatnyi_allapot << "# fotonok aktualis allapota:" << std::endl;
    parameter->_pillanatnyi_allapot << "# kiserlet index: " << parameter->_kiserlet_index << std::endl;
    parameter->_pillanatnyi_allapot << "# ciklus   index: " << parameter->_ciklus_index << std::endl;
    parameter->_pillanatnyi_allapot << "# lepes szamlalo: " << parameter->_lepes_szamlalo << std::endl;
    parameter->_pillanatnyi_allapot << "# elinditott fotonok szama : " << parameter->fotonSzalakSzama <<  std::endl;
    parameter->_pillanatnyi_allapot << "# -------------------------------------" << std::endl;



    parameter->_pillanatnyi_allapot

            << "#sorszam   "    << "\t"            
    
            << "energia    "    << "\t"            
            << "hely.x     "    << "\t"       
            << "hely.y     "    << "\t"       
            << "hely.z     "    << "\t"       
            << "irany.x    "    << "\t"      
            << "irany.y    "    << "\t"      
            << "irany.z    "    << "\t"      
                                                        
            << "aktiv"    << "\t"        
            << "kcsh"    << "\t" 
                                                       
            << "leadottE   "    << "\t"    
            << "voxel"    //<< "\t"
/*                                                        
            << "fotoeff    "    << "\t"       
            << "compton    "    << "\t"       
            << "parkelt    "    << "\t"       
                                                        
                                                        
            << "foton_aram "    << "\t"       
                                                        
                                                        
            //                              
            << "hkrm_csop  "    << "\t"       
            << "hkrm_total "    << "\t"       
            << "lambda     "    << "\t"             
            << "kiindulo.x "    << "\t"    
            << "kiindulo.y "    << "\t"    
            << "kiindulo.z "    //<< "\t"    
*/            
            << std::endl;
            
            
    parameter->_pillanatnyi_allapot << "# -----------------------------------" << std::endl;
    
    parameter->_pillanatnyi_allapot << std::scientific;
    //parameter->_pillanatnyi_allapot << std::fixed;
    //parameter->_pillanatnyi_allapot.unsetf(std::ios_base::floatfield);
    //int prec = parameter->_pillanatnyi_allapot.precision();
    //std::cout << "generalt eloszlas precision: "<< parameter->_pillanatnyi_allapot.precision() << std::endl;
    parameter->_pillanatnyi_allapot << std::setprecision(4);
    parameter->_pillanatnyi_allapot << std::showpos;
    //parameter->_pillanatnyi_allapot << std::noshowpos;
    
    
    //parameter->_generalt_eloszlas << parameter->_E_intervallumok[0] << "\t" << parameter->_fluxus[0]   << std::endl;

    
    for(int iii = 0; iii < parameter->fotonSzalakSzama; iii++)
    {
        parameter->_pillanatnyi_allapot
        
            << (iii+1)/double(parameter->fotonSzalakSzama)      << "\t"            

            << parameter->_host_foton.energia[iii]              << "\t"            
            << parameter->_host_foton.elozo_hely.x[iii]         << "\t"       
            << parameter->_host_foton.elozo_hely.y[iii]         << "\t"       
            << parameter->_host_foton.elozo_hely.z[iii]         << "\t"       
            << parameter->_host_foton.elozo_irany.x[iii]        << "\t"      
            << parameter->_host_foton.elozo_irany.y[iii]        << "\t"      
            << parameter->_host_foton.elozo_irany.z[iii]        << "\t"      
                                                        
            << parameter->_host_foton.aktiv_foton[iii]          << "\t"        
            << parameter->_host_foton.kolcsonhatas_tipus[iii]   << "\t" 
                                                        
            << parameter->_host_foton.leadott_energia[iii]      << "\t"    
            << parameter->_host_foton.voxel[iii]                //<< "\t"              
/*                                                       
            << parameter->_host_foton.fotoeff_szam[iii]         << "\t"       
            << parameter->_host_foton.compton_szam[iii]         << "\t"       
            << parameter->_host_foton.parkelt_szam[iii]         << "\t"       
                                                        
                                                        
            << parameter->_host_foton.foton_aram_1[iii]         << "\t"       
                                                        
                                                        
            //                              
            << parameter->_host_foton.hkrm_csoport[iii]         << "\t"       
            << parameter->_host_foton.hkrm_totalis[iii]         << "\t"       
            << parameter->_host_foton.lambda[iii]               << "\t"             
            << parameter->_host_foton.kiindulo_pont.x[iii]      << "\t"    
            << parameter->_host_foton.kiindulo_pont.y[iii]      << "\t"    
            << parameter->_host_foton.kiindulo_pont.z[iii]      //<< "\t"    
*/
            
            << std::endl;
    }

 
    //parameter->_generalt_eloszlas << parameter->_E_intervallumok[parameter->_spektrum_sorok_szama-1] << "\t" << parameter->_fluxus[parameter->_spektrum_sorok_szama-1]   << std::endl;

 
 
    parameter->_pillanatnyi_allapot.close();

 
 
    
    
    std::cout << "kiiras elkeszult, output fajl neve:" << std::endl;
    std::cout << fajlnev << std::endl;
    std::cout << "-------------------------------------" << std::endl;

    std::cout << std::endl;

    
    
}


 