
/*!

\mainpage 

<br> <br>
### Sugárterápiás Monte Carlo szimulációs kód fejlesztése GPU architektúrára teleterápiás alkalmazások esetén

BME TTK NTI

BSc szakdolgozat téma megvalósítása NVIDIA CUDA C nyelven.

\author Berczédi Balázs

<br>



---------------------------------------------

### Changelog




3.19 <br>
Voxelenként különböző sűrűségű víz:
`voxelek_suruseg.dat` fájlból beolvasva, 
`voxel_rho` változóban tárolva és 
`kovetkezo_utkozespont_kernel` -ben használva.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0) 
<br> 2017 április

--------------------------------------------------------------------------


3.18 <br>
Kezdőpont és kezdőirány változtatása parancssorból
(de továbbra is a kocka felső lapján kell, hogy belépjen a foton).

NaI detektor tesztek induló energia, kocka méret és
forrás pozíció változtatásával lefuttatva.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------

3.17 <br>
`Allapot` osztályban `allapot_kapcsolo` változó bevezetése.

NaI detektor teszthez `inputFeldolgozasa.cu` -ban sűrűség és hatáskeresztmetszetek megadása.
Különböző kocka méretek `voxel_anyagtipus_beallitas_kernel` szerkesztve.

Különböző energiák kigyűjtéséhez az `allapot_kapcsolo` -t kell használni,
oda-vissza kommentezni a megfelelő helyeken.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0) 

--------------------------------------------------------------------------


3.16 <br>
Detektor üzemmódhoz: `csatorna_meghataroz_kernel`, `csatorna_kigyujt_kernel` és 
szükséges változók létrehozása `Adatok`, `Allapot` és `Hataskeresztmetszet` osztályokban
(1.23A alapjan)

Tesztekhez:
induló energia, hely és irány tesztekhez szükséges módosítások kommentekbe írva
(`kovetkezo_utkozespont_kernel`, `divergalo_nyalab_kernel`, `dofespont_kernel`...).

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------


3.15 <br>
`foton_aram_meghatarozas_kernel` -ben csak kölcsönhatás nélküli fotonok darabszáma
egzaktul számolva. (Ehhez `volt_kolcsonhatas` új változó bevezetve és megvalósítva `Allapot` osztályban.)

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------


3.14 <br>
Paraméterek változtatása teszteléshez:
- párhuzamos irányú pontforrásokhoz -> `futtat.sh` -ban módosítva
- monoenergia változtatása -> `input/spektrum.txt` -ben módosítva
- homogén víz -> `voxel_anyagtipus_beallitas_kernel.cu` -ban 3D Shepp-Logan kikommentezve
- hatáskeresztmetszetek szuprémumanak változatása -> `inputFeldolgozasa.cu` szerkesztve,
  `anyagfajtak_szama = 1`, ha csak víz kell

Lamber-Beer törvény teszteléséhez:
- `foton_aram_meghatarozas_kernel` -ben csak ütközesnélküli (monoenergiás esetben
  teljes energiával rendelkező és csak előrefelé haladó) fotonok darabszámának meghatározása
- 5 helyett 6 db fotonáram felület 0, 10, 20, 30, 40, 50 cm mélyen
- fotonáram felületek mérete csökkentve

Homogén alumíniummal és félig víz félig alumíniummal is lefuttatva.

Első kölcsönhatások (kirepül, fotoeffektus, Compton-szórás, párkeltés)
is külön összeszámolva és `statisztika.txt`-be kiírva.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------


3.13 <br>
Kommentek takarítása.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)
<br> 2017 március

--------------------------------------------------------------------------


3.12 <br>
- `main` szétdarabolva több függvényre:
  `inputFeldolgozasa`, `memoriaFoglalasGPU`, `adatokMasolasaGPUra`,
  `futtatasGPU`, `adatokMasolasaHostra`, `adatokIrasaHDD`,
  `memoriaFelszabaditasGPU`, `memoriaFelszabaditasHost`
- `tobbKiserletbolEgyMeres`, `tobbLejatszasbolEgyKiserlet`,
  `tobbLepesbolEgyLejatszas`, `lepesElsoFotonnak` és
  `lepesParkeltesFotonnak` függvények megvalósítása
- CUDA_debug.h -ban preprocesszor definíciók összegyűjtése

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------

3.11 <br>
`generaltEnergiakKiirasa` valamint `allapotKiirasHostrol`, `allapotMasolasHostra`,
és `allapotMasolasGPUn` függvények megvalósítása.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------

3.10 <br>
Változók összegyűjtése `Adatok` és `ThrustObjektumok` osztályokba.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)
<br> 2017 február

--------------------------------------------------------------------------


3.09 <br>
Kiírások tisztázása, fordításhoz Makefile átírása.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------

3.08 <br>
`dozis_atlag_nullaz_meres_elejen_kernel` -ben mérési eredmények korrigálatlan empírikus szorása
lecserélve az átlagérték becsült standard hibájára.

(DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)

--------------------------------------------------------------------------

3.07 <br>
Shepp-Logan 3D fantom geometria alumíniummal es vízzel feltöltve.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------


3.06 <br>
Woodcock módszer while ciklusa a `kovetkezo_utkozespont_kernel` -be helyezve.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------


3.05 <br>
Különböző ellipszoid alakú anyagokból fantom létrehozása.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)
<br> 2017 január

--------------------------------------------------------------------------

3.04 <br>
Input táblázatokba valódi hatáskeresztmetszet adatok beírása (korábban még csak víz adatai voltak 3-szor ismételve).
- `kovetkezo_utkozespont_kernel` -ben `voxel_anyagtipus` beolvasása
- szabad úthossz sorsolás Woodcock módszerrel HKRM szuprémumának felhasználásával
- végül kölcsönhatás típusának kisorsolása (fotoeffektus / Compton-szóras / párkeltés / virtuális ütkozés)

A különböző kölcsönhatásokhoz tartozó kernelek if feltételeinek ennek megfelelő módosítása.

(DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)

--------------------------------------------------------------------------

3.03 <br>
Minden voxelhez anyagtípus hozzárendelése: <br>
memóriafoglalás GPU-n majd `voxel_anyagtipus_beallitas_kernel` segitségével
anyagtípus sorszámok rögzítése.

(DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)

--------------------------------------------------------------------------


3.02 <br>
3-féle anyag különböző sűrűség és makroszkópikus hatáskeresztmetszet adatainak átmásolása GPU-ra
és ezek használata a homogén anyagra vonatkozó képletekben.

(DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)

--------------------------------------------------------------------------


3.01 <br>
Hatáskeresztmetszetek 3 féle anyag esetén:
- mikroszkópikus hatáskeresztmetszetek [cm2/g] beolvasása
- sűrűség [g/cm3] megadása
- ezekből makroszkópikus hatáskersztmetszet [1/cm] számitása

Minden energiacsoport esetén makroszkópikus hatáskersztmetszetek szuprémumának meghatározása
(egyelőre csak beolvasás és kiírás hoston, kernelekben számitás továbbra is az eredeti homogén anyaggal).

(DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)

--------------------------------------------------------------------------

3.00  <br>
`kovetkezo_utkozespont_kernel` random return javítva.

(DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)

--------------------------------------------------------------------------

2.01 <br>
függvények külön forrásfájlokba szétdarabolva:

<tt>
Vektor3D, Korlap, Koordinatak, Allapot, Hataskeresztmetszet, 
tablazat2_beolvas, tablazat4_beolvas, marsaglia_izotrop_tesztelo_kernel, 
bazis_transzformacio_Device, izotrop_kup_Device, nyalab_forgatas_Device, 
kahn_sorsolas_Device, marsaglia_izotrop_irany_Device, cuRAND_beallitas_kernel, 
indulo_energia_kernel, korlap_kernel, divergalo_nyalab_kernel, dofespont_kernel, 
kovetkezo_utkozespont_kernel, kirepulo_fotonok_kernel, fotoeffektus_kernel, 
comptonszoras_kernel, parkeltes_kernel, parkelt_foton_nullaz_kernel, 
foton_aram_meghatarozas_kernel, dozis_atlag_nullaz_meres_elejen_kernel, 
voxel_nullaz_kiserletenkent_kernel, voxel_kigyujt_lepesenkent_kernel, 
beutes_nullaz_lepesenkent_kernel, dozis_atlag_frissit_kiserletenkent_kernel, 
dozis_atlag_meghataroz_meres_vegen_kernel 
</tt>
<br> (DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)


2.00 <br>
Teszteléshez `semaphore` törölve.
<br> (DELL780, GT-710, xubuntu 16.04.1 LTS, CUDA 8.0)
<br> 2016 december

--------------------------------------------------------------------------


1.25 <br>
Bruttó fotonáramok meghatározása, mert az MCNP azt számol F1 tally-n.
<br> (DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)


1.24 <br>
Kirepült energia összeadása és kiírása.
<br> (DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)


1.23 <br>
5 db vízszintes rögzített felületen fotonáram meghatározása.
<br> (DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)


1.01 - 1.22 <br>
3 különböző fajta kölcsönhatás, lépcsős spektrum,  homogén anyag, voxelenként leadott dózis, szórás...
<br> (DELL745, GT-610, xubuntu 12.04.3 LTS, CUDA 5.0)

*/




/*!

\defgroup Konstansok_group Konstansok
\brief A program által használt, makróként definiált konstansok.
\details

\defgroup Osztalyok_goup Osztályok
\brief A programban használt adatstruktúrák.
\details

\defgroup fuggvenyek Függvények
\brief A program függvényei.
\details
\{
    
    \defgroup main_fuggveny_group main 
    \brief 
    A main függvény.
    \details
    
    \defgroup CPU_fuggvenyek_group __host__ függvények
    \brief 
    CPU-n futó függvények.
    \details
    
    \defgroup wrapper_fuggvenyek_group wrapper és vegyes függvények
    \brief 
    CPU-n futó függvények, amelyek GPU hívásokat is tartalmaznak.
    \details
    
    \defgroup Kernel_fuggvenyek_group __global__ függvények
    \brief 
    Host kódból hívható GPU-n futó függvények.
    \details
    
    \defgroup Device_fuggvenyek_group __device__ függvények
    \brief 
    Kernel kódból hívható GPU-n futó függvények.
    \details
    
\}

*/


/*========================================================================================*/

#include <iostream>

#include "Adatok.cuh"

#include "inputFeldolgozasa.cuh"
#include "memoriaFoglalasGPU.cuh"
#include "adatokMasolasaGPUra.cuh"
#include "futtatasGPU.cuh"
#include "adatokMasolasaHostra.cuh"
#include "adatokIrasaHDD.cuh"
#include "memoriaFelszabaditasGPU.cuh"
#include "memoriaFelszabaditasHost.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()



/*========================================================================================*/




/*!

\ingroup main_fuggveny_group 

\brief A main függvény.

\details A program csak megfelelő paraméterekkel idul el.

*/

int main(int argc, char** argv)
{

/*!

A szükséges parancssori paraméterek:

1. egyszerre elindított fotonok száma
2. egy kísérletben hány ciklusban indítunk fotonokat
3. kisérletek száma
4. kocka mérete cm-ben
5. voxel méret cm-ben
6. forrás kezdőpont x koordinátája cm-ben
7. forrás kezdőpont y koordinátája cm-ben
8. forrás kezdőpont z koordinátája cm-ben
9.  kezdő irányvektor x komponense
10. kezdő irányvektor y komponense
11. kezdő irányvektor z komponense
12. induló nyaláb körlap sugara cm-ben
13. nyaláb széttartása (kúp fél nyilásszöge fokban)

program indítása pl.: <br>
./teleterapia.run 100000 1000 10 50.0 .20000 25.0 25.0 150.0 0.0 0.0 -1.0 1.0 2.5 

*/


    if(argc != 14)          
    {
        std::cout << std::endl;
        std::cout << "a program 15db bemeneti parametert var:" << std::endl;
        
        std::cout << " 1.: egyszerre elinditott fotonok szama" << std::endl;
        std::cout << " 2.: egy kiserletben hany ciklusban inditunk fotonokat" << std::endl;
        std::cout << " 3.: kiserletek szama" << std::endl;
        std::cout << " 4.: kocka merete cm-ben" << std::endl;
        std::cout << " 5.: voxel meret cm-ben" << std::endl;
        std::cout << " 6.: forras kezdopont x koordinataja cm-ben" << std::endl;
        std::cout << " 7.: forras kezdopont y koordinataja cm-ben" << std::endl;
        std::cout << " 8.: forras kezdopont z koordinataja cm-ben" << std::endl;
        std::cout << " 9.: kezdo iranyvektor x komponense" << std::endl;
        std::cout << "10.: kezdo iranyvektor y komponense" << std::endl;
        std::cout << "11.: kezdo iranyvektor z komponense" << std::endl;
        std::cout << "12.: indulo nyalab korlap sugara cm-ben" << std::endl;
        std::cout << "13.: nyalab szettartasa (kup fel nyilasszoge fokban)" << std::endl;
        
        
        std::cout << "program inditasa pl.:" << std::endl;
        std::cout << "programneve 100000 10 10 50.0 0.5 5.0 3.0 0.0 1.0 45.0" << std::endl << std::endl;
        exit(EXIT_FAILURE);
    }
    
    std::cout << "# ---------------------------------------------" << std::endl;
    std::cout << std::endl;
    
    
    
    
    Adatok parameter_stack;             // az adatok osszegyujtve egy helyen
    
    Adatok* parameter = &parameter_stack;
    
    // main bemeno parameterei is elerhetoek lesznek "parameter" strukturan keresztul
    parameter->_argc = argc;
    parameter->_argv = argv;
    
    
    
    
    
    cudaDeviceReset();
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    inputFeldolgozasa(parameter);
    
    memoriaFoglalasGPU(parameter);
    
    adatokMasolasaGPUra(parameter);
    
    futtatasGPU(parameter);
    
    adatokMasolasaHostra(parameter);
    
    adatokIrasaHDD(parameter);
    
    memoriaFelszabaditasGPU(parameter);
    
    memoriaFelszabaditasHost(parameter);
    
    
    
    
    
    
    check_cuda_errors(__FILE__, __LINE__);
    cudaDeviceReset();
        
    return EXIT_SUCCESS;
}



/*========================================================================================*/



