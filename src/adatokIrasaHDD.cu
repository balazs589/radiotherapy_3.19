#include "adatokIrasaHDD.cuh"

#include "constants.h"      // pi es elektron nyugalmi energia
#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"



/*!
\ingroup CPU_fuggvenyek_group

\brief 
Szimuláció eredményeinek bináris és szöveges fálokba írása.

\param [in,out] parameter Ezen a változón keresztül érhető el a program összes globálisan használt adata.
\return Nincs.

\details 

*/

void adatokIrasaHDD(Adatok *parameter)
{
    
    
    
    
    std::cout << "---------------------------------------------"  << '\n' << std::endl;
    std::cout << "voxel adatok kiirasa..." << std::endl << std::endl << std::endl;
    
    
    
    parameter->_dozis_atlag_file.open("./output/eredmeny/dozis_atlag.dat", std::ios::out | std::ios::binary);
    if(parameter->_dozis_atlag_file.is_open())
    {
        parameter->_dozis_atlag_file.write((char*)parameter->_h_szigma.dozis_atlag, parameter->_h_szigma.voxelek_szama * sizeof(double));
        parameter->_dozis_atlag_file.close();
    }
    
    parameter->_dozis_szoras_file.open("./output/eredmeny/dozis_szoras.dat", std::ios::out | std::ios::binary);
    if(parameter->_dozis_szoras_file.is_open())
    {
        parameter->_dozis_szoras_file.write((char*)parameter->_h_szigma.dozis_szoras, parameter->_h_szigma.voxelek_szama * sizeof(double));
        parameter->_dozis_szoras_file.close();
    }
    
    
/*----------------------------------------------------------------------------------------------------*/    
    
    // meres vegeredmenyeinek: 
    parameter->_statisztika.open("./output/eredmeny/statisztika.txt");
    
    
    for(int iii = 0; iii<parameter->_argc; iii++)
    {
        parameter->_statisztika << parameter->_argv[iii] << " ";
    }
    parameter->_statisztika << std::endl;
    parameter->_statisztika << std::endl;
    parameter->_statisztika << "---------------------------------------------"  << '\n' << std::endl;
    
    
    parameter->_statisztika << " 1.: egyszerre elinditott fotonok szama" << std::endl;
    parameter->_statisztika << " 2.: egy kiserletben hany ciklusban inditunk fotonokat" << std::endl;
    parameter->_statisztika << " 3.: kiserletek szama" << std::endl;
    parameter->_statisztika << " 4.: kocka merete cm-ben" << std::endl;
    parameter->_statisztika << " 5.: voxel meret cm-ben" << std::endl;
    parameter->_statisztika << " 6.: forras kezdopont x koordinataja cm-ben" << std::endl;
    parameter->_statisztika << " 7.: forras kezdopont y koordinataja cm-ben" << std::endl;
    parameter->_statisztika << " 8.: forras kezdopont z koordinataja cm-ben" << std::endl;
    parameter->_statisztika << " 9.: kezdo iranyvektor x komponense" << std::endl;
    parameter->_statisztika << "10.: kezdo iranyvektor y komponense" << std::endl;
    parameter->_statisztika << "11.: kezdo iranyvektor z komponense" << std::endl;
    parameter->_statisztika << "12.: indulo nyalab korlap sugara cm-ben" << std::endl;
    parameter->_statisztika << "13.: nyalab szettartasa (kup fel nyilasszoge fokban)" << std::endl;
    
    parameter->_statisztika << std::endl;
    parameter->_statisztika << "---------------------------------------------"  << '\n' << std::endl;
    
    
    parameter->_statisztika << std::endl;
    parameter->_statisztika << "voxelek szama x-y-z iranyban:" << std::endl;
    parameter->_statisztika << "nx: " << parameter->_h_szigma.nx << std::endl;
    parameter->_statisztika << "ny: " << parameter->_h_szigma.ny << std::endl;
    parameter->_statisztika << "nz: " << parameter->_h_szigma.nz << std::endl;
    parameter->_statisztika << "osszes voxel szama: " << parameter->_h_szigma.voxelek_szama << std::endl;
    parameter->_statisztika << std::endl;
    
    
    double ro = 1.0;  // [g/cm3]
    double voxel_terfogat = parameter->_h_szigma.dx * parameter->_h_szigma.dy * parameter->_h_szigma.dz;    // [cm3]
    double voxel_tomeg = voxel_terfogat * ro;   // [g]
    //double MeV = 1.0e+6 * 1.6e-19;                      // MeV -> Joule
    
    
    parameter->_statisztika << "viz eseten ro = " << ro << " g/cm3" << std::endl;
    parameter->_statisztika << "voxel_terfogat = " << voxel_terfogat << " cm3" << std::endl;
    parameter->_statisztika << "voxel_tomeg = " << voxel_tomeg << " g" << std::endl;
    
    
    parameter->_statisztika << "---------------------------------------------"  << '\n' << std::endl;
    
    parameter->_statisztika << parameter->_kiserletek_szama << " db kiserlet soran:" << std::endl;
    parameter->_statisztika << "kiserletenkent elinditott fotonok szama = " << parameter->fotonSzalakSzama*parameter->_ciklusok_szama << " db\n\n" << std::endl;
    
    parameter->_statisztika << "osszes_elinditott_foton = " << parameter->_osszes_elinditott_foton << " db" << std::endl << std::endl;
    
    
    parameter->_statisztika << "_elso_lepesben_kirepult_szam = " << parameter->_elso_lepesben_kirepult_szam << " db" << std::endl;
    parameter->_statisztika << "_elso_lepesben_fotoeff_szam  = " << parameter->_elso_lepesben_fotoeff_szam  << " db" << std::endl;
    parameter->_statisztika << "_elso_lepesben_compton_szam  = " << parameter->_elso_lepesben_compton_szam  << " db" << std::endl;
    parameter->_statisztika << "_elso_lepesben_parkelt_szam  = " << parameter->_elso_lepesben_parkelt_szam  << " db" << std::endl;
    
    
    parameter->_statisztika << "---------------------------------------------"  << '\n' << std::endl;
    
    parameter->_statisztika << "osszes_fotoeff_szam = " << parameter->_osszes_fotoeff_szam << " db" << std::endl;
    parameter->_statisztika << "osszes_compton_szam = " << parameter->_osszes_compton_szam << " db" << std::endl;
    parameter->_statisztika << "osszes_parkelt_szam = " << parameter->_osszes_parkelt_szam << " db" << std::endl;
    
    parameter->_statisztika << std::endl;
    
    
    
    parameter->_statisztika << std::endl;
    
    parameter->_statisztika << "---------------------------------------------" << std::endl;
    parameter->_statisztika << "fotonaram feluletek:" << std::endl;
    
    parameter->_statisztika << "felulet_x_min = " << parameter->_d_szigma.felulet_x_min << " cm" << std::endl;
    parameter->_statisztika << "felulet_x_max = " << parameter->_d_szigma.felulet_x_max << " cm" << std::endl;
    parameter->_statisztika << "felulet_y_min = " << parameter->_d_szigma.felulet_y_min << " cm" << std::endl;
    parameter->_statisztika << "felulet_y_max = " << parameter->_d_szigma.felulet_y_max << " cm" << std::endl;
    parameter->_statisztika << std::endl;
    parameter->_statisztika << "felulet1_z = " << parameter->_d_szigma.felulet1_z << " cm" << std::endl;
    parameter->_statisztika << "felulet2_z = " << parameter->_d_szigma.felulet2_z << " cm" << std::endl;
    parameter->_statisztika << "felulet3_z = " << parameter->_d_szigma.felulet3_z << " cm" << std::endl;
    parameter->_statisztika << "felulet4_z = " << parameter->_d_szigma.felulet4_z << " cm" << std::endl;
    parameter->_statisztika << "felulet5_z = " << parameter->_d_szigma.felulet5_z << " cm" << std::endl;
    parameter->_statisztika << "felulet6_z = " << parameter->_d_szigma.felulet6_z << " cm" << std::endl;
    
    parameter->_statisztika << "---------------------------------------------" << std::endl;
    parameter->_statisztika << "kolcsonhatas nelkul athalado fotonok arama a fenti feluleteken" << std::endl;
    parameter->_statisztika << std::endl;
    
    
    parameter->_statisztika << parameter->_d_szigma.felulet1_z << " cm" << "\t" << parameter->_foton_aram_szam_2[1-1] << " db" << std::endl;
    parameter->_statisztika << parameter->_d_szigma.felulet2_z << " cm" << "\t" << parameter->_foton_aram_szam_2[2-1] << " db" << std::endl;
    parameter->_statisztika << parameter->_d_szigma.felulet3_z << " cm" << "\t" << parameter->_foton_aram_szam_2[3-1] << " db" << std::endl;
    parameter->_statisztika << parameter->_d_szigma.felulet4_z << " cm" << "\t" << parameter->_foton_aram_szam_2[4-1] << " db" << std::endl;
    parameter->_statisztika << parameter->_d_szigma.felulet5_z << " cm" << "\t" << parameter->_foton_aram_szam_2[5-1] << " db" << std::endl;
    parameter->_statisztika << parameter->_d_szigma.felulet6_z << " cm" << "\t" << parameter->_foton_aram_szam_2[6-1] << " db" << std::endl;
    
    
    parameter->_statisztika << std::endl;
    parameter->_statisztika << "_osszes_detektor_0_energias_fotonok_szama = " << parameter->_osszes_detektor_0_energias_fotonok_szama << " db" << std::endl;
    
    parameter->_statisztika << std::endl;
    parameter->_statisztika << "---------------------------------------------" << std::endl;
    parameter->_statisztika << std::endl;
    
    
    parameter->_statisztika << "osszes_elinditott_energia = " << parameter->_osszes_elinditott_energia << " MeV" << std::endl;
    
    parameter->_statisztika << "osszes_leadott_energia = " << parameter->_osszes_leadott_energia << " MeV" << std::endl;
    
    parameter->_statisztika << "osszes kirepult_energia = " << parameter->_osszes_kirepult_energia << " MeV" << std::endl;
    
    
    
    parameter->_statisztika <<  std::endl;
    parameter->_statisztika << "dozis_atlag.dat fajlba kiirt voxel adatok az 1 db kiserletre juto atlagerteket tartalmazzak MeV mertekegysegben" << std::endl;
    parameter->_statisztika << "illetve az atlagertek becsult hibaja (standard error of the mean) is MeV mertekegysegben szerepel dozis_szoras.dat fajlban" << std::endl;
    parameter->_statisztika << "(fuggoleges metszeten mar realtiv hiba van abrazolva)" << std::endl;
    
    parameter->_statisztika << "---------------------------------------------"  << '\n' << std::endl;
    
    
    parameter->_statisztika << "meres osszes ideje:\t" << parameter->_time_MERES/1000.0 << " sec" << std::endl; 
    
    
    //std::cout << "max_dozis = " << max_dozis << std::endl;
    //std::cout << "min_dozis = " << min_dozis << std::endl;
    
    
    
    
    // spektrumbol szamitott elmeleti energia eloszlas kiirasa:
    parameter->_elmeleti_eloszlas << "# elmeleti energia eloszlas" << std::endl;
    parameter->_elmeleti_eloszlas << "# -----------------------------------" << std::endl;
    parameter->_elmeleti_eloszlas << "# E [MeV]" << "\t" << "eloszlas" << std::endl;
    parameter->_elmeleti_eloszlas << "# -----------------------------------" << std::endl;
    parameter->_elmeleti_eloszlas << std::scientific;
    
    for(int iii = 0; iii<parameter->_spektrum_sorok_szama; iii++)
    {
        parameter->_elmeleti_eloszlas << parameter->_E_intervallumok[iii] << "\t" << parameter->_fluxus[iii]   << std::endl;
    }
    
    
    parameter->_detektor_file << 0.0 << "\t" << parameter->_osszes_detektor_0_energias_fotonok_szama << std::endl;
    for(int iii = 0; iii<parameter->_h_szigma.csatornak_szama; iii++ )
    {
        parameter->_detektor_file << (iii+1)*parameter->_h_szigma.csatorna_energia_osztas << "\t" << parameter->_h_szigma.csatornak[iii] << std::endl;
    }
    
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    std::cout << "kiiras elkeszult" << std::endl;
    
     
//---------------
    parameter->_elmeleti_eloszlas.close();
    
    parameter->_detektor_file.close();
    
    parameter->_statisztika.close();
    
    
    
    
    
}


 