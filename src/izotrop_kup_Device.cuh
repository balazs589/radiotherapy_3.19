#ifndef IZOTROP_KUP_DEVICE_CUH
#define IZOTROP_KUP_DEVICE_CUH

#include "Vektor3D.cuh"
#include <curand_kernel.h>

#include "constants.h"

__device__ Vektor3D izotrop_kup_Device(double cos_alfa, curandState* localState);


#endif
