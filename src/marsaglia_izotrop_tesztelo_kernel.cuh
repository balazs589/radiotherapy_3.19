#ifndef MARSAGLIA_IZOTROP_TESZTELO_KERNEL_CUH
#define MARSAGLIA_IZOTROP_TESZTELO_KERNEL_CUH


#include <curand_kernel.h>
#include "Allapot.cuh"


__global__ void marsaglia_izotrop_tesztelo_kernel(int N, curandState *state, Allapot foton);



#endif
