#ifndef THRUSTOBJEKTUMOK_CUH
#define THRUSTOBJEKTUMOK_CUH


#include <thrust/device_vector.h>
//#include <thrust/pair.h>




/*!
    \ingroup Osztalyok_goup
    
    \brief Thrust objektumok összegyűjtéséhez.
    
    \details
    Pointerek beállítása a memoriaFoglalasGPU() függvényen belül a memoriaFoglalasGPU.cu fájlban.
    
    
*/

class ThrustObjektumok
{
public:
    
    /*! \name 
    \{*/
    thrust::device_ptr<double>  _dozis; // voxelek dozisai kiserletenkent
    thrust::device_ptr<double>  _dozis_atlag; // voxelek osszesitett dozisai
    thrust::device_ptr<int>     _sorszam_gyujto; // melyik voxelben volt beutes
    thrust::device_ptr<double>  _dozis_gyujto; // mekkora volt a leadott energia az adott voxelben
    thrust::device_ptr<int>     _beutesszam_gyujto; // hany darab fotonbol szarmazik a leadott energia
    
    
    thrust::device_ptr<long long int>   _csatornak; // detektor csatornakban hany beutes volt
    thrust::device_ptr<int>             _csatorna_sorszam_gyujto; // egy kigyujteskor erintett csatornak listaja
    thrust::device_ptr<int>             _csatorna_beutesszam_gyujto; // kigyujteskor az erintett csatornakhoz tartozo beutesszamok
    
    thrust::device_ptr<long long int>   _detektor_0_energias_fotonok_szama; // (-1) sorszamu csatornaban a "0" leadott energiaju beutesek szama
    
    
    thrust::device_ptr<double>  _kirepult_energia; // (-1) sorszamu voxelben a kockan kivul leadott energia
    
    
    
    thrust::device_ptr<double> _energia; // foton energia tombre mutat
    thrust::device_ptr<double> _elozo_hely_x; // foton elozo_hely.x tombre mutat
    thrust::device_ptr<double> _elozo_hely_y; // ...
    thrust::device_ptr<double> _elozo_hely_z; //
    thrust::device_ptr<double> _elozo_irany_x; //
    thrust::device_ptr<double> _elozo_irany_y; //
    thrust::device_ptr<double> _elozo_irany_z; //
    
    thrust::device_ptr<int>     _aktiv_foton;
    thrust::device_ptr<int>     _kolcsonhatas_tipus;
    thrust::device_ptr<double>  _leadott_energia;
    
    thrust::device_ptr<int> _voxel; // foton voxelre mutat
    
    thrust::device_ptr<int> _fotoeff_szam; // foton fotoeff szamra mutat
    thrust::device_ptr<int> _compton_szam; // ...
    thrust::device_ptr<int> _parkelt_szam;
    
    thrust::device_ptr<int> _volt_kolcsonhatas;
    thrust::device_ptr<int> _allapot_kapcsolo;
    
    thrust::device_ptr<double> _detektor_energia;
    thrust::device_ptr<int>    _detektor_csatorna;
    
    
    thrust::device_ptr<int> _foton_aram_1;
    
    thrust::device_ptr<int>     _hkrm_csoport;
    thrust::device_ptr<double>  _hkrm_totalis;
    thrust::device_ptr<double>  _lambda;
    thrust::device_ptr<double>  _kiindulo_pont_x;
    thrust::device_ptr<double>  _kiindulo_pont_y;
    thrust::device_ptr<double>  _kiindulo_pont_z;
    
    
    //----------------------------------
    
    thrust::device_ptr<double> _parkelt_energia;
    thrust::device_ptr<double> _parkelt_elozo_hely_x;
    thrust::device_ptr<double> _parkelt_elozo_hely_y;
    thrust::device_ptr<double> _parkelt_elozo_hely_z;
    thrust::device_ptr<double> _parkelt_elozo_irany_x;
    thrust::device_ptr<double> _parkelt_elozo_irany_y;
    thrust::device_ptr<double> _parkelt_elozo_irany_z;
    
    thrust::device_ptr<int> _parkelt_aktiv_foton;
    thrust::device_ptr<int> _parkelt_kolcsonhatas_tipus;
    
    thrust::device_ptr<double>  _parkelt_leadott_energia;    thrust::device_ptr<int>     _parkelt_voxel;
    
    thrust::device_ptr<int>     _parkelt_fotoeff_szam;
    thrust::device_ptr<int>     _parkelt_compton_szam;
    
    thrust::device_ptr<int>     _parkelt_volt_kolcsonhatas;
    thrust::device_ptr<int>     _parkelt_allapot_kapcsolo;
    
    thrust::device_ptr<double> _parkelt_detektor_energia;
    thrust::device_ptr<int>    _parkelt_detektor_csatorna;
    
    thrust::device_ptr<int>     _parkelt_foton_aram_1;
    
    thrust::device_ptr<int>    _parkelt_hkrm_csoport;
    thrust::device_ptr<double> _parkelt_hkrm_totalis;
    thrust::device_ptr<double> _parkelt_lambda;
    thrust::device_ptr<double> _parkelt_kiindulo_pont_x;
    thrust::device_ptr<double> _parkelt_kiindulo_pont_y;
    thrust::device_ptr<double> _parkelt_kiindulo_pont_z;
    
    
    //----------------------
    // teszteleshez pillanatnyi allapot kiirasahoz
    thrust::device_ptr<double> _pillanatnyi_energia; // foton energia tombre mutat
    thrust::device_ptr<double> _pillanatnyi_elozo_hely_x; // foton elozo_hely.x tombre mutat
    thrust::device_ptr<double> _pillanatnyi_elozo_hely_y; // ...
    thrust::device_ptr<double> _pillanatnyi_elozo_hely_z; //
    thrust::device_ptr<double> _pillanatnyi_elozo_irany_x; //
    thrust::device_ptr<double> _pillanatnyi_elozo_irany_y; //
    thrust::device_ptr<double> _pillanatnyi_elozo_irany_z; //
    
    thrust::device_ptr<int>     _pillanatnyi_aktiv_foton;
    thrust::device_ptr<int>     _pillanatnyi_kolcsonhatas_tipus;
    thrust::device_ptr<double>  _pillanatnyi_leadott_energia;
    
    thrust::device_ptr<int> _pillanatnyi_voxel; // foton voxelre mutat
    
    thrust::device_ptr<int> _pillanatnyi_fotoeff_szam; // foton fotoeff szamra mutat
    thrust::device_ptr<int> _pillanatnyi_compton_szam; // ...
    thrust::device_ptr<int> _pillanatnyi_parkelt_szam;
    
    thrust::device_ptr<int> _pillanatnyi_volt_kolcsonhatas;
    thrust::device_ptr<int> _pillanatnyi_allapot_kapcsolo;
    
    thrust::device_ptr<double> _pillanatnyi_detektor_energia;
    thrust::device_ptr<int>    _pillanatnyi_detektor_csatorna;
    
    thrust::device_ptr<int>     _pillanatnyi_foton_aram_1;
    
    thrust::device_ptr<int>     _pillanatnyi_hkrm_csoport;
    thrust::device_ptr<double>  _pillanatnyi_hkrm_totalis;
    thrust::device_ptr<double>  _pillanatnyi_lambda;
    thrust::device_ptr<double>  _pillanatnyi_kiindulo_pont_x;
    thrust::device_ptr<double>  _pillanatnyi_kiindulo_pont_y;
    thrust::device_ptr<double>  _pillanatnyi_kiindulo_pont_z;
    
    
    //-----------------------------
    
    thrust::pair< thrust::device_ptr<int>, thrust::device_ptr<double> > _lista2_vege; // voxelek kigyujtesehez
    
    thrust::pair< thrust::device_ptr<int>, thrust::device_ptr<int> >    _lista3_vege; // detektor csatornak kigyujtesehez
    
    /*!\}*/
    
};


#endif
