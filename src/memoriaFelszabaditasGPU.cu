#include "memoriaFelszabaditasGPU.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"



/*!
\ingroup wrapper_fuggvenyek_group

\brief
Mérés végén grafikus kártya globális memóriájának felszabadítása.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void memoriaFelszabaditasGPU(Adatok *parameter)
{
    
    
    
    CUDA_CALL(cudaFree(parameter->_d_szigma.voxel_anyagtipus));
    CUDA_CALL(cudaFree(parameter->_d_szigma.voxel_rho));

    CUDA_CALL(cudaFree(parameter->_d_szigma.dozis));
    //CUDA_CALL(cudaFree(parameter->_d_szigma.beutesek_szama));
    CUDA_CALL(cudaFree(parameter->_d_szigma.dozis_atlag));
    CUDA_CALL(cudaFree(parameter->_d_szigma.dozis_szoras));
    
    
    CUDA_CALL(cudaFree(parameter->_d_szigma.sorszam_gyujto));
    CUDA_CALL(cudaFree(parameter->_d_szigma.dozis_gyujto));
    CUDA_CALL(cudaFree(parameter->_d_szigma.beutesszam_gyujto));
    
    CUDA_CALL(cudaFree(parameter->_d_szigma.energia));
    CUDA_CALL(cudaFree(parameter->_d_szigma.fotoeff));
    CUDA_CALL(cudaFree(parameter->_d_szigma.compton));
    CUDA_CALL(cudaFree(parameter->_d_szigma.parkelt));
    CUDA_CALL(cudaFree(parameter->_d_szigma.totalis));
    
    CUDA_CALL(cudaFree(parameter->_devStates));
    
    
    CUDA_CALL(cudaFree(parameter->_d_szigma.csatornak));
    CUDA_CALL(cudaFree(parameter->_d_szigma.csatorna_sorszam_gyujto));
    CUDA_CALL(cudaFree(parameter->_d_szigma.csatorna_beutesszam_gyujto));
    CUDA_CALL(cudaFree(parameter->_d_szigma.detektor_0_energias_fotonok_szama));
    
    
    CUDA_CALL(cudaFree(parameter->_d_szigma.kirepult_energia));
    
    
    //---------------   
    
    CUDA_CALL(cudaFree(parameter->_foton.energia));
    CUDA_CALL(cudaFree(parameter->_foton.elozo_hely.x));
    CUDA_CALL(cudaFree(parameter->_foton.elozo_hely.y));
    CUDA_CALL(cudaFree(parameter->_foton.elozo_hely.z));
    CUDA_CALL(cudaFree(parameter->_foton.elozo_irany.x));
    CUDA_CALL(cudaFree(parameter->_foton.elozo_irany.y));
    CUDA_CALL(cudaFree(parameter->_foton.elozo_irany.z));
    
    CUDA_CALL(cudaFree(parameter->_foton.aktiv_foton));
    CUDA_CALL(cudaFree(parameter->_foton.kolcsonhatas_tipus));
    
    CUDA_CALL(cudaFree(parameter->_foton.leadott_energia));
    CUDA_CALL(cudaFree(parameter->_foton.voxel));
    //CUDA_CALL(cudaFree(parameter->_foton.leadott_energia_2));
    
    CUDA_CALL(cudaFree(parameter->_foton.fotoeff_szam));
    CUDA_CALL(cudaFree(parameter->_foton.compton_szam));
    CUDA_CALL(cudaFree(parameter->_foton.parkelt_szam));
    
    CUDA_CALL(cudaFree(parameter->_foton.volt_kolcsonhatas));
    CUDA_CALL(cudaFree(parameter->_foton.allapot_kapcsolo));
    
    
    CUDA_CALL(cudaFree(parameter->_foton.detektor_energia));
    CUDA_CALL(cudaFree(parameter->_foton.detektor_csatorna));
    
    CUDA_CALL(cudaFree(parameter->_foton.foton_aram_1));
    
    
    CUDA_CALL(cudaFree(parameter->_foton.hkrm_csoport));
    CUDA_CALL(cudaFree(parameter->_foton.hkrm_totalis));
    CUDA_CALL(cudaFree(parameter->_foton.lambda));
    CUDA_CALL(cudaFree(parameter->_foton.kiindulo_pont.x));
    CUDA_CALL(cudaFree(parameter->_foton.kiindulo_pont.y));
    CUDA_CALL(cudaFree(parameter->_foton.kiindulo_pont.z));
    
    //---------------
    
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.energia));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.elozo_hely.x));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.elozo_hely.y));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.elozo_hely.z));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.elozo_irany.x));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.elozo_irany.y));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.elozo_irany.z));
    
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.aktiv_foton));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.kolcsonhatas_tipus));
    
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.leadott_energia));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.voxel));
    //CUDA_CALL(cudaFree(parameter->_parkelt_foton.leadott_energia_2));
    
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.fotoeff_szam));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.compton_szam));
    //CUDA_CALL(cudaFree(parameter->_parkelt_foton.parkelt_szam));
    
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.volt_kolcsonhatas));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.allapot_kapcsolo));
    
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.detektor_energia));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.detektor_csatorna));
    
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.foton_aram_1));
    
    
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.hkrm_csoport));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.hkrm_totalis));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.lambda));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.kiindulo_pont.x));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.kiindulo_pont.y));
    CUDA_CALL(cudaFree(parameter->_parkelt_foton.kiindulo_pont.z));
    
    
    //---------------   
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.energia));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.elozo_hely.x));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.elozo_hely.y));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.elozo_hely.z));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.elozo_irany.x));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.elozo_irany.y));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.elozo_irany.z));
    
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.aktiv_foton));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.kolcsonhatas_tipus));
    
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.leadott_energia));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.voxel));
    //CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.leadott_energia_2));
    
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.fotoeff_szam));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.compton_szam));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.parkelt_szam));
    
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.volt_kolcsonhatas));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.allapot_kapcsolo));
    
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.detektor_energia));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.detektor_csatorna));
    
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.foton_aram_1));
    
    
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.hkrm_csoport));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.hkrm_totalis));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.lambda));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.kiindulo_pont.x));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.kiindulo_pont.y));
    CUDA_CALL(cudaFree(parameter->_pillanatnyi_foton.kiindulo_pont.z));
    
    
    
    
}


 