#ifndef DOFESPONT_KERNEL_CUH
#define DOFESPONT_KERNEL_CUH

#include "Allapot.cuh"
#include "Hataskeresztmetszet.cuh"


__global__ void dofespont_kernel(int N, Allapot foton, Hataskeresztmetszet szigma);


#endif
