#include "marsaglia_izotrop_irany_Device.cuh"

#include <curand_kernel.h>
#include "Vektor3D.cuh"

/*!
\ingroup Device_fuggvenyek_group

\brief
3D izotróp iránysorsolás Marsaglia módszerrel.

\param [in,out] localState
CUDA random generátor állapota.

\return
3 dimenzióban izotróp irányeloszlású 1 hosszúságra normált véletlen irányvektor.

\details 

*/

__device__ Vektor3D marsaglia_izotrop_irany_Device(curandState* localState)
{
    Vektor3D izotrop_irany;
        
    double v1;
    double v2;
    double s;
    
    do
    {
        v1 = 2.0*curand_uniform_double(localState) - 1.0;
        v2 = 2.0*curand_uniform_double(localState) - 1.0;
        s = v1*v1 + v2*v2;
        
    } while( !(s<1.0) );
    
    double r = sqrt(1.0 - s);
        
    izotrop_irany.x = 2.0 * v1 * r;
    izotrop_irany.y = 2.0 * v2 * r;
    izotrop_irany.z = 1.0 - 2.0 * s;
        
    return izotrop_irany;
        
}
