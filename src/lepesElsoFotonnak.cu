#include "lepesElsoFotonnak.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"



# include <curand_kernel.h>
// http://stackoverflow.com/questions/23352122/including-thrust-sort-h-before-curand-kernel-h-gives-compilation-error


//#include <thrust/device_vector.h>
//#include <thrust/host_vector.h>
#include <thrust/sort.h>

#include <thrust/reduce.h>
#include <thrust/count.h>

//#include <thrust/pair.h>
//#include <thrust/iterator/constant_iterator.h>





#include "kovetkezo_utkozespont_kernel.cuh"

#include "kirepulo_fotonok_kernel.cuh"
#include "fotoeffektus_kernel.cuh"
#include "comptonszoras_kernel.cuh"
#include "parkeltes_kernel.cuh"

#include "foton_aram_meghatarozas_kernel.cuh"

#include "voxel_kigyujt_lepesenkent_kernel.cuh"
#include "beutes_nullaz_lepesenkent_kernel.cuh"


//#include "generaltEnergiakKiirasa.cuh"




/*!
\ingroup wrapper_fuggvenyek_group

\brief
Nyalábban induló fotonok anyaggal való kölcsönhatása során bekövetkező változások lejátszása, voxelenkénti dózisok kigyűjtése. (Párkeltésből származó első fotonok is.)

\param [in,out]
parameter Ezen keresztül lehet írni és olvasni a program adatait.

\return
Nincs.

\details 

*/

void lepesElsoFotonnak(Adatok *parameter)
{
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    // elso Foton
    kovetkezo_utkozespont_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_devStates, parameter->_foton, parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    if(parameter->_lepes_szamlalo == 1)
    {
        parameter->_elso_lepesben_kirepult_szam += thrust::count(parameter->_thrust._kolcsonhatas_tipus, parameter->_thrust._kolcsonhatas_tipus+parameter->fotonSzalakSzama, 0);  // csak elso kolcsonhatas soran novelve
        check_cuda_errors(__FILE__, __LINE__);
        
        parameter->_elso_lepesben_fotoeff_szam  += thrust::count(parameter->_thrust._kolcsonhatas_tipus, parameter->_thrust._kolcsonhatas_tipus+parameter->fotonSzalakSzama, 1);  // csak elso kolcsonhatas soran novelve
        check_cuda_errors(__FILE__, __LINE__);
        
        parameter->_elso_lepesben_compton_szam  += thrust::count(parameter->_thrust._kolcsonhatas_tipus, parameter->_thrust._kolcsonhatas_tipus+parameter->fotonSzalakSzama, 2);  // csak elso kolcsonhatas soran novelve
        check_cuda_errors(__FILE__, __LINE__);
        
        parameter->_elso_lepesben_parkelt_szam  += thrust::count(parameter->_thrust._kolcsonhatas_tipus, parameter->_thrust._kolcsonhatas_tipus+parameter->fotonSzalakSzama, 3);  // csak elso kolcsonhatas soran novelve
        check_cuda_errors(__FILE__, __LINE__);
    
    }
    
    
    
    foton_aram_meghatarozas_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_foton, parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    
    for(int aram_index = 0; aram_index < 6; aram_index++)
    {
        parameter->_foton_aram_szam_2[aram_index] += thrust::reduce(parameter->_thrust._foton_aram_1 + (aram_index+0)*parameter->fotonSzalakSzama, parameter->_thrust._foton_aram_1 + (aram_index+1)*parameter->fotonSzalakSzama);
        check_cuda_errors(__FILE__, __LINE__);
    }
    
    
/*----------------------------------------------------------------------------------------------------*/
// kolcsonhatasok reszleteinek kiszamitasa
    
    kirepulo_fotonok_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama,parameter->_foton);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
//--------------
    
    
    fotoeffektus_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama,parameter->_foton);
    check_cuda_errors(__FILE__, __LINE__);
    
//--------------
    
    
    comptonszoras_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_devStates, parameter->_foton);
    check_cuda_errors(__FILE__, __LINE__);
    
//--------------
    
    parkeltes_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_devStates, parameter->_foton, parameter->_parkelt_foton);
    check_cuda_errors(__FILE__, __LINE__);
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    // voxelek dozisainak kigyujtesehez, voxel szerint novekvo sorrendbe rakva a fotonok megfelelo adatait
    thrust::sort_by_key(    parameter->_thrust._voxel, parameter->_thrust._voxel + parameter->fotonSzalakSzama, parameter->_thrust._leadott_energia );
//                          thrust::make_zip_iterator(make_tuple(   parameter->_thrust._leadott_energia,
//                                                                  leadott_energia_2)));
    
    
    check_cuda_errors(__FILE__, __LINE__);
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    // osszegyujtjuk, hogy melyik voxelben hany darab Foton osszesen mennyi energiat adott le (az adott lepesben):
    // (es teszteleshez kiirjuk az adataikat)
    
    parameter->_thrust._lista2_vege = thrust::reduce_by_key(parameter->_thrust._voxel, parameter->_thrust._voxel + parameter->fotonSzalakSzama, parameter->_thrust._leadott_energia, parameter->_thrust._sorszam_gyujto, parameter->_thrust._dozis_gyujto);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    parameter->_lista2_hossz = parameter->_thrust._lista2_vege.first - parameter->_thrust._sorszam_gyujto;
    
    parameter->_gyujto_blockSize = parameter->_blockSize;
    parameter->_gyujto_gridSize = parameter->_lista2_hossz / parameter->_gyujto_blockSize + ( (parameter->_lista2_hossz % parameter->_gyujto_blockSize == 0) ? 0:1);
    
    
    // az adott lepesben tortent beuteseket feljegyezzuk a voxelek teljes listajaban:
    voxel_kigyujt_lepesenkent_kernel<<<parameter->_gyujto_gridSize, parameter->_gyujto_blockSize>>>(parameter->_lista2_hossz,  parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    // kovetkezo lepes inditasa elott a korabbi energialeadasok lenullazasa:
    beutes_nullaz_lepesenkent_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_foton);
    check_cuda_errors(__FILE__, __LINE__);
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    parameter->_van_aktiv_foton = thrust::reduce(parameter->_thrust._aktiv_foton, parameter->_thrust._aktiv_foton+parameter->fotonSzalakSzama, -1, thrust::maximum<int>());
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    
    
    
    check_cuda_errors(__FILE__, __LINE__);
}


 