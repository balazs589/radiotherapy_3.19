#include "adatokMasolasaHostra.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"



/*!
\ingroup wrapper_fuggvenyek_group

\brief
Szimuláció eredményeinek grafikus kártyáról host memóriába másolása.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void adatokMasolasaHostra(Adatok *parameter)
{
    
    CUDA_CALL(cudaMemcpy(parameter->_h_szigma.dozis_atlag,  parameter->_d_szigma.dozis_atlag,   parameter->_h_szigma.voxelek_szama * sizeof(double), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_h_szigma.dozis_szoras, parameter->_d_szigma.dozis_szoras,  parameter->_h_szigma.voxelek_szama * sizeof(double), cudaMemcpyDeviceToHost));
    
    
    CUDA_CALL(cudaMemcpy(parameter->_h_szigma.csatornak, parameter->_d_szigma.csatornak,  parameter->_h_szigma.csatornak_szama * sizeof(long long int), cudaMemcpyDeviceToHost));
    
    
    // osszes kirepult energiahoz:
    CUDA_CALL( cudaMemcpy( &parameter->_osszes_kirepult_energia, parameter->_d_szigma.kirepult_energia, 1*sizeof(double), cudaMemcpyDeviceToHost));
    
    // detektorban 0 energiat leado fotonok
    CUDA_CALL( cudaMemcpy( &parameter->_osszes_detektor_0_energias_fotonok_szama, parameter->_d_szigma.detektor_0_energias_fotonok_szama, 1*sizeof(long long int), cudaMemcpyDeviceToHost));
    
    
}


 