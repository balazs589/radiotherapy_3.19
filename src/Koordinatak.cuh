#ifndef KOORDINATAK_CUH
#define KOORDINATAK_CUH



/*!
    \ingroup Osztalyok_goup
    
    \brief 3D vektor adatok csoportba foglalásához GPU-n (tömbök struktúrája).
    
    \details
    
*/

class Koordinatak
{
public:
    double* x;      //!< 
    double* y;      //!< 
    double* z;      //!< 
};


#endif
