#ifndef KORLAP_KERNEL_CUH
#define KORLAP_KERNEL_CUH

#include <curand_kernel.h>
#include "Korlap.cuh"
#include "Allapot.cuh"


__global__ void korlap_kernel(int N, curandState *state, Korlap kezdo_kor, Allapot foton);



#endif
