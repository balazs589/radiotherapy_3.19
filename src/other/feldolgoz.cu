//#include <cstdio>
//#include <cstdlib>
#include <fstream>
//#include <vector>
//#include <string>
//#include <sstream>
#include <iostream>
//#include <iomanip>




int main(int argc, char** argv)
{
    
    
    std::cout << "eredmeny feldolgozasa" << std::endl;
    
    
    std::ofstream voxel_metszet_01("./output/generalt_adatok/adatok/voxel_metszet_01.txt");
    std::ofstream voxel_metszet_02("./output/generalt_adatok/adatok/voxel_metszet_02.txt");
    std::ofstream voxel_metszet_03("./output/generalt_adatok/adatok/voxel_metszet_03.txt");
    std::ofstream voxel_metszet_04("./output/generalt_adatok/adatok/voxel_metszet_04.txt");
    std::ofstream voxel_metszet_04_szoras("./output/generalt_adatok/adatok/voxel_metszet_04_szoras.txt");
    
    
    
    std::streampos size_of_bytes;
    std::ifstream infile;
    
    double* dozis_atlag = NULL;
    double* dozis_szoras = NULL;
    
    
    
    
    infile.open("./output/eredmeny/dozis_atlag.dat", std::ios::in | std::ios::binary | std::ios::ate);
    if(infile.is_open())
    {
        size_of_bytes = infile.tellg();
        dozis_atlag = (double*)malloc(size_of_bytes*sizeof(char));
        if(dozis_atlag == NULL)
        {
            std::cout << std::endl;
            std::cout << "dozis_atlag.dat memoriafoglalas hiba" << std::endl;
            std::cout << std::endl;
            exit(-1);
        }
        
        infile.seekg(0, std::ios::beg);
        infile.read((char*)dozis_atlag, size_of_bytes);
        infile.close();
    }
    else
    {
        std::cout << std::endl;
        std::cout << "dozis_atlag.dat fajl olvasas hiba" << std::endl;
        std::cout << std::endl;
        exit(-1);
    }
    
    
    int voxelek_szama = size_of_bytes / sizeof(double);
    if(voxelek_szama != 250*250*250)
    {
        std::cout << "atlag voxelek_szama nem megfelelo: " << voxelek_szama << std::endl;
        exit(EXIT_FAILURE);
    }
    
    int nx = 250;
    int ny = 250;
    int nz = 250;
    
    // legfelso (x-y) metszet
    for(int iii = 0; iii < (nx*ny); iii++)
    {
        if(iii%nx == 0)
        {
            voxel_metszet_01 << std::endl;
        }
        
        voxel_metszet_01 << dozis_atlag[iii + (nz - 1)*(nx*ny)] << "\t";
    }
    
    // kozepso (x-y) metszet
    for(int iii = 0; iii < (nx*ny); iii++)
    {
        if(iii%nx == 0)
        {
            voxel_metszet_02 << std::endl;
        }
        
        voxel_metszet_02 << dozis_atlag[iii + (int(nz/2))*(nx*ny)] << "\t";
    }
    
    // legalso (x-y) metszet
    for(int iii = 0; iii < (nx*ny); iii++)
    {
        if(iii%nx == 0)
        {
            voxel_metszet_03 << std::endl;
        }
        
        voxel_metszet_03 << dozis_atlag[iii] << "\t";
    }
    
    
    
    
    // fuggoleges (y-z) metszet kozepen
    for(int iii = 0; iii < (ny*nz); iii++)
    {
        if(iii%ny == 0)
        {
            voxel_metszet_04 << std::endl;
        }
        
        voxel_metszet_04 << dozis_atlag[int(nx/2) + iii*ny ] << "\t";
    }
    
    
    
    
/////////////////////
    
    infile.open("./output/eredmeny/dozis_szoras.dat", std::ios::in | std::ios::binary | std::ios::ate);
    if(infile.is_open())
    {
        size_of_bytes = infile.tellg();
        dozis_szoras = (double*)malloc(size_of_bytes*sizeof(char));
        infile.seekg(0, std::ios::beg);
        infile.read((char*)dozis_szoras, size_of_bytes);
        infile.close();
    }
    
    voxelek_szama = size_of_bytes / sizeof(double);
    if(voxelek_szama != 250*250*250)
    {
        std::cout << "szoras voxelek_szama nem megfelelo: " << voxelek_szama << std::endl;
        exit(EXIT_FAILURE);
    }
    
    
    // relativ szoras (korrigalatlan empirikus szoras)
    for(int iii = 0; iii < (ny*nz); iii++)
    {
        if(iii%ny == 0)
        {
            voxel_metszet_04_szoras << std::endl;
        }
//relativ szoras:
        voxel_metszet_04_szoras << dozis_szoras[int(nx/2) + iii*ny ] / dozis_atlag[int(nx/2) + iii*ny ] << "\t";
//abszolut szoras:
        //voxel_metszet_04_szoras << dozis_szoras[int(nx/2) + iii*ny ] << "\t";
    }
    
    
    
    
    free(dozis_atlag);
    free(dozis_szoras);
    
    
    voxel_metszet_01.close();
    voxel_metszet_02.close();
    voxel_metszet_03.close();
    voxel_metszet_04.close();
    voxel_metszet_04_szoras.close();
    
    
    return EXIT_SUCCESS;
}
