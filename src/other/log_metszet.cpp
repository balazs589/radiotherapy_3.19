#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>




int feldolgoz(const char* input_file_neve, const char* output_file_neve, int oszlopok_szama)
{
    int sorok_szama = 0;
//  int oszlopok_szama = 250;   
    double tmp;

    std::ofstream output_file(output_file_neve);
    std::ifstream input_file(input_file_neve);
    
    // ha megnyithato a fajl, akkor sorrol sorra beolvassuk
    // es a sorok elso ket oszlopat egy-egy tombbe mentjuk
    if(input_file.is_open())
    {       
        std::vector<std::string> sorok;
        std::string aktualis_sor;
        
        while(std::getline(input_file, aktualis_sor))
        {
            if(aktualis_sor.empty()) continue;
            sorok.push_back(aktualis_sor);
        }
        
        sorok_szama = sorok.size();

        for(int iii = 0; iii < sorok_szama; iii++)
        {
            std::istringstream iss(sorok[iii]);
            
            for(int iii = 0; iii<oszlopok_szama; iii++)
            {
                iss >> tmp;
                output_file << log(tmp) << "\t";
            }

            output_file << std::endl;
            
        }

        input_file.close();
        output_file.close();

        std::cout << "# " << input_file_neve << " feldolgozva" << std::endl;
        std::cout << "# " << "beolvasott sorok szama: " << sorok_szama << std::endl;
    }
    else
    {
        std::cout << input_file_neve << " nem talalhato" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    return sorok_szama;
}









int main(int argc, char** argv)
{

    int oszlopok_szama = atoi(argv[1]);

//  int sorok = 250;
//  int oszlopok = 250;


    feldolgoz("../output/generalt_adatok/adatok/voxel_metszet_01.txt", "../output/generalt_adatok/adatok/log_voxel_metszet_01.txt", oszlopok_szama);
    feldolgoz("../output/generalt_adatok/adatok/voxel_metszet_02.txt", "../output/generalt_adatok/adatok/log_voxel_metszet_02.txt", oszlopok_szama);
    feldolgoz("../output/generalt_adatok/adatok/voxel_metszet_03.txt", "../output/generalt_adatok/adatok/log_voxel_metszet_03.txt", oszlopok_szama);
    feldolgoz("../output/generalt_adatok/adatok/voxel_metszet_04.txt", "../output/generalt_adatok/adatok/log_voxel_metszet_04.txt", oszlopok_szama);


    return 0;
}
