#include "dozis_atlag_frissit_kiserletenkent_kernel.cuh"


#include "Hataskeresztmetszet.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Minden kisérlet után a voxelenkénti dózisok átlagának és szorásának
meghatározásához szükséges mennyiségeket frissítjük.

\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void dozis_atlag_frissit_kiserletenkent_kernel(Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < szigma.voxelek_szama)
    {
        double utolso_dozis = szigma.dozis[sorszam];
        
        szigma.dozis_atlag[sorszam]     += utolso_dozis;
        
        szigma.dozis_szoras[sorszam]    += utolso_dozis * utolso_dozis;
        
    }
}

