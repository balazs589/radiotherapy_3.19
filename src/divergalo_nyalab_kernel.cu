#include "divergalo_nyalab_kernel.cuh"

#include <curand_kernel.h>
#include "Korlap.cuh"
#include "Allapot.cuh"
#include "Vektor3D.cuh"
#include "izotrop_kup_Device.cuh"
#include "bazis_transzformacio_Device.cuh"


#include "marsaglia_izotrop_irany_Device.cuh"


/*!
\ingroup Kernel_fuggvenyek_group

\brief
A körlap normálvektorával egybeeső tengelyű kúpban izotróp iránysorsolás.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] state CUDA random generátor állapota.
\param [in] kezdo_kor Induló koordináták tartománya: középponttal, sugárral és normálvektorral megadva. 
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void divergalo_nyalab_kernel(int N, curandState *state, Korlap kezdo_kor, Allapot foton)
{
    
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam<N)
    {
        curandState localState = state[sorszam];
        
        
        
        Vektor3D tengely_irany;
        
        tengely_irany.x = kezdo_kor.normalv_x;
        tengely_irany.y = kezdo_kor.normalv_y;
        tengely_irany.z = kezdo_kor.normalv_z;
        
        Vektor3D irany;
        
        irany = izotrop_kup_Device(kezdo_kor.cos_alfa, &localState);        // random irany a tengelyhez rogzitett koordinatarendszerben
        
    //-----------------------------------------
    //teszt
    //irany = izotrop_kup_Device(-1, &localState);              // teszt
    //-----------------------------------------
        
        irany = bazis_transzformacio_Device(tengely_irany, irany);          // irany a laborrendszerben
        
        
    //-----------------------------------------
    //vagy teszt
    //irany = marsaglia_izotrop_irany_Device(&localState);      // teszt
    //-----------------------------------------
        
        
        
        foton.elozo_irany.x[sorszam] = irany.x;     //
        foton.elozo_irany.y[sorszam] = irany.y;     //
        foton.elozo_irany.z[sorszam] = irany.z;     // foton kezdo iranyanak beallitasa
        
        
        
        state[sorszam] = localState;
        
    }
    
    
}
