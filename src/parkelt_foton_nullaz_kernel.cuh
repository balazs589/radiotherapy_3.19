#ifndef PARKELT_FOTON_NULLAZ_KERNEL_CUH
#define PARKELT_FOTON_NULLAZ_KERNEL_CUH

#include "Allapot.cuh"


__global__ void parkelt_foton_nullaz_kernel(int N, Allapot parkelt_foton);


#endif
