#include "tablazat2_beolvas.cuh"


#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

/*!
\ingroup CPU_fuggvenyek_group

\brief
Számadatokat 2 oszlopban tartalmazó szöveges fájl beolvasása 2 tömbbe.

\param [in] input_file_neve Olvasásra megnyitandó szöveges fájl neve.
\param [out] oszlop_1 1. tömbre mutató pointer.
\param [out] oszlop_2 2. tömbre mutató pointer.
\return Adatokat tartalmazó sorok száma, a tömbök elemszáma.

\details 

*/

// (c++ vector, string és stringstream hasznalataval)

int tablazat2_beolvas(const char* input_file_neve, double** oszlop_1, double** oszlop_2)
{
    int sorok_szama = 0;
    
    std::ifstream input_file(input_file_neve);
    
    // ha megnyithato a fajl, akkor sorrol sorra beolvassuk
    // es a sorok elso ket oszlopat egy-egy tombbe mentjuk
    if(input_file.is_open())
    {       
        std::vector<std::string> sorok;
        std::string aktualis_sor;
        
        while(std::getline(input_file, aktualis_sor))
        {
            if(aktualis_sor.empty()) continue;
            sorok.push_back(aktualis_sor);
        }
        
        sorok_szama = sorok.size();
        *oszlop_1 = (double*)malloc(sorok_szama*sizeof(double));
        *oszlop_2 = (double*)malloc(sorok_szama*sizeof(double));
        
        for(int iii = 0; iii < sorok_szama; iii++)
        {
            std::istringstream iss(sorok[iii]);

            iss >> (*oszlop_1)[iii];
            iss >> (*oszlop_2)[iii];
        }

        input_file.close();
        std::cout << "# " << input_file_neve << " feldolgozva" << std::endl;
        std::cout << "# " << "beolvasott sorok szama: " << sorok_szama << std::endl;
    }
    else
    {
        std::cout << input_file_neve << " nem talalhato" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    return sorok_szama;
}

