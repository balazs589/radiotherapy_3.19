#include "beutes_nullaz_lepesenkent_kernel.cuh"

#include "Allapot.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Minden lépés után nullázzuk a korábbi beütéseket.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 
(`foton.voxel`, `foton.leadott_energia`, `foton.foton_aram_1`)
*/


__global__ void beutes_nullaz_lepesenkent_kernel(int N, Allapot foton)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < N)
    {
        
        foton.voxel[sorszam] = -1;
        foton.leadott_energia[sorszam] = 0.0;
        
        for(int iii = 0; iii<6; iii++)
        {
            foton.foton_aram_1[sorszam + iii*N] = 0;
        }
        
    }
}
