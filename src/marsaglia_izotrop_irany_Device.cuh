#ifndef MARSAGLIA_IZOTROP_IRANY_DEVICE_CUH
#define MARSAGLIA_IZOTROP_IRANY_DEVICE_CUH

#include <curand_kernel.h>
#include "Vektor3D.cuh"


__device__ Vektor3D marsaglia_izotrop_irany_Device(curandState* localState);


#endif
