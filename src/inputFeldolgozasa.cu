#include "inputFeldolgozasa.cuh"

#include "constants.h"      // pi es elektron nyugalmi energia
#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"


#include "tablazat2_beolvas.cuh"
#include "tablazat4_beolvas.cuh"


/*!
\ingroup CPU_fuggvenyek_group

\brief
Parancssori paramétereket és input fájlokat feldolgozó függvény.

\param [in, out] parameter Ezen a változón keresztül érhető el a program összes globálisan használt adata.
\return Nincs.

\details 

*/

void inputFeldolgozasa(Adatok *parameter)
{
    
    
    
    parameter->_seed = 1;                           // GPU random generatoranak, reprodukalhatosag erdekeben nem valtoztatva
    //parameter->_seed = atoi(parameter->_argv[10]);  // lehetne program bemeno parameter is
    //parameter->_seed = 123456789;
    
    // az egesz programban valtoztathato a default blokkmeret:
    parameter->_blockSize = 128;      // gyujto_blockSize es _detektor_gyujto_blockSize
    // kulonbozo ertek fotonokra es voxelekre kesobb beallitva 
    
//    int N = atoi(parameter->_argv[1]); // (korabbi N valtozo atnevezve:)
    // annyi darab fotont inditunk egyszerre amennyit
    // a program inditasakor parameterkent megadunk
    parameter->fotonSzalakSzama = atoi(parameter->_argv[1]);
    
    // kiserletenkent ennyiszer:
    parameter->_ciklusok_szama = atoi(parameter->_argv[2]);
    
    // ennyi kiserletet vegzunk:
    parameter->_kiserletek_szama = atoi(parameter->_argv[3]);   
    
    
    parameter->_kocka_meret = atof(parameter->_argv[4]); // [cm]
    
    parameter->_voxel_meret = atof(parameter->_argv[5]); // [cm]
    
    parameter->_kezdopont_x = atof(parameter->_argv[6]); // [cm]
    parameter->_kezdopont_y = atof(parameter->_argv[7]); // [cm]
    parameter->_kezdopont_z = atof(parameter->_argv[8]); // [cm]
    
    parameter->_kezdoirany_x = atof(parameter->_argv[ 9]); // [1]
    parameter->_kezdoirany_y = atof(parameter->_argv[10]); // [1]
    parameter->_kezdoirany_z = atof(parameter->_argv[11]); // [1]

    
    // fotonyalab kezdeti atmero:
    parameter->_kor_sugara = atof(parameter->_argv[12]); // [cm]
    
    // izotrop kup felnyilasszoge:
    parameter->_divergencia_fok = atof(parameter->_argv[13]); // [°]
    
    
    
        
    // fotonaram vizsgalatahoz feluletek:
    // korabban 50cm x 50cm x 50cm kockamerethez tervezve:
    // kocka kozepen egymas alatt tobb darab vizszintes teglalap oldalvonalai:
    
    parameter->_felulet_x_min = 0.48 * parameter->_kocka_meret;
    parameter->_felulet_x_max = 0.52 * parameter->_kocka_meret;
    parameter->_felulet_y_min = 0.48 * parameter->_kocka_meret;
    parameter->_felulet_y_max = 0.52 * parameter->_kocka_meret;
        

    // magassagok (kocka also es felso lapja kozott korabban: 0-50 cm)
    parameter->_felulet1_z = 0.9999999999 * parameter->_kocka_meret;    // [cm]
    parameter->_felulet2_z = 0.8 * parameter->_kocka_meret;             // [cm]
    parameter->_felulet3_z = 0.6 * parameter->_kocka_meret;             // [cm]
    parameter->_felulet4_z = 0.4 * parameter->_kocka_meret;             // [cm]
    parameter->_felulet5_z = 0.2 * parameter->_kocka_meret;             // [cm]
    parameter->_felulet6_z = 0.0 * parameter->_kocka_meret;             // [cm]
    
        
    
    parameter->_detektor_file.open("./output/generalt_adatok/adatok/detektor_output.txt");
    
    // majd tesztelt adatok kiirasahoz:
    parameter->_elmeleti_eloszlas.open("./output/generalt_adatok/adatok/elmeleti_eloszlas.txt");
    
    
    
    
/*----------------------------------------------------------------------------------------------------*/
// spektrum adatok beolvasasa es letrehozasa host-on:
    
    parameter->_E_intervallumok = NULL;     // kulon megadott fajlban levo hisztogram energiaadatainak (E [MeV])
    parameter->_fluxus = NULL;              // fluxus adatoknak
    
    // adatok beolvasasa fajlbol
    parameter->_spektrum_sorok_szama = tablazat2_beolvas("./input/spektrum.txt", &parameter->_E_intervallumok, &parameter->_fluxus);
    // fluxus: dFI/dE
    
    std::cout << std::endl;
    
    // adatok indulo_energia_kernel altal hasznalhato formaba transzformalasa:
    
    // fluxus: dFI = dFI/dE * dE
    for(int iii = 1; iii < parameter->_spektrum_sorok_szama; iii++)
    {
        parameter->_fluxus[iii] = (parameter->_E_intervallumok[iii] - parameter->_E_intervallumok[iii-1]) * parameter->_fluxus[iii];
    }
    
    // FI = sum(dFI)
    parameter->_sum_fluxus = 0.0;
    for(int iii = 1; iii < parameter->_spektrum_sorok_szama; iii++)
    {
        parameter->_sum_fluxus += parameter->_fluxus[iii];
    }
    
    // fluxus: dFI / FI
    for(int iii = 1; iii < parameter->_spektrum_sorok_szama; iii++)
    {
        parameter->_fluxus[iii] /= parameter->_sum_fluxus;
    }
    
    // fluxus: eloszlas fugveny
    for(int iii = 1; iii<parameter->_spektrum_sorok_szama; iii++)
    {
        parameter->_fluxus[iii] += parameter->_fluxus[iii-1];
    }
    parameter->_fluxus[parameter->_spektrum_sorok_szama-1] = 1.0;
    
    // atterve darabszamra:
    // az adott energianal kisebb energiaju fotonok szama
    parameter->_foton_eloszlas = (int*)malloc(parameter->_spektrum_sorok_szama*sizeof(int));
    for(int iii = 0; iii<parameter->_spektrum_sorok_szama; iii++)
    {
        parameter->_foton_eloszlas[iii] = int(parameter->fotonSzalakSzama*parameter->_fluxus[iii]);
    }
    
    
    
    
    parameter->_csatornak_szama = 1000;     //[db]
    //parameter->_csatorna_max_energia = 20.0;     //[MeV]

    // erdemes spektrum maxiumumahoz rogziteni:
    parameter->_csatorna_max_energia = parameter->_E_intervallumok[parameter->_spektrum_sorok_szama-1] * 1.1;     //[MeV]
    
    
/*----------------------------------------------------------------------------------------------------*/
    // anyagot tartalmazo terfogat adatainak letrehozasa es beolvasasa host-on:
   
    parameter->_h_szigma.x_min =  0.0;              //
    parameter->_h_szigma.x_max =   parameter->_kocka_meret;     //
    parameter->_h_szigma.y_min =  0.0;              //
    parameter->_h_szigma.y_max =   parameter->_kocka_meret;     //
    parameter->_h_szigma.z_min =  0.0;              //
    parameter->_h_szigma.z_max =   parameter->_kocka_meret;     // az anyagot tartalmazo vizsgalt terfogat egy kocka
    
    parameter->_h_szigma.dx = parameter->_voxel_meret;      //
    parameter->_h_szigma.dy = parameter->_voxel_meret;      //
    parameter->_h_szigma.dz = parameter->_voxel_meret;      // voxelek merete
    
    parameter->_h_szigma.nx = int( ceil( (parameter->_h_szigma.x_max - parameter->_h_szigma.x_min) / parameter->_h_szigma.dx ) );   //
    parameter->_h_szigma.ny = int( ceil( (parameter->_h_szigma.y_max - parameter->_h_szigma.y_min) / parameter->_h_szigma.dy ) );   //
    parameter->_h_szigma.nz = int( ceil( (parameter->_h_szigma.z_max - parameter->_h_szigma.z_min) / parameter->_h_szigma.dz ) );   // voxelek szama x-y-z iranyban
    
    parameter->_h_szigma.voxelek_szama = parameter->_h_szigma.nx * parameter->_h_szigma.ny * parameter->_h_szigma.nz;               // osszes voxel szama
    
    
    
    
    // fotonaram vizsgalatahoz felulet:
    parameter->_h_szigma.felulet_x_min = parameter->_felulet_x_min;
    parameter->_h_szigma.felulet_x_max = parameter->_felulet_x_max;
    parameter->_h_szigma.felulet_y_min = parameter->_felulet_y_min;
    parameter->_h_szigma.felulet_y_max = parameter->_felulet_y_max;
    
    parameter->_h_szigma.felulet1_z = parameter->_felulet1_z;       // elso felulet inditasi parameterkent mefadva
    parameter->_h_szigma.felulet2_z = parameter->_felulet2_z;
    parameter->_h_szigma.felulet3_z = parameter->_felulet3_z;
    parameter->_h_szigma.felulet4_z = parameter->_felulet4_z;
    parameter->_h_szigma.felulet5_z = parameter->_felulet5_z;
    parameter->_h_szigma.felulet6_z = parameter->_felulet6_z;
    
    parameter->_h_szigma.csatornak_szama = parameter->_csatornak_szama;
    parameter->_h_szigma.csatorna_max_energia = parameter->_csatorna_max_energia;
    parameter->_h_szigma.csatorna_energia_osztas = parameter->_csatorna_max_energia / double(parameter->_csatornak_szama);
    
    
    // XCOM adatok beolvasasa fajlbol
    // energia [MeV] fuggvenyeben mikroszkopikus hataskeresztmetszet [cm2/g]
    
    // majd a surusegek felhasznalasaval rogton makroszkopikus hataskeresztmetszetek meghatarozasa [1/cm]
    
    parameter->_h_szigma.anyagfajtak_szama = 1;     // ennyi fajta tablazatot olvasunk be es ennyi fele kulonbozo suruseg lehet
                                                    // (1,2,3: viz, aluminium, nitrogengaz)
                                                    // mindenhol (input tablazatok, voxel_anyagtipus_beallitas_kernel,
                                                    // kovetkezo_utkozespont_kernel... ) ennek megfeleloen kell elkesziteni mindent
    
    
    switch(parameter->_h_szigma.anyagfajtak_szama)
    {
//-----------------------------------------------------------------------------
        // 1 anyagtipus: viz
        case 1:
        {
            parameter->_h_szigma.rho = (double*)malloc(parameter->_h_szigma.anyagfajtak_szama * sizeof(double));
            
            // surusegek megadasa csak statikusan (anyagfajtak_szama = 1)
            // elso anyag: víz
            
            parameter->_h_szigma.rho[0] = 1.0;              // [g/cm3] H2O
            //parameter->_h_szigma.rho[1] = 2.7;              // [g/cm3] Al
            //parameter->_h_szigma.rho[2] = 0.00125;          // [g/cm3] N2
            
            
            
            
            double* oszlop_1;       //
            double* oszlop_2;       //
            double* oszlop_3;       //
            double* oszlop_4;       // ideiglenes tarolok a beolvasashoz
            
            // 1. tablazat mikroszkopikus hataskeresztmetszetekkel [cm2/g]
            parameter->_h_szigma.sorok_szama = tablazat4_beolvas("./input/01_hataskeresztmetszet_H2O.txt", &(oszlop_1), &(oszlop_2), &(oszlop_3), &(oszlop_4));
            
            
            
            std::cout << std::endl;
            
            parameter->_h_szigma.energia = (double*)malloc(                          1  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.fotoeff = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.compton = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.parkelt = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            
            
            // 1. anyag makroszkopikus hataskeresztmetszeteinek [1/cm] szamitasa es rogzitese
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.energia[iii] = oszlop_1[iii];
                parameter->_h_szigma.fotoeff[iii] = parameter->_h_szigma.rho[1-1] * oszlop_2[iii];
                parameter->_h_szigma.compton[iii] = parameter->_h_szigma.rho[1-1] * oszlop_3[iii];
                parameter->_h_szigma.parkelt[iii] = parameter->_h_szigma.rho[1-1] * oszlop_4[iii];
            }
            
            free(oszlop_1);
            free(oszlop_2);
            free(oszlop_3);
            free(oszlop_4);    
            
            
            
            // totalis hataskeresztmetszetek [1/cm] kiszamitasa minden anyagra:
            parameter->_h_szigma.totalis = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama * parameter->_h_szigma.sorok_szama*sizeof(double));
            for(int iii = 0; iii < parameter->_h_szigma.anyagfajtak_szama * parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.totalis[iii] = parameter->_h_szigma.fotoeff[iii] + parameter->_h_szigma.compton[iii] + parameter->_h_szigma.parkelt[iii];
            }
            
            parameter->_h_szigma.szupremum = (double*)malloc( parameter->_h_szigma.sorok_szama*sizeof(double));
            
            // minden energiacsoportra totalis makroszkopikus hataskeresztmetszetek szupremumanak meghatarozasa
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.szupremum[iii] = 0.0;
                
                for(int jjj = 0; jjj < parameter->_h_szigma.anyagfajtak_szama; jjj++)
                {
                    if(parameter->_h_szigma.totalis[jjj * parameter->_h_szigma.sorok_szama + iii] > parameter->_h_szigma.szupremum[iii])
                    {
                        parameter->_h_szigma.szupremum[iii] = parameter->_h_szigma.totalis[jjj * parameter->_h_szigma.sorok_szama + iii];
                    }
                }
                
            }
        
        }
        break;
        
        
        
        
//-----------------------------------------------------------------------------
        // 2 anyagtipus: NaI, vakuum
        case 2:
        {
            parameter->_h_szigma.rho = (double*)malloc(parameter->_h_szigma.anyagfajtak_szama * sizeof(double));
            
            // surusegek megadasa csak statikusan (anyagfajtak_szama = 2)
            // NaI teszthez: NaI - levego
            parameter->_h_szigma.rho[0] = 3.67;              // [g/cm3] NaI
            parameter->_h_szigma.rho[1] = 0.00;              // [g/cm3]
            
            
            double* oszlop_1;       //
            double* oszlop_2;       //
            double* oszlop_3;       //
            double* oszlop_4;       // ideiglenes tarolok a beolvasashoz
            
            // 1. tablazat mikroszkopikus hataskeresztmetszetekkel [cm2/g]
            // NaI teszthez:
            parameter->_h_szigma.sorok_szama = tablazat4_beolvas("./input/00_hataskeresztmetszet_NaI.txt", &(oszlop_1), &(oszlop_2), &(oszlop_3), &(oszlop_4));
            std::cout << std::endl;
            
            parameter->_h_szigma.energia = (double*)malloc(                          1  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.fotoeff = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.compton = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.parkelt = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            
            
            // 1. anyag makroszkopikus hataskeresztmetszeteinek [1/cm] szamitasa es rogzitese
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.energia[iii] = oszlop_1[iii];
                parameter->_h_szigma.fotoeff[iii] = parameter->_h_szigma.rho[1-1] * oszlop_2[iii];
                parameter->_h_szigma.compton[iii] = parameter->_h_szigma.rho[1-1] * oszlop_3[iii];
                parameter->_h_szigma.parkelt[iii] = parameter->_h_szigma.rho[1-1] * oszlop_4[iii];
            }
            
            free(oszlop_1);
            free(oszlop_2);
            free(oszlop_3);
            free(oszlop_4);    
            
            // most vakkum lesz a rho = 0 miatt ->
            // 2. tablazat mikroszkopikus hataskeresztmetszetekkel [cm2/g]
            parameter->_h_szigma.sorok_szama = tablazat4_beolvas("./input/00_hataskeresztmetszet_NaI.txt", &(oszlop_1), &(oszlop_2), &(oszlop_3), &(oszlop_4));
            std::cout << std::endl;
            
            // 2.anyag makroszkopikus hataskeresztmetszeteinek [1/cm] szamitasa es rogzitese
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.fotoeff[parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[2-1] * oszlop_2[iii];
                parameter->_h_szigma.compton[parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[2-1] * oszlop_3[iii];
                parameter->_h_szigma.parkelt[parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[2-1] * oszlop_4[iii];
            }
            
            free(oszlop_1);
            free(oszlop_2);
            free(oszlop_3);
            free(oszlop_4);
            
            
            
            // totalis hataskeresztmetszetek [1/cm] kiszamitasa minden anyagra:
            parameter->_h_szigma.totalis = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama * parameter->_h_szigma.sorok_szama*sizeof(double));
            for(int iii = 0; iii < parameter->_h_szigma.anyagfajtak_szama * parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.totalis[iii] = parameter->_h_szigma.fotoeff[iii] + parameter->_h_szigma.compton[iii] + parameter->_h_szigma.parkelt[iii];
            }
            
            parameter->_h_szigma.szupremum = (double*)malloc( parameter->_h_szigma.sorok_szama*sizeof(double));
            
            // minden energiacsoportra totalis makroszkopikus hataskeresztmetszetek szupremumanak meghatarozasa
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.szupremum[iii] = 0.0;
                
                for(int jjj = 0; jjj < parameter->_h_szigma.anyagfajtak_szama; jjj++)
                {
                    if(parameter->_h_szigma.totalis[jjj * parameter->_h_szigma.sorok_szama + iii] > parameter->_h_szigma.szupremum[iii])
                    {
                        parameter->_h_szigma.szupremum[iii] = parameter->_h_szigma.totalis[jjj * parameter->_h_szigma.sorok_szama + iii];
                    }
                }
                
            }
        }
        break;
        
        
//-----------------------------------------------------------------------------
        // 3 anyagtipus: viz, aluminium, nitrogengaz
        case 3:
        {
            parameter->_h_szigma.rho = (double*)malloc(parameter->_h_szigma.anyagfajtak_szama * sizeof(double));
            
            // surusegek megadasa csak statikusan (anyagfajtak_szama = 3)
            parameter->_h_szigma.rho[0] = 1.0;              // [g/cm3] H2O
            parameter->_h_szigma.rho[1] = 2.7;              // [g/cm3] Al
            parameter->_h_szigma.rho[2] = 0.00125;          // [g/cm3] N2
            
            
            double* oszlop_1;       //
            double* oszlop_2;       //
            double* oszlop_3;       //
            double* oszlop_4;       // ideiglenes tarolok a beolvasashoz
            
            // 1. tablazat mikroszkopikus hataskeresztmetszetekkel [cm2/g]
            parameter->_h_szigma.sorok_szama = tablazat4_beolvas("./input/01_hataskeresztmetszet_H2O.txt", &(oszlop_1), &(oszlop_2), &(oszlop_3), &(oszlop_4));
            std::cout << std::endl;
            
            parameter->_h_szigma.energia = (double*)malloc(                          1  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.fotoeff = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.compton = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            parameter->_h_szigma.parkelt = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama  * parameter->_h_szigma.sorok_szama * sizeof(double));
            
            
            // 1. anyag makroszkopikus hataskeresztmetszeteinek [1/cm] szamitasa es rogzitese
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.energia[iii] = oszlop_1[iii];
                parameter->_h_szigma.fotoeff[iii] = parameter->_h_szigma.rho[1-1] * oszlop_2[iii];
                parameter->_h_szigma.compton[iii] = parameter->_h_szigma.rho[1-1] * oszlop_3[iii];
                parameter->_h_szigma.parkelt[iii] = parameter->_h_szigma.rho[1-1] * oszlop_4[iii];
            }
            
            free(oszlop_1);
            free(oszlop_2);
            free(oszlop_3);
            free(oszlop_4);    
            
            
            // 2. tablazat mikroszkopikus hataskeresztmetszetekkel [cm2/g]
            parameter->_h_szigma.sorok_szama = tablazat4_beolvas("./input/02_hataskeresztmetszet_Al.txt", &(oszlop_1), &(oszlop_2), &(oszlop_3), &(oszlop_4));
            std::cout << std::endl;
            
            // 2.anyag makroszkopikus hataskeresztmetszeteinek [1/cm] szamitasa es rogzitese
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.fotoeff[parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[2-1] * oszlop_2[iii];
                parameter->_h_szigma.compton[parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[2-1] * oszlop_3[iii];
                parameter->_h_szigma.parkelt[parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[2-1] * oszlop_4[iii];
            }
            
            free(oszlop_1);
            free(oszlop_2);
            free(oszlop_3);
            free(oszlop_4);

            
            
            // 3. tablazat mikroszkopikus hataskeresztmetszetekkel [cm2/g]
            parameter->_h_szigma.sorok_szama = tablazat4_beolvas("./input/03_hataskeresztmetszet_N2.txt", &(oszlop_1), &(oszlop_2), &(oszlop_3), &(oszlop_4));
            std::cout << std::endl;
            
            // 3. anyag makroszkopikus hataskeresztmetszeteinek [1/cm] szamitasa es rogzitese
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.fotoeff[2 * parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[3-1] * oszlop_2[iii];
                parameter->_h_szigma.compton[2 * parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[3-1] * oszlop_3[iii];
                parameter->_h_szigma.parkelt[2 * parameter->_h_szigma.sorok_szama + iii] = parameter->_h_szigma.rho[3-1] * oszlop_4[iii];
            }
            
            free(oszlop_1);
            free(oszlop_2);
            free(oszlop_3);
            free(oszlop_4);
            
            
            // totalis hataskeresztmetszetek [1/cm] kiszamitasa minden anyagra:
            parameter->_h_szigma.totalis = (double*)malloc( parameter->_h_szigma.anyagfajtak_szama * parameter->_h_szigma.sorok_szama*sizeof(double));
            for(int iii = 0; iii < parameter->_h_szigma.anyagfajtak_szama * parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.totalis[iii] = parameter->_h_szigma.fotoeff[iii] + parameter->_h_szigma.compton[iii] + parameter->_h_szigma.parkelt[iii];
            }
            
            parameter->_h_szigma.szupremum = (double*)malloc( parameter->_h_szigma.sorok_szama*sizeof(double));
            
            // minden energiacsoportra totalis makroszkopikus hataskeresztmetszetek szupremumanak meghatarozasa
            for(int iii = 0; iii < parameter->_h_szigma.sorok_szama; iii++)
            {
                parameter->_h_szigma.szupremum[iii] = 0.0;
                
                for(int jjj = 0; jjj < parameter->_h_szigma.anyagfajtak_szama; jjj++)
                {
                    if(parameter->_h_szigma.totalis[jjj * parameter->_h_szigma.sorok_szama + iii] > parameter->_h_szigma.szupremum[iii])
                    {
                        parameter->_h_szigma.szupremum[iii] = parameter->_h_szigma.totalis[jjj * parameter->_h_szigma.sorok_szama + iii];
                    }
                }
                
            }
        
        }
        break;
        
        
        //default:
        
    } // end switch(parameter->_h_szigma.anyagfajtak_szama) 1, 2, 3
    
    
    
    
/*----------------------------------------------------------------------------------------------------*/
    // voxelek surusegenek beolvasasa "voxelek_suruseg.dat" fajlbol parameter->_h_szigma.voxel_rho -n
    // keresztul elerheto memoriaba host-on
    
    std::streampos rho_fajlmeret;
    std::ifstream  rho_fajlpointer;
    
    parameter->_h_szigma.voxel_rho = NULL;
    
    // beolvasas:
    rho_fajlpointer.open("./voxelek_suruseg.dat", std::ios::in | std::ios::binary | std::ios::ate);
    if(rho_fajlpointer.is_open())
    {
        rho_fajlmeret = rho_fajlpointer.tellg();
        parameter->_h_szigma.voxel_rho = (double*)calloc(rho_fajlmeret,sizeof(char));
        if(parameter->_h_szigma.voxel_rho == NULL)
        {
            std::cout << std::endl;
            std::cout << "voxelek_suruseg.dat memoriafoglalas hiba" << std::endl;
            std::cout << std::endl;
            exit(EXIT_FAILURE);
        }
        
        rho_fajlpointer.seekg(0, std::ios::beg);
        rho_fajlpointer.read((char*)parameter->_h_szigma.voxel_rho, rho_fajlmeret);
        rho_fajlpointer.close();
    }
    else
    {
        std::cout << std::endl;
        std::cout << "voxelek_suruseg.dat fajl olvasas hiba" << std::endl;
        std::cout << std::endl;
        exit(EXIT_FAILURE);
    }    
    
    
    // ellenorzes
    int rho_voxelek_szama = rho_fajlmeret / sizeof(double);
    if(rho_voxelek_szama != parameter->_h_szigma.voxelek_szama)
    {
        std::cout << "voxelek_suruseg.dat nem megfelelo, voxelek szama: " << rho_voxelek_szama << std::endl;
        std::cout << "nx * ny * nz : " << parameter->_h_szigma.voxelek_szama << std::endl;
        exit(EXIT_FAILURE);
    }    
    
    
    
    // maximalis suruseg meghatarozasa (jelenleg valojaban csak mri felvetel jel intenzitas)
    double maximum_rho = 0.0;
    for(int iii = 0; iii < parameter->_h_szigma.voxelek_szama; iii++)
    {
        if(parameter->_h_szigma.voxel_rho[iii] > maximum_rho)
        {
            maximum_rho = parameter->_h_szigma.voxel_rho[iii];
        }
    }
    
    // normalas 0-1 intervallumra -> majd ezzel az ertekkel lesz sulyozva
    // a voxelek totalis makroszkopikus hataskeresztmetszetete
    for(int iii = 0; iii < parameter->_h_szigma.voxelek_szama; iii++)
    {
        // linearis sulyozas
        parameter->_h_szigma.voxel_rho[iii] /= maximum_rho;
        
        // vagy egyebb fuggveny szerinti sulyozas
        //parameter->_h_szigma.voxel_rho[iii] = 2*sqrt(parameter->_h_szigma.voxel_rho[iii]);
        
        // MRI fajl helyett valtozatlan suruseg:
        // parameter->_h_szigma.voxel_rho[iii] = 1.0;
    }
    
    
    std::cout << "voxelek_suruseg.dat beolvasva " << std::endl;
    std::cout << "maximum MRI suly: " << maximum_rho << std::endl;
    
    
    
/*----------------------------------------------------------------------------------------------------*/
// anyagot tartalmazo terfogat adatai a GPU-n is hasznalt valtozoba:
    
    parameter->_d_szigma.sorok_szama = parameter->_h_szigma.sorok_szama;
    parameter->_d_szigma.anyagfajtak_szama = parameter->_h_szigma.anyagfajtak_szama;
    
    parameter->_d_szigma.x_min = parameter->_h_szigma.x_min;
    parameter->_d_szigma.x_max = parameter->_h_szigma.x_max;
    parameter->_d_szigma.y_min = parameter->_h_szigma.y_min;
    parameter->_d_szigma.y_max = parameter->_h_szigma.y_max;
    parameter->_d_szigma.z_min = parameter->_h_szigma.z_min;
    parameter->_d_szigma.z_max = parameter->_h_szigma.z_max;

    parameter->_d_szigma.dx = parameter->_h_szigma.dx;
    parameter->_d_szigma.dy = parameter->_h_szigma.dy;
    parameter->_d_szigma.dz = parameter->_h_szigma.dz;

    parameter->_d_szigma.nx = parameter->_h_szigma.nx;
    parameter->_d_szigma.ny = parameter->_h_szigma.ny;
    parameter->_d_szigma.nz = parameter->_h_szigma.nz;
    
    parameter->_d_szigma.voxelek_szama = parameter->_h_szigma.voxelek_szama;
    
    
    parameter->_d_szigma.felulet_x_min = parameter->_h_szigma.felulet_x_min;        
    parameter->_d_szigma.felulet_x_max = parameter->_h_szigma.felulet_x_max;
    parameter->_d_szigma.felulet_y_min = parameter->_h_szigma.felulet_y_min;
    parameter->_d_szigma.felulet_y_max = parameter->_h_szigma.felulet_y_max;
    
    parameter->_d_szigma.felulet1_z = parameter->_h_szigma.felulet1_z;
    parameter->_d_szigma.felulet2_z = parameter->_h_szigma.felulet2_z;
    parameter->_d_szigma.felulet3_z = parameter->_h_szigma.felulet3_z;
    parameter->_d_szigma.felulet4_z = parameter->_h_szigma.felulet4_z;
    parameter->_d_szigma.felulet5_z = parameter->_h_szigma.felulet5_z;
    parameter->_d_szigma.felulet6_z = parameter->_h_szigma.felulet6_z;
    
    
    parameter->_d_szigma.csatornak_szama            = parameter->_h_szigma.csatornak_szama;
    parameter->_d_szigma.csatorna_max_energia       = parameter->_h_szigma.csatorna_max_energia;
    parameter->_d_szigma.csatorna_energia_osztas    = parameter->_h_szigma.csatorna_energia_osztas;
    
    
    // egy lepesben legfeljebb ennyi kulonbozo voxelben tortenhet energialeadas (+1 a kirepult fotonoknak):
    parameter->_gyujto_meret = ((parameter->fotonSzalakSzama < parameter->_d_szigma.voxelek_szama) ? parameter->fotonSzalakSzama : parameter->_d_szigma.voxelek_szama) + 1;
    
    // csatornak szama +1 a nulla leadott energiaju fotonok szamolasahoz
    parameter->_csatorna_gyujto_meret = parameter->_d_szigma.csatornak_szama + 1;
    
    
    //kernel hivas parameterei lesznek:
    //parameter->_foton_blockSize = parameter->_blockSize;
    parameter->_foton_blockSize = 128;
    parameter->_foton_gridSize = parameter->fotonSzalakSzama / parameter->_foton_blockSize + ( (parameter->fotonSzalakSzama % parameter->_foton_blockSize == 0) ? 0:1);   //  parameter->fotonSzalakSzama: az osszes elinditott Foton szama (6MeV alatti fotonok szama)
    
    //parameter->_voxel_blockSize = parameter->_blockSize;
    parameter->_voxel_blockSize = 256;
    parameter->_voxel_gridSize = parameter->_d_szigma.voxelek_szama / parameter->_voxel_blockSize + ( (parameter->_d_szigma.voxelek_szama % parameter->_voxel_blockSize == 0) ? 0:1);
    
    
/*----------------------------------------------------------------------------------------------------*/
    
//    parameter->_kezdo_kor beallitasa
    
    
    
    parameter->_kezdo_kor.kozeppont_x = parameter->_kezdopont_x;
    parameter->_kezdo_kor.kozeppont_y = parameter->_kezdopont_y;        // korabban kocka felso lapjanak kozepe
    parameter->_kezdo_kor.kozeppont_z = parameter->_kezdopont_z;        // korabban inditas a kocka felszinetol valamekkora tavolsagra
    
    
    double vx = parameter->_kezdoirany_x;
    double vy = parameter->_kezdoirany_y;
    double vz = parameter->_kezdoirany_z;
    
    double norma = sqrt(vx*vx + vy*vy + vz*vz);
    
    vx /= norma;
    vy /= norma;
    vz /= norma;
    
    parameter->_kezdo_kor.normalv_x = vx;
    parameter->_kezdo_kor.normalv_y = vy;
    parameter->_kezdo_kor.normalv_z = vz;         // korabban csak y-z sikban dontve
    
    
    parameter->_kezdo_kor.sugar = parameter->_kor_sugara;
    
    // nyalab szettartasat jellemzo ertek
    double divergencia_rad = _PI_ * parameter->_divergencia_fok / 180.0;
    // nyalab szettartasa:
    parameter->_kezdo_kor.cos_alfa = cos(divergencia_rad);
    
    
    
    /*----------------------------------------------------------------------------------------------------*/
    
    // az egesz meres soran vagy kiserletenkent:
    parameter->_osszes_elinditott_foton = 0; // ciklus vegen novelve
    
    parameter->_elso_lepesben_fotoeff_szam  = 0;  // csak elso kolcsonhatas soran novelve
    parameter->_elso_lepesben_compton_szam  = 0;  // csak elso kolcsonhatas soran novelve
    parameter->_elso_lepesben_parkelt_szam  = 0;  // csak elso kolcsonhatas soran novelve
    parameter->_elso_lepesben_kirepult_szam = 0;  // csak elso kolcsonhatas soran novelve
    
    
    
    parameter->_osszes_fotoeff_szam = 0;     // ciklus vegen novelve
    parameter->_osszes_compton_szam = 0;     // ciklus vegen novelve
    parameter->_osszes_parkelt_szam = 0;     // ciklus vegen novelve
    
    parameter->_foton_aram_szam_2[0] = 0;
    parameter->_foton_aram_szam_2[1] = 0;
    parameter->_foton_aram_szam_2[2] = 0;
    parameter->_foton_aram_szam_2[3] = 0;
    parameter->_foton_aram_szam_2[4] = 0;
    parameter->_foton_aram_szam_2[5] = 0;
    // ciklus vegen novelve
    
    parameter->_osszes_elinditott_energia = 0.0;
    
    parameter->_osszes_leadott_energia = 0.0;
    
    parameter->_osszes_kirepult_energia = 0.0;
    
    parameter->_osszes_detektor_0_energias_fotonok_szama = 0;
    
    /*----------------------------------------------------------------------------------------------------*/
    
    parameter->_h_szigma.dozis_atlag  = (double*)malloc(parameter->_h_szigma.voxelek_szama * sizeof(double));
    parameter->_h_szigma.dozis_szoras = (double*)malloc(parameter->_h_szigma.voxelek_szama * sizeof(double));
    
    parameter->_h_szigma.csatornak     = (long long int*)    calloc(parameter->_h_szigma.csatornak_szama, sizeof(long long int));
    
    
    // memoriafoglalas a fotonokat leiro adatoknak hoston:
    parameter->_host_foton.energia               = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    
    parameter->_host_foton.elozo_hely.x          = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.elozo_hely.y          = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.elozo_hely.z          = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.elozo_irany.x         = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.elozo_irany.y         = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.elozo_irany.z         = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    
    parameter->_host_foton.aktiv_foton           = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    parameter->_host_foton.kolcsonhatas_tipus    = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    
    parameter->_host_foton.leadott_energia       = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.voxel                 = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    
    parameter->_host_foton.fotoeff_szam          = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    parameter->_host_foton.compton_szam          = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    parameter->_host_foton.parkelt_szam          = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    
    parameter->_host_foton.volt_kolcsonhatas     = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    parameter->_host_foton.allapot_kapcsolo      = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    
    
    parameter->_host_foton.foton_aram_1          = (int*)    calloc(6*parameter->fotonSzalakSzama, sizeof(int));
    
    //teszteleshez
    parameter->_host_foton.hkrm_csoport          = (int*)    calloc(parameter->fotonSzalakSzama, sizeof(int));
    parameter->_host_foton.hkrm_totalis          = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.lambda                = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.kiindulo_pont.x       = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.kiindulo_pont.y       = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    parameter->_host_foton.kiindulo_pont.z       = (double*) calloc(parameter->fotonSzalakSzama, sizeof(double));
    
    
    
    
    
}


 