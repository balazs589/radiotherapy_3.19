#ifndef CURAND_BEALLITAS_KERNEL_CUH
#define CURAND_BEALLITAS_KERNEL_CUH

#include <curand_kernel.h>


__global__ void cuRAND_beallitas_kernel(int N, curandState *state, int seed);


#endif
