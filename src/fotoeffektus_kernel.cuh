#ifndef FOTOEFFEKTUS_KERNEL_CUH
#define FOTOEFFEKTUS_KERNEL_CUH

#include "Allapot.cuh"


__global__ void fotoeffektus_kernel(int N, Allapot foton);


#endif
