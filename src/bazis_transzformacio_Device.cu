#include "bazis_transzformacio_Device.cuh"


/*!
\ingroup Device_fuggvenyek_group

\brief
Irányvektorok kúp-tengely koordinátarendszerbeli komponenseinek transzformációja labor koordinátarendszerbeli komponensekké.

\param [in] tengely 
Tengely iránya labor-rendszerben.
\param [in] elterules
Véletlen irány a tengelyhez rögzített koordinátarendszerben.

\return
Véletlen irány a laborhoz rögzített koordinátarendszerben.

\details 
Egy labor-rendszerben ismert irányú tengelyhez rögzített koordinátarendszerben megadott komponensekkel
rendelkező vektor, labor-rendszerhez rögzített koordinátarendszerbeli komponenseinek meghatározása.

Compton-szórás utáni új irány, valamint kúp alakú izotróp nyalábban 
fotonok irányanak meghatározásához.

Fehér Sándor:
Monte Carlo módszerek előadás és gyakorlat,
BME, 2013 tavaszi félév

*/


__device__ Vektor3D bazis_transzformacio_Device(Vektor3D tengely, Vektor3D elterules)
{
    Vektor3D labor_irany;

    double gyok_u2_v2 = sqrt(tengely.x*tengely.x + tengely.y*tengely.y);
    
    if(gyok_u2_v2 > 0)
    {
        double sin_fi = tengely.y / gyok_u2_v2;
        double cos_fi = tengely.x / gyok_u2_v2;
        
        double sin_theta = sqrt(1.0-tengely.z*tengely.z);
        double cos_theta = tengely.z;
        
        labor_irany.x = (sin_fi)    *elterules.x    +(cos_theta*cos_fi) *elterules.y    +(tengely.x)    *elterules.z;
        labor_irany.y = (-cos_fi)   *elterules.x    +(cos_theta*sin_fi) *elterules.y    +(tengely.y)    *elterules.z;
        labor_irany.z = (0.0)       *elterules.x    +(-sin_theta)       *elterules.y    +(tengely.z)    *elterules.z;
    }
    else
    {
        labor_irany.x = (tengely.z) * elterules.x;
        labor_irany.y = (tengely.z) * elterules.y;
        labor_irany.z = (tengely.z) * elterules.z;      
    }

    return labor_irany;
}

