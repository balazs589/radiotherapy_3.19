#include "nyalab_forgatas_Device.cuh"


#include "Vektor3D.cuh"


/*!
\ingroup Device_fuggvenyek_group

\brief
X-Y sík pontjait `t` normálvektorú síkba forgató transzformáció.

\param [in] t 
Az a normálvektor, amire merőleges síkba akarjuk forgatni az X-Y síkot.
\param [in] v
Egy (x,y,0) koordinátájú, X-Y síkban lévő ponthoz húzott helyvektor .

\return
Az eredetileg X-Y síkban lévő pont elforgatása utáni új koordinátáit megadó helyvektor.

\details 
A kezdeti fotonnyaláb elforgatásához.

Wettl Ferenc: Lineáris Algebra (TÁMOP jegyzet, 2011) <br>
http://tankonyvtar.ttk.bme.hu/pdf/14.pdf <br>
a 301. oldal 7.28 tétel alapján <br>

*/


__device__ Vektor3D nyalab_forgatas_Device(Vektor3D t, Vektor3D v)
{

    Vektor3D indulo_hely;
    
    
    double sin_alfa = sqrt(1.0 - t.z*t.z);

    double R11 = ( 1.0 - (1+t.z)*t.x*t.x );
    double R12 = ( -(1.0+t.z)*t.x*t.y );
//  double R13 = ( -t.x*sin_alfa );

    double R21 = ( -(1.0+t.z)*t.x*t.y );
    double R22 = ( 1.0 - (1.0+t.z)*t.y*t.y );
//  double R23 = ( -t.y*sin_alfa );

    double R31 = ( t.x*sin_alfa );
    double R32 = ( t.y*sin_alfa );
//  double R33 = ( 1.0 - (1.0+t.z)*(t.x*t.x + t.y*t.y) );
    

    indulo_hely.x = R11*v.x + R12*v.y;
    indulo_hely.y = R21*v.x + R22*v.y;
    indulo_hely.z = R31*v.x + R32*v.y;


    
// ha nem csak az x-y sik vektorait, hanem az egesz 3D teret szeretnenk transzformalni:

//  indulo_hely.x = R11*v.x + R12*v.y + R13*v.z;
//  indulo_hely.y = R21*v.x + R22*v.y + R23*v.z;
//  indulo_hely.z = R31*v.x + R32*v.y + R33*v.z;


    
    return indulo_hely;
}

