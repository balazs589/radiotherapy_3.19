#include "kahn_sorsolas_Device.cuh"

#include <curand_kernel.h>



/*!
\ingroup Device_fuggvenyek_group

\brief
Klein-Nishina formula szerinti új energia sorsolása Kahn módszerével.

\param [in] lambda1
Compton-hullámhossz: az elektron nyugalmi energiájának és a foton energiájának hányadosa.
\param [in,out] localState
CUDA random generátor állapota.

\return
Kahn módszer X változója. A Compton-szórás során az új és a régi energia hányadosát megadó érték.

\details
Lux-Koblinger: Monte Carlo Particle Transport Methods
68. oldal, 3A.1. ábra alapján

*/


__device__ double kahn_sorsolas_Device(double lambda1, curandState* localState)
{
    
    //double alfa = 1.0 / lambda1;
    
    int elfogad = 0;
    double X;
    
    while(elfogad == 0)
    {
        double r1 = curand_uniform_double(localState);
        double r2 = curand_uniform_double(localState);
        double r3 = curand_uniform_double(localState);
        
        if( r1 <= ((1.0 + 2.0/lambda1)/(9.0 + 2.0/lambda1)) )
        {
            X = 1.0 + 2.0*r2/lambda1;
            if( r3 <= 4.0*(1/X - 1/X/X) )
            {
                elfogad = 1;
            }
        }
        else
        {
            X = (1.0 + 2.0/lambda1)/(1.0 + 2.0*r2/lambda1);
            if( r3 <= 0.5 * ((lambda1 - X*lambda1 + 1.0)*(lambda1 - X*lambda1 + 1.0) + 1.0/X ) )
            {
                elfogad = 1;
            }
        }
    }
    
    return X;
}

