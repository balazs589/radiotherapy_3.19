#ifndef KAHN_SORSOLAS_DEVICE_CUH
#define KAHN_SORSOLAS_DEVICE_CUH

#include <curand_kernel.h>


__device__ double kahn_sorsolas_Device(double lambda1, curandState* localState);


#endif
