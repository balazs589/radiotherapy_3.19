#include "tobbLejatszasbolEgyKiserlet.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"


#include "voxel_nullaz_kiserletenkent_kernel.cuh"
#include "dozis_atlag_frissit_kiserletenkent_kernel.cuh"

#include "tobbLepesbolEgyLejatszas.cuh"


/*!
\ingroup wrapper_fuggvenyek_group

\brief
Megadott számú ciklusban fotonok indítása, ezek során leadott dózisok összegzése
voxelenként.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void tobbLejatszasbolEgyKiserlet(Adatok *parameter)
{
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    // minden kiserlet elejen voxelek beutes szamainak es elnyelt energiainak lenullazasa:
    
    voxel_nullaz_kiserletenkent_kernel<<<parameter->_voxel_gridSize, parameter->_voxel_blockSize>>>(parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
/*----------------------------------------------------------------------------------------------------*/
    
    std::cout << "-----------------------------------------------------------------------------" << std::endl;
    
    parameter->_maximum_lepes_szam = 0;
    int tab = 0;
    
    for(parameter->_ciklus_index = 0; parameter->_ciklus_index<parameter->_ciklusok_szama; parameter->_ciklus_index++)
    {
        
        if(parameter->_ciklus_index % 100 == 0)
        {
            if(tab == 1)
            {
                std::cout << "\t";
                tab = 0;
            }
            else
            {
                tab = 1;
            }
            std::cout << parameter->_kiserlet_index+1 << ". kiserlet, "<< parameter->_ciklus_index+1 << ". lepes" << std::endl;
        }
        
        check_cuda_errors(__FILE__, __LINE__);
            tobbLepesbolEgyLejatszas(parameter);
        check_cuda_errors(__FILE__, __LINE__);
        
    } // end for(int parameter->_ciklus_index = 0; parameter->_ciklus_index<parameter->_ciklusok_szama; parameter->_ciklus_index++)
    // egy kiserlet vege
    
    
    
/*----------------------------------------------------------------------------------------------------*/
// minden egyes kiserlet utan a dozisok atlagahoz es szorasahoz szukseges mennyisegeket frissitjuk
    
    dozis_atlag_frissit_kiserletenkent_kernel<<<parameter->_voxel_gridSize, parameter->_voxel_blockSize>>>(parameter->_d_szigma);
    check_cuda_errors(__FILE__, __LINE__);
    
    
    
    
    
    check_cuda_errors(__FILE__, __LINE__);
}


 