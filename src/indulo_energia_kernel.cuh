#ifndef INDULO_ENERGIA_KERNEL_CUH
#define INDULO_ENERGIA_KERNEL_CUH

#include <curand_kernel.h>
#include "Allapot.cuh"


__global__ void indulo_energia_kernel(int N, int also_hatar, int felso_hatar, double E1, double E2, Allapot foton, curandState *state);


#endif
