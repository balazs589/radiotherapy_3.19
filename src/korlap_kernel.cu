#include "korlap_kernel.cuh"


#include <curand_kernel.h>
#include "Korlap.cuh"
#include "Allapot.cuh"
#include "Vektor3D.cuh"
#include "nyalab_forgatas_Device.cuh"


/*!
\ingroup Kernel_fuggvenyek_group

\brief
Foton induló koordinátáinak sorsolása.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] state CUDA random generátor állapota.
\param [in] kezdo_kor Induló koordináták tartománya: középponttal, sugárral és normálvektorral megadva.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 

*/

// kezdeti foton nyalab:
// normalvektorra meroleges siku korlapon egyenletesen szetszort fotonok
// kezdokoordinatainak szamitasa GPU-n rejekcios modszerrel

__global__ void korlap_kernel(int N, curandState *state, Korlap kezdo_kor, Allapot foton)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam<N)
    {
        curandState localState = state[sorszam];
        
        double u1 = 2.0*curand_uniform_double(&localState) - 1.0;
        double u2 = 2.0*curand_uniform_double(&localState) - 1.0;
        double s = u1*u1 + u2*u2;
        
        while(s>1.0)
        {
            u1 = 2.0*curand_uniform_double(&localState) - 1.0;
            u2 = 2.0*curand_uniform_double(&localState) - 1.0;
            s = u1*u1 + u2*u2;
        }
        
        Vektor3D tengely_irany;
        
        tengely_irany.x = kezdo_kor.normalv_x;
        tengely_irany.y = kezdo_kor.normalv_y;
        tengely_irany.z = kezdo_kor.normalv_z;
        
        Vektor3D elozo_hely;
        
        elozo_hely.x = kezdo_kor.sugar*u1;
        elozo_hely.y = kezdo_kor.sugar*u2;
        elozo_hely.z = 0.0;
        
        elozo_hely = nyalab_forgatas_Device(tengely_irany, elozo_hely);
        
        
        
        foton.elozo_hely.x[sorszam] = elozo_hely.x + kezdo_kor.kozeppont_x;
        foton.elozo_hely.y[sorszam] = elozo_hely.y + kezdo_kor.kozeppont_y;
        foton.elozo_hely.z[sorszam] = elozo_hely.z + kezdo_kor.kozeppont_z;
        
        
        
        state[sorszam] = localState;
    }
}
