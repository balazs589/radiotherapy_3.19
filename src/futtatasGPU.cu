#include "futtatasGPU.cuh"

#include "constants.h"      // pi es elektron nyugalmi energia
#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"

#include "tobbKiserletbolEgyMeres.cuh"

#include "marsaglia_izotrop_tesztelo_kernel.cuh"


/*!
\ingroup wrapper_fuggvenyek_group

\brief
A main függvényből meghívott, szimulációt grafikus kártyán elindító függvény.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void futtatasGPU(Adatok *parameter)
{
    
    // GPU futasido meghatarozasahoz:
    cudaEventCreate(&parameter->_start_MERES);
    cudaEventCreate(&parameter->_stop_MERES);
    cudaEventRecord( parameter->_start_MERES,0);    // kezdo idopont


    check_cuda_errors(__FILE__, __LINE__);
    
    tobbKiserletbolEgyMeres(parameter);
    
    check_cuda_errors(__FILE__, __LINE__);

    
    
    cudaEventRecord(parameter->_stop_MERES,0);
    cudaEventSynchronize(parameter->_stop_MERES);
    cudaEventElapsedTime(&parameter->_time_MERES, parameter->_start_MERES, parameter->_stop_MERES);
   
    check_cuda_errors(__FILE__, __LINE__);

    
    
/*----------------------------------------------------------------------------------------------------

    
    // 3D izotrop irany generalas tesztelesehez
    
    marsaglia_izotrop_tesztelo_kernel<<<parameter->_foton_gridSize, parameter->_foton_blockSize>>>(parameter->fotonSzalakSzama, parameter->_devStates, parameter->_foton);
    
    check_cuda_errors(__FILE__, __LINE__);
    
/*----------------------------------------------------------------------------------------------------*/    
    
    
    
    
    
    check_cuda_errors(__FILE__, __LINE__);
}


 