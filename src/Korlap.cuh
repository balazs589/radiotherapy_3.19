#ifndef KORLAP_CUH
#define KORLAP_CUH



/*!
    \ingroup Osztalyok_goup
    
    \brief Kezdeti foton nyaláb megadásához.
    
    \details
    
*/

class Korlap
{
public:
    double sugar;           //!< körlap sugara
    
    double kozeppont_x;     //!< körlap középpontjának x koordinátája
    double kozeppont_y;     //!< körlap középpontjának y koordinátája
    double kozeppont_z;     //!< körlap középpontjának z koordinátája
    
    double normalv_x;       //!< körlap normálisának x koordinátája
    double normalv_y;       //!< körlap normálisának y koordinátája
    double normalv_z;       //!< körlap normálisának z koordinátája
    
    double cos_alfa;        //!< nyaláb széttartását jellemző paraméter (kúp fél nyíllásszögének koszinusza)
};


#endif
