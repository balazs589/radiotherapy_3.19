#include "allapotMasolasHostra.cuh"


#include "Adatok.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()


/*!
\ingroup wrapper_fuggvenyek_group

\brief
Fotonok pillanatnyi állapotát megadó adatok grafikus kártyáról host memóriába másolása.

\param [in,out] parameter Ezen keresztül lehet írni és olvasni a program adatait.
\return Nincs.

\details 

*/

void allapotMasolasHostra(Adatok *parameter)
{
    
    std::cout << std::endl;
    std::cout << "fotonok aktualis allapotanak masolasa GPU-rol hostra...\n" << std::endl;
    
    
    check_cuda_errors(__FILE__, __LINE__);
    
    
     
    // memoriafoglalas a fotonokat leiro adatoknak:
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.energia,            parameter->_pillanatnyi_foton.energia,            parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.elozo_hely.x,       parameter->_pillanatnyi_foton.elozo_hely.x,       parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.elozo_hely.y,       parameter->_pillanatnyi_foton.elozo_hely.y,       parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.elozo_hely.z,       parameter->_pillanatnyi_foton.elozo_hely.z,       parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.elozo_irany.x,      parameter->_pillanatnyi_foton.elozo_irany.x,      parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.elozo_irany.y,      parameter->_pillanatnyi_foton.elozo_irany.y,      parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.elozo_irany.z,      parameter->_pillanatnyi_foton.elozo_irany.z,      parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.aktiv_foton,        parameter->_pillanatnyi_foton.aktiv_foton,        parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.kolcsonhatas_tipus, parameter->_pillanatnyi_foton.kolcsonhatas_tipus, parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToHost));
    
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.leadott_energia,    parameter->_pillanatnyi_foton.leadott_energia,    parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.voxel,              parameter->_pillanatnyi_foton.voxel,              parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToHost));
    
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.fotoeff_szam,       parameter->_pillanatnyi_foton.fotoeff_szam,       parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.compton_szam,       parameter->_pillanatnyi_foton.compton_szam,       parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.parkelt_szam,       parameter->_pillanatnyi_foton.parkelt_szam,       parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToHost));
    
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.volt_kolcsonhatas,  parameter->_pillanatnyi_foton.volt_kolcsonhatas,  parameter->fotonSzalakSzama*sizeof(int),    cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.allapot_kapcsolo,   parameter->_pillanatnyi_foton.allapot_kapcsolo,   parameter->fotonSzalakSzama*sizeof(int),    cudaMemcpyDeviceToHost));
    
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.foton_aram_1,       parameter->_pillanatnyi_foton.foton_aram_1,       6*parameter->fotonSzalakSzama*sizeof(int),    cudaMemcpyDeviceToHost));
    
    
    //teszteleshez
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.hkrm_csoport,       parameter->_pillanatnyi_foton.hkrm_csoport,       parameter->fotonSzalakSzama*sizeof(int),      cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.hkrm_totalis,       parameter->_pillanatnyi_foton.hkrm_totalis,       parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.lambda,             parameter->_pillanatnyi_foton.lambda,             parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.kiindulo_pont.x,    parameter->_pillanatnyi_foton.kiindulo_pont.x,    parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.kiindulo_pont.y,    parameter->_pillanatnyi_foton.kiindulo_pont.y,    parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(parameter->_host_foton.kiindulo_pont.z,    parameter->_pillanatnyi_foton.kiindulo_pont.z,    parameter->fotonSzalakSzama*sizeof(double),   cudaMemcpyDeviceToHost));
    
    
    
    check_cuda_errors(__FILE__, __LINE__);
    
    
    std::cout << "masolas elkeszult\n" << std::endl;
    std::cout << "-------------------------------------" << std::endl;
    std::cout << std::endl;
    
    
}


 