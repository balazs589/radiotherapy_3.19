#include "memoriaFelszabaditasHost.cuh"

#include "CUDA_debug.h"     // CUDA_CALL() & check_cuda_errors()

#include "Adatok.cuh"



/*!
\ingroup CPU_fuggvenyek_group

\brief
Mérés végén host memória felszabadítása.

\param [in,out] parameter Ezen a változón keresztül érhető el a program összes globálisan használt adata.
\return Nincs.

\details 

*/

void memoriaFelszabaditasHost(Adatok *parameter)
{
    
    
    
    // memoriateruletek felszabaditasa es megnyitott fajlok bezarasa
    
    free(parameter->_E_intervallumok);
    free(parameter->_fluxus);
    free(parameter->_foton_eloszlas);   
    
    free(parameter->_h_szigma.voxel_rho); 
    
    free(parameter->_h_szigma.dozis_atlag); 
    free(parameter->_h_szigma.dozis_szoras);    
    
    free(parameter->_h_szigma.csatornak);    
    
    free(parameter->_h_szigma.energia); 
    free(parameter->_h_szigma.fotoeff); 
    free(parameter->_h_szigma.compton); 
    free(parameter->_h_szigma.parkelt); 
    free(parameter->_h_szigma.totalis); 
    free(parameter->_h_szigma.szupremum);
    free(parameter->_h_szigma.rho);
    
    
    // fotonokat leiro adatok hoston:
    free(parameter->_host_foton.energia);
    
    free(parameter->_host_foton.elozo_hely.x);
    free(parameter->_host_foton.elozo_hely.y);
    free(parameter->_host_foton.elozo_hely.z);
    free(parameter->_host_foton.elozo_irany.x);
    free(parameter->_host_foton.elozo_irany.y);
    free(parameter->_host_foton.elozo_irany.z);
    
    free(parameter->_host_foton.aktiv_foton);
    free(parameter->_host_foton.kolcsonhatas_tipus);
    
    free(parameter->_host_foton.leadott_energia);
    free(parameter->_host_foton.voxel);
    
    free(parameter->_host_foton.fotoeff_szam);
    free(parameter->_host_foton.compton_szam);
    free(parameter->_host_foton.parkelt_szam);
    
    free(parameter->_host_foton.volt_kolcsonhatas);
    free(parameter->_host_foton.allapot_kapcsolo);
    
    free(parameter->_host_foton.foton_aram_1);
    
    
    free(parameter->_host_foton.hkrm_csoport);
    free(parameter->_host_foton.hkrm_totalis);
    free(parameter->_host_foton.lambda);
    free(parameter->_host_foton.kiindulo_pont.x);
    free(parameter->_host_foton.kiindulo_pont.y);
    free(parameter->_host_foton.kiindulo_pont.z);
   
    
    
}


 