#include "foton_aram_meghatarozas_kernel.cuh"


#include "Allapot.cuh"
#include "Hataskeresztmetszet.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Foton áramok meghatározása adott téglalapokon.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void foton_aram_meghatarozas_kernel(int N, Allapot foton, Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam<N)
    {
        int aktiv_foton = foton.aktiv_foton[sorszam];
        
        if(aktiv_foton == 1)
        {
            
            //double aktualis_energia = foton.energia[sorszam];   // [MeV]
            int volt_kolcsonhatas = foton.volt_kolcsonhatas[sorszam];   // 0 - nem, 1 - igen
            
            
            // csak a kolcsonhatas nelkul athalado fotonokat szamoljuk
            if(volt_kolcsonhatas == 0)
            {
                
                // double x1 = foton.kiindulo_pont.x[sorszam];
                // double y1 = foton.kiindulo_pont.y[sorszam];
                double z1 = foton.kiindulo_pont.z[sorszam];
                
                double x2 = foton.elozo_hely.x[sorszam];
                double y2 = foton.elozo_hely.y[sorszam];
                double z2 = foton.elozo_hely.z[sorszam];
                
                double vx = foton.elozo_irany.x[sorszam];
                double vy = foton.elozo_irany.y[sorszam];
                double vz = foton.elozo_irany.z[sorszam];
                
                double t;
                double x;
                double y;
                double z;
                
                // if( (z1>z)&&(z2<z) )        // fotonaram csak lefele
                // if( (z1<z)&&(z2>z) )        // fotonaram csak felfele
                // if( ((z1-z)*(z2-z)) < 0.0 ) // fotonaram lefele vagy felfele 
                
                
                double felulet_z[6];
                felulet_z[0] = szigma.felulet1_z;
                felulet_z[1] = szigma.felulet2_z;
                felulet_z[2] = szigma.felulet3_z;
                felulet_z[3] = szigma.felulet4_z;
                felulet_z[4] = szigma.felulet5_z;
                felulet_z[5] = szigma.felulet6_z;
                
                
                
                for(int iii = 0; iii<6; iii++)
                {
                    z = felulet_z[iii];
                    
                    if( ((z1-z)*(z2-z)) < 0.0 ) // fotonaram lefele vagy felfele 
                    {
                        t = (z - z2) / vz;
                        x = x2 + vx*t;
                        y = y2 + vy*t;
                        
                        if( (x>szigma.felulet_x_min)&&(x<szigma.felulet_x_max)&&(y>szigma.felulet_y_min)&&(y<szigma.felulet_y_max) )
                        {   
                            //foton.foton_aram_1[sorszam + iii*N] = ((z1>z2) ? (+1) : (-1));    // netto fotonaramhoz (darabszam)
                            
                            //foton.foton_aram_1[sorszam + iii*N] = (+1);                       // brutto fotonaram (darabszam)
                            
                            foton.foton_aram_1[sorszam + iii*N] = ((z1>z2) ? (+1) : (0));       // fentrol lefele fotonaramhoz (darabszam)
                            
                        }
                    
                    }
                
                }
                
            
            }
            
            
        }
    }
}
 
