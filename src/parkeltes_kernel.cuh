#ifndef PARKELTES_KERNEL_CUH
#define PARKELTES_KERNEL_CUH


#include <curand_kernel.h>
#include "Allapot.cuh"


__global__ void parkeltes_kernel(int N, curandState *state, Allapot foton, Allapot parkelt_foton);


#endif
