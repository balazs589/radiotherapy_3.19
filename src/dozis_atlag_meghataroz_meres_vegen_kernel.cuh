#ifndef DOZIS_ATLAG_MEGHATAROZ_MERES_VEGEN_KERNEL_CUH
#define DOZIS_ATLAG_MEGHATAROZ_MERES_VEGEN_KERNEL_CUH

#include "Hataskeresztmetszet.cuh"


__global__ void dozis_atlag_meghataroz_meres_vegen_kernel(int kiserletek_szama, Hataskeresztmetszet szigma);


#endif
