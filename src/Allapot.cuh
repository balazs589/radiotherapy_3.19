#ifndef ALLAPOT_CUH
#define ALLAPOT_CUH

#include "Koordinatak.cuh"

/*!
    \ingroup Osztalyok_goup
    
    \brief Fotonokat jellemző adatok összegyűjtéséhez.
    
    \details
    
*/

class Allapot
{
public:
    
    
    Koordinatak elozo_hely;         //!< foton aktuális helyzete
    Koordinatak elozo_irany;        //!< foton aktuális iránya
    
    double* energia;                //!< foton aktuális energiája
    int*    aktiv_foton;            //!< 0 - ha a foton már nem képes energiát leadni a vizsgált térfogatban
    int*    kolcsonhatas_tipus;     //!< 1 - fotoeffektus, 2 - Compton-szórás, 3 - párkeltés
    
    double* leadott_energia;        //!< aktuális kölcsönhatásban leadott energia
    int*    voxel;                  //!< melyik voxelben történt az energialeadás
    
    int* fotoeff_szam;              //!< hány fotoeffektus következett be a foton életútja során (0,1)
    int* compton_szam;              //!< hány compton szórás következett be a foton életútja során (0,1,2,...)
    int* parkelt_szam;              //!< hány párkeltés következett be a foton életútja során (0,1)
    
        
    int* volt_kolcsonhatas;         //!< 0 - foton kölcsönhatás nélkül, 1 - foton már kölcsönhatásba lépett az anyaggal
    
    int* allapot_kapcsolo;          //!< tesztelések során bármilyen szűréshez
    
    double* detektor_energia;       //!<  a foton által leadott összes energia gyűjtéséhez
    int* detektor_csatorna;         //!<  a leadott összes energiához tartozó detektor csatorna
    
    
    int* foton_aram_1;              //!< keresztül haladt-e a foton a vizsgált felületeken
    Koordinatak kiindulo_pont;      //!< kölcsönhatás előtti előző ütközéspont helye
    
    
    
    /*! \name Teszteléshez
    \{*/
    int*    hkrm_csoport;           //!< XCOM táblázat energiacsoport
    double* hkrm_totalis;           //!< lineárisan interpolált totális hatáskeresztmetszet
    double* lambda;                 //!< sorsolt szabad úthossz
    Koordinatak kovetkezo_irany;    //!< sorsolt új irány
    /*!\}*/
    
    
};


#endif
