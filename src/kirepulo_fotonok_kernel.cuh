#ifndef KIREPULO_FOTONOK_KERNEL_CUH
#define KIREPULO_FOTONOK_KERNEL_CUH

#include "Allapot.cuh"


__global__ void kirepulo_fotonok_kernel(int N, Allapot foton);


#endif
