#include "dozis_atlag_meghataroz_meres_vegen_kernel.cuh"


#include "Hataskeresztmetszet.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Az utolsó kísérlet után meghatározzuk a voxelenkétni dózisok átlagát és szórását.

\param [in] kiserletek_szama Ennyi adatból számolunk átlagot.
\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void dozis_atlag_meghataroz_meres_vegen_kernel(int kiserletek_szama, Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < szigma.voxelek_szama)
    {
        double osszeg           = szigma.dozis_atlag[sorszam];
        double negyzetosszeg    = szigma.dozis_szoras[sorszam];
        
        double n = double(kiserletek_szama);
        
        
        // atlag (mintakozep)
        szigma.dozis_atlag[sorszam]     = (1.0 / n) * osszeg;
        
        
        // szoras (korrigalatlan empirikus szoras)
//        szigma.dozis_szoras[sorszam]    = sqrt( (1.0 / n) * (negyzetosszeg - (1.0 / n)*osszeg*osszeg) );
        
        // szoras (korrigalt empirikus szoras, egyes kiserletek eredmenyeinek standard elterese)
//        szigma.dozis_szoras[sorszam]    = sqrt( (1.0 / (n-1)) * (negyzetosszeg - (1.0 / n)*osszeg*osszeg) );
    
    
    
        // atlagertek standard elterese (becsult 1 szigmas hibaja)
        szigma.dozis_szoras[sorszam]    = sqrt( (1.0 / ( n*(n-1) ) ) * (negyzetosszeg - (1.0 / n)*osszeg*osszeg) );

        
        
    }
}


