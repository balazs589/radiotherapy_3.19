#include "voxel_kigyujt_lepesenkent_kernel.cuh"

#include "Hataskeresztmetszet.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Minden kölcsönhatás után összegyűjtjük, hogy az egyes voxelekben mennyi energiát adtak le a fotonok.

\param [in] voxel_listahossz Kigyűjtendő voxelek száma.
\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void voxel_kigyujt_lepesenkent_kernel(int voxel_listahossz, Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < voxel_listahossz)
    {
        int voxel_sorszam   = szigma.sorszam_gyujto[sorszam];
        double voxel_dozis  = szigma.dozis_gyujto[sorszam];
        
        if(voxel_sorszam != -1)
        {
            szigma.dozis[voxel_sorszam] += voxel_dozis;
        }
        else
        {
            szigma.kirepult_energia[0] += voxel_dozis;
        }
        
    }
}
