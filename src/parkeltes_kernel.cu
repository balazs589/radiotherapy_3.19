#include "parkeltes_kernel.cuh"

#include <curand_kernel.h>
#include "Allapot.cuh"
#include "Vektor3D.cuh"
#include "marsaglia_izotrop_irany_Device.cuh"
#include "constants.h"      // pi es elektron nyugalmi energia

/*!
\ingroup Kernel_fuggvenyek_group

\brief
Párkeltés során történt változások
(leadott energia, új irány és energia első és második foton számára) meghatározása.

\param [in] N Egyszerre elindított fotonok száma.
\param [in,out] state CUDA random generátor állapota.
\param [in,out] foton Fotonok aktuális adatait elérő struktúra.
\param [in,out] parkelt_foton Párkeltésből származó második fotonok aktuális adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void parkeltes_kernel(int N,curandState *state,Allapot foton,Allapot parkelt_foton)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;  // thread-hez rendelt foton sorszama
    
    if(sorszam < N)     // fotonok 0 -tol (N-1) -ig indexelve
    {
        // thread-hez rendelt foton allapotanak beolvasasa:
        int aktiv_foton = foton.aktiv_foton[sorszam];
        int kolcsonhatas_tipus = foton.kolcsonhatas_tipus[sorszam];
        
        // csak akkor szamolunk tovabb, ha ennek a fotonnak elozoleg
        // parkeltes kolcsonhatast sorsoltunk:
        if((aktiv_foton == 1)&&(kolcsonhatas_tipus == 3))
        {
            foton.parkelt_szam[sorszam] = 1;        // parkeltesek osszeszamolasahoz
            
            foton.volt_kolcsonhatas[sorszam] = 1;   // ez mar nem utkozes nelkuli
            
            curandState localState = state[sorszam];    // CUDA random generator
            
            double regi_energia = foton.energia[sorszam];   // eddigi energia beolvasasa
            
            // elektron es pozitron leadott mozgasi energiajanak osszege
            foton.leadott_energia[sorszam] = (regi_energia - 2.0 * NYUGALMI_ENERGIA);
            
            // foton energiak csoportba gyujtesehez, (a megfelelo helyen kell kapcsolni):
            if(foton.allapot_kapcsolo[sorszam] == 1)
            {
                foton.detektor_energia[sorszam] += (regi_energia - 2.0 * NYUGALMI_ENERGIA);
            }
            
            // 3D izotrop iranysorsolas:
            Vektor3D egyik_irany = marsaglia_izotrop_irany_Device(&localState);
            
            // elso foton adatai:
            foton.energia[sorszam] = NYUGALMI_ENERGIA;
            foton.elozo_irany.x[sorszam] = egyik_irany.x;
            foton.elozo_irany.y[sorszam] = egyik_irany.y;
            foton.elozo_irany.z[sorszam] = egyik_irany.z;
            
            // masodik foton adatai:
            parkelt_foton.energia[sorszam] = NYUGALMI_ENERGIA;
            parkelt_foton.elozo_irany.x[sorszam] = -egyik_irany.x;
            parkelt_foton.elozo_irany.y[sorszam] = -egyik_irany.y;
            parkelt_foton.elozo_irany.z[sorszam] = -egyik_irany.z;
            
            parkelt_foton.elozo_hely.x[sorszam] = foton.elozo_hely.x[sorszam];
            parkelt_foton.elozo_hely.y[sorszam] = foton.elozo_hely.y[sorszam];
            parkelt_foton.elozo_hely.z[sorszam] = foton.elozo_hely.z[sorszam];
            parkelt_foton.aktiv_foton[sorszam] = 1;
            
            // energiacsatornakba csak parkeltes elso fotonjainak jaruleka:
            //foton.allapot_kapcsolo[sorszam] = 1;
            
            // energiacsatornakba csak parkeltes masodik fotonjainak jaruleka:
            //parkelt_foton.allapot_kapcsolo[sorszam] = 1;
            
            state[sorszam] = localState;
        }
    }
}


