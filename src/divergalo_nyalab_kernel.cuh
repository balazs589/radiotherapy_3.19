#ifndef DIVERGALO_NYALAB_KERNEL_CUH
#define DIVERGALO_NYALAB_KERNEL_CUH

#include <curand_kernel.h>
#include "Korlap.cuh"
#include "Allapot.cuh"


__global__ void divergalo_nyalab_kernel(int N, curandState *state, Korlap kezdo_kor, Allapot foton);


#endif
