#include "dozis_atlag_nullaz_meres_elejen_kernel.cuh"


#include "Hataskeresztmetszet.cuh"

/*!
\ingroup Kernel_fuggvenyek_group

\brief
A mérés elején a voxelenkénti dózisok átlagát és szórását lenullázzuk.

\param [in,out] szigma Besugárzott anyag adatait elérő struktúra.
\return Nincs.

\details 

*/


__global__ void dozis_atlag_nullaz_meres_elejen_kernel(Hataskeresztmetszet szigma)
{
    int sorszam = threadIdx.x + blockIdx.x*blockDim.x;
    
    if(sorszam < szigma.voxelek_szama)
    {
        szigma.dozis_atlag[sorszam]     = 0.0;
        szigma.dozis_szoras[sorszam]    = 0.0;
    }
}
