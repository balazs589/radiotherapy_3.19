SOURCEDIR = src
OBJDIR = build

SOURCES = $(wildcard $(SOURCEDIR)/*.cu)
OBJECTS = $(patsubst $(SOURCEDIR)/%.cu, $(OBJDIR)/%.o, $(SOURCES))

EXTRADEPS = $(SOURCEDIR)/constants.h $(SOURCEDIR)/CUDA_debug.h
# *.h


NVCC_DEVICE_COMPILING = nvcc -dc   -arch=sm_20 -Wno-deprecated-gpu-targets  #-Xptxas=-v
NVCC_LINKING = nvcc -rdc=true      -arch=sm_20 -Wno-deprecated-gpu-targets  #-Xptxas=-v    #-L /usr/local/cuda/lib/ -l curand


teleterapia.run: $(OBJECTS) 
	$(NVCC_LINKING) -o $@ $^ && echo

$(OBJECTS): $(EXTRADEPS)

$(OBJDIR)/%.o: $(SOURCEDIR)/%.cu $(SOURCEDIR)/%.cuh | $(OBJDIR)
	$(NVCC_DEVICE_COMPILING) $< -o $@

$(OBJDIR):
	mkdir $(OBJDIR)

	
.PHONY: clean

clean:
	rm -rf teleterapia.run $(OBJECTS)


#######################################################

all: teleterapia.run others

others: feldolgoz.run rajzol/log_metszet.run

feldolgoz.run: $(SOURCEDIR)/other/feldolgoz.cu
	nvcc -arch=sm_20 -Wno-deprecated-gpu-targets $< -o $@

rajzol/log_metszet.run: $(SOURCEDIR)/other/log_metszet.cpp
	nvcc -arch=sm_20 -Wno-deprecated-gpu-targets $< -o $@


.PHONY: all cleanall others

#######################################################
DOCUMENTDIR = docs

#.cuda_doc_preprocess:
#	mv $(SOURCEDIR)/main.cu $(SOURCEDIR)/main.txt && \
#	awk '{gsub("<<<.*>>>","");print$0}' $(SOURCEDIR)/main.txt > $(SOURCEDIR)/main.cu 

#docs: .cuda_doc_preprocess | $(DOCUMENTDIR)
#	doxygen Doxyfile && mv $(SOURCEDIR)/main.txt $(SOURCEDIR)/main.cu

docs: | $(DOCUMENTDIR)
	doxygen Doxyfile

latex: docs
	cd $(DOCUMENTDIR)/latex && make

.PHONY: docs latex run .cuda_doc_preprocess

#######################################################

cleanall:
	make clean && rm -rf $(OBJDIR) feldolgoz.run rajzol/log_metszet.run $(DOCUMENTDIR)
